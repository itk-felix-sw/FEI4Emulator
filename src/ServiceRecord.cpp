#include "FEI4Emulator/ServiceRecord.h"
#include <iostream>
#include <iomanip>
#include <bitset>

using namespace std;
using namespace fei4b;

vector<string> ServiceRecord::m_code2meaning={
  "BCID counter error",
  "Hamming code error in word 0",
  "Hamming code error in word 1",
  "Hamming code error in word 2",
  "L1_in counter error",
  "L1 request counter error",
  "L1 register error",
  "L1 Trigger ID error",
  "readout processor error",
  "Fifo_Full flag pulsed",
  "HitOr bus pulsed",
  "not used",
  "not used",
  "not used",
  "3 MSBs of bunch counter and 7 MSBs of L1A counter"
  "Skipped trigger counter"
  "Truncated event flag and counter"
  "not used"
  "not used"
  "not used"
  "not used"
  "Reset bar RA2b pulsed"
  "PLL generated clock phase faster than reference"
  "Reference clock phase faster than PLL"
  "Triple redundant mismatch"
  "Write register data error"
  "Address error"
  "Other command decoder error- see CMD section"
  "Bit flip detected in command decoder input stream",
  "SEU upset detected in command decoder (triple redundant mismatch)",
  "Data bus address error",
  "Triple redundant mismatch"};

ServiceRecord::ServiceRecord(){
  m_id=0xEF;
  m_code=0;
  m_counter=0;
}

ServiceRecord::ServiceRecord(ServiceRecord *copy){
  m_id=copy->m_id;
  m_code=copy->m_code;
  m_counter=copy->m_counter;
}

ServiceRecord::ServiceRecord(uint32_t code, uint32_t counter){
  m_id=0xEF;
  m_code=code;
  m_counter=counter;
}

ServiceRecord * ServiceRecord::DataCounter(uint32_t l1id, uint32_t bcid, bool shifted){
  ServiceRecord * sr=new ServiceRecord(11,0);
  sr->SetL1IDbits(l1id,shifted);
  sr->SetBCIDbits(bcid,shifted);
  return sr;
}

ServiceRecord::~ServiceRecord(){}

string ServiceRecord::ToString(){
  ostringstream os;
  os << "ServiceRecord"
     << " Code: " << m_code
     << " Counter: " << m_counter;
  return os.str();
}
  
uint32_t ServiceRecord::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<3) return 0;
  if(bytes[0]!=0xEF and
     bytes[0]!=0xE7 and
     bytes[0]!=0xFF and
     bytes[0]!=0xCF and
     bytes[0]!=0xAF /*and
     bytes[0]!=0x6F*/){
    //cout << "Service record not recognized: 0x" << hex << (uint32_t)bytes[0] << dec << endl;
    return 0;
  }
  m_id      = bytes[0];
  m_code    =(bytes[1]&0xFF)>>2;
  /*if (m_code == 14){
    bitset<8> anID = bytes[0];
    bitset<8> anInt = bytes[1];
    bitset<8> dunno = bytes[2];
    cout << "my bytes are: " << anID << " " << anInt << " " << dunno << endl;
  }*/
  m_counter =(bytes[1]&0x03)<<8;
  m_counter|=(bytes[2]&0xFF)>>0;
  return 3;
}

uint32_t ServiceRecord::Pack(uint8_t * bytes){
  bytes[0] = m_id;
  cout << "Why should this be called?" << endl;
  bytes[1] =(m_code   <<2)&0xFC;
  bytes[1]|=(m_counter>>8)&0x03;
  bytes[2] =(m_counter>>0)&0xFF;
  return 3;
}

uint32_t ServiceRecord::GetType(){
  return Record::SERVICE_RECORD;
}

void ServiceRecord::SetCode(uint32_t code){
  m_code=code;
}

uint32_t ServiceRecord::GetCode(){
  return m_code;
}

void ServiceRecord::SetCounter(uint32_t counter){
  m_counter=counter;
}

uint32_t ServiceRecord::GetCounter(){
  return m_counter;
}
 
void ServiceRecord::SetL1IDbits(uint32_t l1id, bool shifted){
  cout << "Please never be called." << endl;
  m_counter&=~0x3F8;
  if(shifted) l1id=l1id>>5;
  m_counter|=(l1id&0x7F)<<3;
}

uint32_t ServiceRecord::GetL1IDbits(bool shift){
  uint32_t bits=(m_counter&0x3FF)>>3;
  if(shift) bits=bits<<4;
  return bits;
}

void ServiceRecord::SetBCIDbits(uint32_t bcid, bool shifted){
  cout << "Please never be called2. " << endl;
  m_counter&=~0x7;
  if(shifted) bcid=bcid>>10;
  m_counter|=bcid&0x7;
}

uint32_t ServiceRecord::GetBCIDbits(bool shift){
  uint32_t bits=m_counter&0x7;
  if(shift) bits=bits<<9;
  return bits;
}

