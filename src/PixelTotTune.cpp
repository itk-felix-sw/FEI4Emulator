#include "FEI4Emulator/PixelTotTune.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

PixelTotTune::PixelTotTune(){}

PixelTotTune::~PixelTotTune(){}

void PixelTotTune::Run(){


  uint32_t ttt_ini =  8;
  uint32_t ttt_min =  0;
  uint32_t ttt_max = 16;
  float target_tot = 7;
  float target_tot_err = 0.1;
  float target_tot_min = target_tot - target_tot_err;
  float target_tot_max = target_tot + target_tot_err;

  uint32_t mask_step = 4; //0,1,2,4,8,16
  uint32_t dc_mode = 1; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 4; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 10;
  float charge = GetCharge();
  bool use_scap = true;
  bool use_lcap = true;
  bool retune = GetRetune();


  //create histograms
  map<string,TH2I*> occ;
  map<string,TH2I*> tot;
  map<string,TH2I*> mask;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    tot[fe->GetName()]=new TH2I(("tot_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    mask[fe->GetName()]=new TH2I(("mask_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
  }

  map<string,map<uint32_t,map<uint32_t,uint32_t> > > mthr;
  map<string,map<uint32_t,map<uint32_t,uint32_t> > > mvlo;
  map<string,map<uint32_t,map<uint32_t,uint32_t> > > mvhi;
  map<string,map<uint32_t,map<uint32_t,map<uint32_t, float> > > > mpar;

  uint32_t vcal = Tools::toVcal(charge, use_scap, use_lcap);

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,1);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,10);
    fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        if(not retune){
          fe->SetPixelPreampFeedback(col,row,ttt_ini);
        }
        mthr[fe->GetName()][col][row]=fe->GetPixelPreampFeedback(col,row);
        mvlo[fe->GetName()][col][row]=ttt_min;
        mvhi[fe->GetName()][col][row]=ttt_max;
      }
    }
    fe->SetMask(3,0);
    Send(fe);
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger();

  //keep going until we converge or we give up
  while(true){

    //change the mask
    for(uint32_t mask=0; mask<mask_step; mask++){
      cout << "Mask: " << (mask+1) << "/" << mask_step << endl;
  
      for(auto fe : GetFEs()){
        fe->SetMask(mask_step,mask);
      }
      
      //select the double column
      for(uint32_t dc=0; dc<dc_max; dc++){
        cout << "DC: " << (dc+1) << "/" << dc_max << endl;
        for(auto fe : GetFEs()){
          fe->SelectDoubleColumn(dc_mode,dc);
          fe->SetRunMode(true);
          Send(fe);
        }
        uint32_t nhits2=0;

        //Trigger and read-out
        for(uint32_t trig=0;trig<n_trigs; trig++){

          Trigger();

          auto start = chrono::steady_clock::now();

          //histogram it
          uint32_t nhits=0;
          while(true){
            for(auto fe : GetFEs()){
              if(!fe->HasHits()) continue;
              Hit* hit = fe->GetHit();
              //cout << hit->ToString() << endl;
              if(hit and hit!=0){
                occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
                tot[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetTOT());
                nhits++;
              }
              fe->NextHit();
            }
            auto end = chrono::steady_clock::now();
            uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
            if(ms>50){break;}
          }
          nhits2+=nhits;
          //cout << "NHits per trigger: " << nhits << endl;
        }
        cout << "NHits in step: " << nhits2 << endl;
        for(auto fe : GetFEs()){
          fe->SetRunMode(false);
          Send(fe);
        }
      }
    }

    bool all_done=true;
    for(auto fe : GetFEs()){
      cout << fe->GetName() << " Analyse" << endl;
      for(uint32_t col=0;col<80;col++){
        for(uint32_t row=0;row<336;row++){
          uint32_t thr = mthr[fe->GetName()][col][row];
          uint32_t vlo = mvlo[fe->GetName()][col][row];
          uint32_t vhi = mvhi[fe->GetName()][col][row];
          uint32_t bin=occ[fe->GetName()]->GetBin(col,row);
          uint32_t ddd=occ[fe->GetName()]->GetBinContent(bin);
          uint32_t nnn=tot[fe->GetName()]->GetBinContent(bin);
          float mean = nnn/(float)ddd;
          cout << fe->GetName()
               << " col:" << col
               << " row:" << row
               << " thr:" << thr
               << " vlo:" << vlo
               << " vhi:" << vhi
               << " mean: " << mean
               << endl;
          mpar[fe->GetName()][col][row][thr]=mean;
          if(mean >= target_tot_min and mean < target_tot_max){
            cout << fe->GetName() << " Cannot get better than this" << endl;
          }else{
            if (mean >= target_tot_max) {
              vlo = thr;
            }else if (mean < target_tot_min) {
              vhi = thr;
            }
            if ((vhi - vlo) > 1) {
              thr = ((unsigned)(vhi + vlo))/2;
              if(mpar[fe->GetName()][col][row].find(thr) != mpar[fe->GetName()][col][row].end()){
                //Already used this value...
                continue;
              }
              mthr[fe->GetName()][col][row]=thr;
              mvlo[fe->GetName()][col][row]=vlo;
              mvhi[fe->GetName()][col][row]=vhi;
              fe->SetPixelThreshold(col,row,thr);
              all_done=false;
            }
          }
        }
      }
      Send(fe);
    }
    if(all_done) break;
  }

  //Results

  for(auto fe : GetFEs()){
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        mask[fe->GetName()]->Fill(col,row,fe->GetPixelConfig(col,row,"Enable"));
      }
    }
    //mask[fe->GetName()]->SaveAs(("Mask_"+fe->GetName()+".C").c_str());
    //occ[fe->GetName()]->SaveAs(("Occ_"+fe->GetName()+".C").c_str());
    //tot[fe->GetName()]->SaveAs(("Tot_"+fe->GetName()+".C").c_str());

    mask[fe->GetName()]->Write();
    occ[fe->GetName()]->Write();
    tot[fe->GetName()]->Write();
  }


}
