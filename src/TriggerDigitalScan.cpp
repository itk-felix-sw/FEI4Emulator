#include "FEI4Emulator/TriggerDigitalScan.h"
#include "TH2I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

TriggerDigitalScan::TriggerDigitalScan(){}

TriggerDigitalScan::~TriggerDigitalScan(){}

void TriggerDigitalScan::Run(){

  uint32_t mask_step = 8; //0,1,2,4,8,16
  uint32_t dc_mode = 1; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 4; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 20;

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,1);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-50);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0x1);
    fe->SetGlobalThreshold(200);
    fe->ConfigGlobal();
    fe->SetMask(3,0);
    Send(fe);
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger();
  
  //change the mask
  for(uint32_t mask=0; mask<mask_step; mask++){
    cout << "Mask: " << (mask+1) << "/" << mask_step << endl;
    
    for(auto fe : GetFEs()){
      fe->SetMask(mask_step,mask);
    }

    //select the double column
    for(uint32_t dc=0; dc<dc_max; dc++){
      cout << "DC: " << (dc+1) << "/" << dc_max << endl;
      for(auto fe : GetFEs()){
        fe->SelectDoubleColumn(dc_mode,dc);
        fe->SetRunMode(true);
        Send(fe);
      }

      //Trigger and read-out
      for(uint32_t trig=0;trig<n_trigs; trig++){
        Trigger();
        
        
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }
      for(auto fe : GetFEs()){
        fe->SetRunMode(false);
        Send(fe);
      }
    }
  }
}
