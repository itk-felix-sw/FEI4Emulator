#include "FEI4Emulator/AnalogScan.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

AnalogScan::AnalogScan(){}

AnalogScan::~AnalogScan(){}

void AnalogScan::Run(){

  uint32_t mask_step = 8; //0,1,2,4,8,16
  uint32_t dc_mode = 1; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 4; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 100;

  float charge = 50000;
  bool use_scap = true;
  bool use_lcap = true;

  //create histograms
  map<string,TH2I*> occ;
  map<string,TH1I*> occ_1D;
  map<string,TH2I*> mask;
  map<string,TH2I*> dc_corr;

  map<string,uint32_t> mem_thres;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    occ[fe->GetName()]->GetZaxis()->SetRangeUser(0,2*n_trigs);
    mask[fe->GetName()]=new TH2I(("mask_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    occ_1D[fe->GetName()]=new TH1I(("occ_1D_"+fe->GetName()).c_str(),";1D Occ",201,-0.5,200.5);
    dc_corr[fe->GetName()]=new TH2I(("dc_corr_"+fe->GetName()).c_str(),";Expected DC step;Observed DC step",dc_max,-0.5,dc_max-0.5,dc_max,-0.5,dc_max-0.5);
  }

  uint32_t vcal = Tools::toVcal(charge, use_scap, use_lcap);

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,5); // maybe more needed?
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-50);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
    fe->GetConfig()->SetField(Configuration::CalPulseWidth,20); // before 5, maybe 20?
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelThreshold(col,row,16); 
      }
    }
    mem_thres[fe->GetName()]=fe->GetGlobalThreshold();
    fe->SetGlobalThreshold(100);
    fe->ConfigGlobal();
    fe->ConfigPixelThresholds();
    if (use_lcap) fe->EnableLCap();
    if (use_scap) fe->EnableSCap();
    fe->SetMask(3,0);
    Send(fe);
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger(50);

  //change the mask
  for(uint32_t mask=0; mask<mask_step; mask++){
    cout << "Mask: " << (mask+1) << "/" << mask_step << endl;

    for(auto fe : GetFEs()){
      fe->SetMask(mask_step,mask);
    }

    //select the double column
    for(uint32_t dc=0; dc<dc_max; dc++){
      cout << "DC: " << (dc+1) << "/" << dc_max << endl;
      for(auto fe : GetFEs()){
        fe->SelectDoubleColumn(dc_mode,dc);
        fe->ConfigGlobal();
        fe->SetRunMode(true);
        Send(fe);
      }
      uint32_t nhits2=0;

      //Trigger and read-out
      for(uint32_t trig=0;trig<n_trigs; trig++){
        Trigger();

        auto start = chrono::steady_clock::now();
        //histogram it
        uint32_t nhits=0;
        while(true){
          for(auto fe : GetFEs()){
            if(!fe->HasHits()) continue;
            Hit* hit = fe->GetHit();
            if(hit!=0){
              occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
              dc_corr[fe->GetName()]->Fill(dc,FrontEnd::mode_dc_it[dc_mode][(hit->GetCol()-1)/2]);
              nhits++;
            }
            fe->NextHit();
          }
          auto end = chrono::steady_clock::now();
          uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
          if(ms>30){break;}
        }
        nhits2+=nhits;
        //cout << "NHits per trigger: " << nhits << endl;
      }
      cout << "NHits in step: " << nhits2 << endl;
      for(auto fe : GetFEs()){
        fe->SetRunMode(false);
        Send(fe);
      }
    }
  }

  cout << "Post-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0x0);
    fe->SetGlobalThreshold(mem_thres[fe->GetName()]);
    Send(fe);
  }

  //Results

  for(auto fe : GetFEs()){
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        occ_1D[fe->GetName()]->Fill(occ[fe->GetName()]->GetBinContent(col+1,row+1));
        if(abs(occ[fe->GetName()]->GetBinContent(col+1,row+1)-n_trigs) > (double)n_trigs * 0.5){
          fe->SetPixelEnable(col,row,false);
          mask[fe->GetName()]->Fill(col+1,row+1,1);
        }
      }
    }
    uint32_t highestVal = 0;
    uint32_t highestBin = 0;
    for (int32_t i=0;i < occ_1D[fe->GetName()]->GetNbinsX();i++){
      if (occ_1D[fe->GetName()]->GetBinContent(i) > highestVal){
        highestVal = occ_1D[fe->GetName()]->GetBinContent(i);
        highestBin = i;
      }
    }
    cout << "Peak at x=" << highestBin -1<< endl;
    mask[fe->GetName()]->Write();
    occ[fe->GetName()]->Write();
    occ_1D[fe->GetName()]->Write();
    dc_corr[fe->GetName()]->Write();

  }


}
