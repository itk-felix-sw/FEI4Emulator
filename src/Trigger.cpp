#include "FEI4Emulator/Trigger.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

Trigger::Trigger(){
  m_field1=0x1D;
}

Trigger::Trigger(Trigger *copy){
  m_field1=copy->m_field1;
}

Trigger::~Trigger(){}

Trigger * Trigger::Clone(){
  return new Trigger(this);
}

string Trigger::ToString(){
  ostringstream os;
  os << "Trigger";
  return os.str();
}
  
Position Trigger::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "Trigger::Unpack" << endl;
  Position newpos=FromByteArray(bytes,&m_field1,5,pos);
  //cout << "Unpacked field1: 0x" << hex << m_field1 << dec << endl; 
  if(m_field1!=0x1D and m_field1!=0x1C and m_field1!=0x1F and
     m_field1!=0x19 and m_field1!=0x15 and m_field1!=0x0D){
    //cout << "This is not a Trigger" << endl;
    return pos;
  }
  return newpos;
}

Position Trigger::Pack(uint8_t * bytes, Position pos){
  return ToByteArray(bytes,m_field1,5,pos);
}

uint32_t Trigger::GetType(){
  return Command::LVL1;
}

