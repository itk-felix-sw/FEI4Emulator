#include "FEI4Emulator/Command.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

vector<vector<uint32_t> > Command::m_masks={
  {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
  {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01},
  {0xC0,0x60,0x30,0x18,0x0C,0x06,0x03,0x01},
  {0xE0,0x70,0x38,0x1C,0x0E,0x07,0x03,0x01},
  {0xF0,0x78,0x3C,0x1E,0x0F,0x07,0x03,0x01},
  {0xF8,0x7C,0x3E,0x1F,0x0F,0x07,0x03,0x01},
  {0xFC,0x7E,0x3F,0x1F,0x0F,0x07,0x03,0x01},
  {0xFE,0x7F,0x3F,0x1F,0x0F,0x07,0x03,0x01},
  {0xFF,0x7F,0x3F,0x1F,0x0F,0x07,0x03,0x01}
};

vector<vector<uint32_t> > Command::m_sizes={
  {0,0,0,0,0,0,0,0},
  {1,1,1,1,1,1,1,1},
  {2,2,2,2,2,2,2,1},
  {3,3,3,3,3,3,2,1},
  {4,4,4,4,4,3,2,1},
  {5,5,5,5,4,3,2,1},
  {6,6,6,5,4,3,2,1},
  {7,7,6,5,4,3,2,1},
  {8,7,6,5,4,3,2,1}
};

vector<vector<uint32_t> > Command::m_rests={
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,1},
  {0,0,0,0,0,0,1,2},
  {0,0,0,0,0,1,2,3},
  {0,0,0,0,1,2,3,4},
  {0,0,0,1,2,3,4,5},
  {0,0,1,2,3,4,5,6},
  {0,1,2,3,4,5,6,7}
};

vector<vector<uint32_t> > Command::m_shifts={
  {0,0,0,0,0,0,0,0},
  {7,6,5,4,3,2,1,0},
  {6,5,4,3,2,1,0,1},
  {5,4,3,2,1,0,1,2},
  {4,3,2,1,0,1,2,3},
  {3,2,1,0,1,2,3,4},
  {2,1,0,1,2,3,4,5},
  {1,0,1,2,3,4,5,6},
  {0,1,2,3,4,5,6,7}
};

Command::Command(){
  m_debug=false;
}

Command::~Command(){}

Position Command::ToByteArray(uint8_t * bytes, 
                              uint32_t data, 
                              uint32_t tsize, 
                              Position pos){

  uint32_t size=tsize;
  uint32_t mask=0;
  uint32_t rest=0;
  uint32_t word=0;
  uint32_t shift=0;
  Position lastpos=pos+tsize;
  
  if(m_debug){
    cout << "----" << endl;
    cout << "TBA: "
         << " data: 0x" << hex << setw(2) << setfill('0') << data << dec 
         << " size: " << tsize
         << " from: " << pos
         << " until: " << lastpos
         << endl;
  }
  
  Position newpos=pos;
  while(pos<lastpos){
    
    shift=m_shifts[size][pos.GetBit()];
    rest=m_rests[size][pos.GetBit()];
    mask=m_masks[size][pos.GetBit()];
    size=m_sizes[size][pos.GetBit()];
    
    newpos+=size;

    if(rest==0){
      word=(data<<shift)&mask;
    }else{
      word=(data>>shift)&mask;
    }
    
    if(m_debug){ 
      cout << "Write:" 
           << " at: " << pos
           << " size: " << size 
           << " until: " << newpos
           << " mask: 0x" << hex << setw(2) << setfill('0') << mask << dec
           << " rest: " << rest
           << " shift: " << shift
           << " word: " << hex << setw(2) << setfill('0') << word << dec
           << endl;
    }
    
    bytes[pos.GetByte()]&=~mask;
    bytes[pos.GetByte()]|=word;


    size=rest;
    pos=newpos;
  }
  
  return pos;
  

}

Position Command::FromByteArray(uint8_t * bytes, 
                                uint32_t * data, 
                                uint32_t tsize, 
                                Position pos){
  
  data[0]=0;
  uint32_t size=tsize;
  uint32_t mask=0;
  uint32_t rest=0;
  uint32_t word=0;
  uint32_t shift=0;
  Position lastpos=pos+tsize;
  
  if(m_debug){
    cout << "----" << endl;
    cout << "FBA: "
         << " size: " << tsize
         << " from: " << pos
         << " until: " << lastpos
         << endl;
  }
  
  Position newpos=pos;
  while(pos<lastpos){
    
    shift=m_shifts[size][pos.GetBit()];
    rest=m_rests[size][pos.GetBit()];
    mask=m_masks[size][pos.GetBit()];
    size=m_sizes[size][pos.GetBit()];
    
    newpos+=size;

    if(rest==0){
      word=(bytes[pos.GetByte()]&mask)>>shift;
    }else{
      word=(bytes[pos.GetByte()]&mask)<<shift;
    }
    
    if(m_debug){
      cout << "Read:" 
           << " at: " << pos
           << " size: " << size 
           << " until: " << newpos
           << " mask: 0x" << hex << setw(2) << setfill('0') << mask << dec
           << " rest: " << rest
           << " shift: " << shift
           << " word: " << hex << setw(2) << setfill('0') << word << dec
           << endl;
    }
    
    data[0]|=word;

    size=rest;
    pos=newpos;
  }
  if(m_debug){
    cout << "FBA:"
         << " data: 0x" << hex << setw(2) << setfill('0') << data[0] << dec 
         << endl;
  }
  return pos;
  

}

