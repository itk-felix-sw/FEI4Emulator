#include "FEI4Emulator/ECR.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

ECR::ECR(){
  m_field1=0x16;
  m_field2=0x02;
}

ECR::ECR(ECR *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
}

ECR::~ECR(){}

ECR * ECR::Clone(){
  return new ECR(this);
}

string ECR::ToString(){
  ostringstream os;
  os << "ECR";
  return os.str();
}

Position ECR::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "ECR::Unpack" << endl;
  Position newpos = FromByteArray(bytes,&m_field1,5,pos);
  //cout << "Unpacked field1: 0x" << hex << m_field1 << dec << endl; 
  if(m_field1!=0x16){
    //cout << "This is not a ECR" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field2,4,newpos);
  if(m_field2!=0x02){
    //cout << "This is not a ECR" << endl;
    return pos;
  }
  return newpos;  
}

Position ECR::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1,5,pos);
  pos = ToByteArray(bytes,m_field2,4,pos);
  return pos;
}

uint32_t ECR::GetType(){
  return Command::ECR;
}

