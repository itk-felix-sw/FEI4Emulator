#include <felixbus/bus.hpp>
#include <felixbus/felixtable.hpp>
#include <felixbus/elinktable.hpp>
#include <FEI4Emulator/Emulator.h>
#include <cmdl/cmdargs.h>
#include <netio/netio.hpp>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>

using namespace std;

namespace felix::base{ 
  struct FromFELIXHeader { 
    FromFELIXHeader(uint16_t len, uint32_t elink) :
      //length(len+8), status(0), elinkid(elink&0x3F), gbtid(elinkid>>6){};
      //uint16_t length; uint16_t status; uint32_t elinkid:6; uint32_t gbtid:26;
      length(len+8), status(0), elinkid(elink){};
    uint16_t length; uint16_t status; uint32_t elinkid;
  };
  struct ToFELIXHeader {uint32_t length; uint32_t reserved; uint64_t elinkid; };
  struct L1AMsg {
    L1AMsg(uint32_t elink, uint32_t l1):length(sizeof(L1AMsg)),elinkid(elink),bcid(0),l1id(l1){};
    uint16_t length; uint16_t status; uint32_t elinkid;
    uint16_t fmt:16; uint8_t len:4; uint16_t bcid :12; uint32_t l1id;
    uint32_t orbit; uint16_t ttype; uint16_t res:16; uint32_t l0id;
  };
}

namespace daq{
  struct L1AInfo {
    L1AInfo(uint16_t bc, uint16_t tt, uint32_t l1, uint64_t c): bcid(bc),ttype(tt),l1id(l1),cnt(c){};
    uint16_t bcid; uint16_t ttype; uint32_t l1id; uint64_t cnt;
  };
}

using namespace std;

bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

void handleTrigger(uint32_t elinkT,
		   daq::L1AInfo&l1inf,
		   map<netio::tag,fei4b::Emulator*> &frontends,
		   netio::publish_socket *data_socket,
		   bool verbose){
  //if(verbose) cout << "Process event: " << l1inf.l1id << endl;
  felix::base::L1AMsg fmsg(elinkT,l1inf.l1id);
  netio::message l1msg((uint8_t*)&fmsg,sizeof(fmsg)); 
  data_socket->publish(elinkT,l1msg); 
  
  std::this_thread::sleep_for(std::chrono::milliseconds(10));

  map<netio::tag,fei4b::Emulator*>::iterator it;
  for(it=frontends.begin();it!=frontends.end();it++){
    fei4b::Emulator * emu=it->second;
    emu->ProcessTrigger();
    if(emu->GetLength()==0){continue;}
    felix::base::FromFELIXHeader hdr(emu->GetLength(),it->first);
    netio::message msg;   
    msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
    msg.add_fragment(emu->GetBytes(), emu->GetLength());
    data_socket->publish(it->first,msg); 
  }      
}

int main(int argc, char *argv[]){
  
  cout << "#####################################" << endl
       << "# Welcome to felix_emulator_2 fei4b #" << endl
       << "#####################################" << endl;
  
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgStr  cInterface('i',"interface","network-card","Default enp24s0f0");
  CmdArgInt  cCmdPort(  'c',"cmdport","number","Command port number. Default 12350");
  CmdArgInt  cDataPort( 'd',"dataport","number","Data port number. Default 12360");
  CmdArgInt  cElink0(   'e',"elink0","elink-number","Default 0");
  CmdArgInt  cElinkN(   'E',"elinkN","elink-number","Default 20");
  CmdArgInt  cElinkT(   'T',"elinkT","elink-number","Default 0x338");
  CmdArgInt  cMaxRate(  'r',"rate","rate","Rate in Hz. Default trigger on demand");
  
  CmdLine cmdl(*argv,&cVerbose,&cBackend,&cInterface,&cCmdPort,&cDataPort,
	       &cElink0,&cElinkN,&cElinkT,&cMaxRate,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
 
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"enp24s0f0");
  uint32_t port_cmd=(cCmdPort.flags()&CmdArg::GIVEN?cCmdPort:12350);
  uint32_t port_data=(cDataPort.flags()&CmdArg::GIVEN?cDataPort:12360);
  uint32_t elink0=(cElink0.flags()&CmdArg::GIVEN?cElink0:0);
  uint32_t elinkN=(cElinkN.flags()&CmdArg::GIVEN?cElinkN:20);
  uint32_t elinkT=(cElinkT.flags()&CmdArg::GIVEN?cElinkT:0x33B);
  uint32_t max_rate=(cMaxRate.flags()&CmdArg::GIVEN?cMaxRate:0);

  cout << "Create felix bus" << endl;
  felix::bus::Bus dbus;
  dbus.setVerbose(cVerbose);
  dbus.setZyreVerbose(cVerbose);

  cout << "Get interface" << endl;
  auto address=felix::bus::getIpAddress(interface);
  cout << " " << address.first << ":" << address.second << endl;  
  dbus.setInterface(address.first);
  
  cout << "Create felix table" << endl;
  felix::bus::FelixTable felixtable;
  stringstream ss;
  ss<<"tcp://"<<address.second<<":"<<port_data;
  cout << "Add felix: " << ss.str() << endl;
  string felixid=felixtable.addFelix(ss.str());

  cout << "Create elink table" << endl;
  felix::bus::ElinkTable elinktable;
  cout << "Enable e-link #" << elinkT << " (0x" << hex << elinkT << ")" << dec << endl;
  elinktable.addElink(elinkT,felixid);
  for(uint32_t elink=elink0;elink<=elinkN;elink++){
    cout << "Enable e-link #" << elink << " (0x" << hex << elink << ")" << dec << endl;
    elinktable.addElink(elink,felixid);
  }
  
  cout << "Connect" << endl;
  dbus.connect();
  cout << "Publish felixtable" << endl;
  dbus.publish("FELIX",felixtable);
  cout << "Publish elinktable" << endl;
  dbus.publish("ELINKS",elinktable);
      
  netio::context ctx(backend.c_str());
  netio::sockcfg cfg = netio::sockcfg::cfg();
  cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, 16);
  cfg(netio::sockcfg::PAGESIZE, 128);
  cfg(netio::sockcfg::FLUSH_INTERVAL_MILLISECS, 10);
 
  daq::L1AInfo l1a(0,0,0xFFFFFFFF,0);
  map<netio::tag,netio::endpoint> elinks;
  map<netio::tag,fei4b::Emulator*> frontends;
  map<netio::tag,mutex> locks;
  
  cout << "Create a publish socket" << endl;
  netio::publish_socket * data_socket=new netio::publish_socket(&ctx, port_data, cfg);
  
  data_socket->register_subscribe_callback([&](netio::tag t, netio::endpoint ep){
      cout << "Subscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
      if(t==elinkT){l1a.l1id=0xFFFFFFFF;return;}
      elinks[t]=ep;
      frontends[t]=new fei4b::Emulator(4);
      //frontends[t]->SetNHits(10);
      frontends[t]->SetVerbose(cVerbose);  
      locks[t].unlock();
  });

  data_socket->register_unsubscribe_callback([&](netio::tag t, netio::endpoint ep){
      cout << "Unsubscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
      if(t==elinkT){return;}
      elinks.erase(t);
      delete frontends[t];
      frontends.erase(t);
    });
  
  cout << "Create a receiver socket" << endl;
  netio::low_latency_recv_socket cmd_socket(&ctx, port_cmd, [&,elinkT](netio::endpoint &ep, netio::message &msg){
      if(cVerbose) cout << "New message received" << endl;
      vector<uint8_t> cmd = msg.data_copy();
      felix::base::ToFELIXHeader hdr; 
      memcpy(&hdr,(const void*)&cmd[0], sizeof(hdr));
      uint32_t pos=sizeof(hdr);
      //cout << "The super loop is not important, but this here is!" << endl;
      if(hdr.elinkid==elinkT){
        if(cVerbose) cout << "Handle trigger" << endl;
        l1a.l1id++;
        handleTrigger(elinkT,l1a,frontends,data_socket,cVerbose);
      }
      locks[hdr.elinkid].lock();
      if(frontends.count(hdr.elinkid)==0){return;}
      if(cVerbose) cout << "Handle command" << endl;
      frontends[hdr.elinkid]->HandleCommand(&cmd[pos],cmd.size()-pos);
      locks[hdr.elinkid].unlock();
    });
  
  g_cont=true;
  signal(SIGINT,handler);
  signal(SIGTERM,handler);

  //Context for netio
  cout << "Create the context for netio" << endl;
  std::thread t_context([&ctx](){ctx.event_loop()->run_forever();});
  
  //Data sender
  cout << "Loop" << endl;
  uint32_t milis=(max_rate==0?500:(1000./(float)max_rate));
  while(g_cont){
    if(max_rate!=0){
      l1a.l1id++;
      handleTrigger(elinkT,l1a,frontends,data_socket,cVerbose);
    }
    //if(cVerbose) cout << "Sleep for: " << milis << " ms" << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(milis));
  }
  
  cout << "Clean the house" << endl;
  //Cannot delete the data socket until netio is not fixed
  //cout << "delete data socket" << endl;
  //delete data_socket;
  cout << "Stop event loop" << endl;
  ctx.event_loop()->stop();
  cout << "Join context" << endl;
  t_context.join();  
  
  cout << "Delete front-ends" << endl;
  map<netio::tag,fei4b::Emulator*>::iterator it;
  for(it=frontends.begin();it!=frontends.end();it++){  
    delete it->second;
    frontends.erase(it);
  }
  
  cout << "Have a nice day" << endl;
  return 0;
}
