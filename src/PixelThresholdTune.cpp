#include "FEI4Emulator/PixelThresholdTune.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"
#include "TH1I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

PixelThresholdTune::PixelThresholdTune(){}

PixelThresholdTune::~PixelThresholdTune(){}

void PixelThresholdTune::Run(){

  uint32_t thr = 16;
  uint32_t mask_step = 4; //0,1,2,4,8,16
  uint32_t dc_mode = 3; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 1; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 100;
  uint32_t trig_delay = 50;
  uint32_t n_iterations = 0;
  
  float charge = GetCharge();
  bool use_scap = true;
  bool use_lcap = true;
  bool retune = GetRetune();


  //create histograms
  map<string,TH2I*> occ;
  map<string,map<uint32_t,TH2I*> > allOccs;
  map<string,TH2I*> tdac;
  map<string,TH1I*> iter;
  map<string,TH1I*> tdac_1d;
  map<string,TH2I*> tdac_vs_iter;
  map<string,TH2I*> occ_vs_iter;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    iter[fe->GetName()]=new TH1I(("iter_"+fe->GetName()).c_str(),";Iterations",11,-0.5,10+0.5);
    tdac[fe->GetName()]=new TH2I(("tdac_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    tdac_1d[fe->GetName()]=new TH1I(("tdac_1d_"+fe->GetName()).c_str(),";TDAC",32,0.5,32.5);
    tdac_vs_iter[fe->GetName()]=new TH2I(("tdac_vs_iter_"+fe->GetName()).c_str(),";Iter;TDAC",16,0.5,16.5,32,-0.5,31.5);
    occ_vs_iter[fe->GetName()]=new TH2I(("occ_vs_iter_"+fe->GetName()).c_str(),";Iter;Occupancy",16,0.5,16.5,10,-0.5,100.5);
  }

  map<string,uint32_t> tunedPixels;
  map<string,uint32_t> badPixels;
  map<string,map<uint32_t,map<uint32_t,uint32_t> > > mthr;
  map<string,map<uint32_t,map<uint32_t,uint32_t> > > mvhi;
  map<string,map<uint32_t,map<uint32_t,uint32_t> > > mvlo;
  map<string,map<uint32_t,map<uint32_t,uint32_t> > > mfix;
  map<string,map<uint32_t,map<uint32_t,map<uint32_t, float> > > > mpar;

  uint32_t vcal = Tools::toVcal(charge, use_scap, use_lcap);

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,15);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-trig_delay-5);
    fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
    fe->GetConfig()->SetField(Configuration::CalPulseWidth,20);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    fe->ConfigGlobal();
    Send(fe);

    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        if(not retune){
          fe->SetPixelThreshold(col,row,thr);
        }
        mthr[fe->GetName()][col][row]=fe->GetPixelThreshold(col,row);
        mfix[fe->GetName()][col][row]=0;
        mvhi[fe->GetName()][col][row]=31;
        mvlo[fe->GetName()][col][row]=0;
      }
    }
    if (not retune) fe->ConfigPixelThresholds();
    if (use_lcap) fe->EnableLCap();
    if (use_scap) fe->EnableSCap();
    fe->SetMask(3,0);
    Send(fe);
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger(trig_delay);

  //keep going until we converge or we give up
  while(true){

    //change the mask
    for(uint32_t mask=0; mask<mask_step; mask++){
      cout << "Mask: " << (mask+1) << "/" << mask_step << endl;

      for(auto fe : GetFEs()){
        fe->SetMask(mask_step,mask);
      }
      
      //select the double column
      for(uint32_t dc=0; dc<dc_max; dc++){
        cout << "DC: " << (dc+1) << "/" << dc_max << endl;
        for(auto fe : GetFEs()){
          fe->SelectDoubleColumn(dc_mode,dc);
          fe->SetRunMode(true);
          Send(fe);
        }
        uint32_t nhits2=0;

        //Trigger and read-out
        for(uint32_t trig=0;trig<n_trigs; trig++){

          Trigger();

          auto start = chrono::steady_clock::now();

          //histogram it
          uint32_t nhits=0;
          while(true){
            for(auto fe : GetFEs()){
              if(!fe->HasHits()) continue;
              Hit* hit = fe->GetHit();
              //cout << hit->ToString() << endl;
              if(hit and hit!=0){
                occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
                nhits++;
              }
              fe->NextHit();
            }
            auto end = chrono::steady_clock::now();
            uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
            if(ms>30){break;}
          }
          nhits2+=nhits;
          //cout << "NHits per trigger: " << nhits << endl;
        }

        cout << "NHits in step: " << nhits2 << endl;
        for(auto fe : GetFEs()){
          fe->SetRunMode(false);
          Send(fe);
        }
      }
    }

    bool all_done=true;
    n_iterations++;
    for(auto fe : GetFEs()){
      cout << fe->GetName() << " Analyse" << endl;
      iter[fe->GetName()]->Fill(n_iterations);
      tunedPixels[fe->GetName()] = 0;
      badPixels[fe->GetName()] = 0;
      for(uint32_t col=0;col<80;col++){
        for(uint32_t row=0;row<336;row++){
          uint32_t thr = mthr[fe->GetName()][col][row];
          uint32_t vlo = mvlo[fe->GetName()][col][row];
          uint32_t vhi = mvhi[fe->GetName()][col][row];
          uint32_t bin=occ[fe->GetName()]->GetBin(col+1,row+1);
          uint32_t cnt=occ[fe->GetName()]->GetBinContent(bin);
          if (fe->GetPixelMask(col,row)){
            //cnt = n_trigs/2;
          }
          float mean = cnt/(float)n_trigs;
          tdac_vs_iter[fe->GetName()]->Fill(n_iterations,thr);
          occ_vs_iter[fe->GetName()]->Fill(n_iterations,mean*100);
          if(mfix[fe->GetName()][col][row]==1){
            tunedPixels[fe->GetName()]++;
            continue;
          }
          /*
          cout << fe->GetName()
               << " col:" << col
               << " row:" << row
               << " thr:" << thr
               << " vlo:" << vlo
               << " vhi:" << vhi
               << " mean: " << mean
               << endl;
          */
          mpar[fe->GetName()][col][row][thr]=mean;

          if(mean >= 0.40 and mean < 0.60){
            mfix[fe->GetName()][col][row]=1;
            tunedPixels[fe->GetName()]++;
          }else{
            if      (mean >= 0.60) { vhi = thr; }
            else if (mean <  0.40) { vlo = thr; }
            thr = ((unsigned)(vhi + vlo))/2;
          }

          /*
          if(mean >= 0.90){
            if(thr<6){thr+=31;}
            thr-=6;
          }else if (mean >= 0.60 and mean < 0.90){
            if(thr<2){thr+=31;}
            thr-=2;
          }else if (mean >= 0.55 and mean < 0.60){
            if(thr<1){thr+=31;}
            thr--;
          }else if (mean >= 0.45 and mean < 0.55){
            mfix[fe->GetName()][col][row]=1;
            tunedPixels[fe->GetName()]++;
          }else if (mean >= 0.40 and mean < 0.45){
            thr++;
            thr&=0x1F;
          }else if (mean >= 0.10 and mean < 0.40){
            thr+=2;
            thr&=0x1F;
          }else if (mean < 0.10){
            thr+=6;
            thr&=0x1F;
          }
          */
          /*
          if(mean >= 0.40 and mean < 0.60){
            mfix[fe->GetName()][col][row]=1;
            tunedPixels[fe->GetName()]++;
          }else if (mean >= 0.60){
            if(thr==0){thr=32;}
            thr--; // this is now the correct way i think
          }else if (mean < 0.40){
            if(thr==31){thr=0;}
            thr++; // this is now the correct way i think
          }
          */
          mthr[fe->GetName()][col][row]=thr;
          mvlo[fe->GetName()][col][row]=vlo;
          mvhi[fe->GetName()][col][row]=vhi;
          fe->SetPixelThreshold(col,row,thr);
          all_done=false;

        }
      }
      
      allOccs[fe->GetName()][n_iterations] = (TH2I*) occ[fe->GetName()]->Clone();
      string s = fe->GetName() + "_Occ_it_" + to_string(n_iterations);
      allOccs[fe->GetName()][n_iterations]->SetName(s.c_str());
      allOccs[fe->GetName()][n_iterations]->GetZaxis()->SetRangeUser(0,1.5 * n_trigs);
      occ[fe->GetName()]->Reset();
      occ[fe->GetName()]->ResetStats();
      
      
      cout << fe->GetName() <<  " has tuned pixels: " << tunedPixels[fe->GetName()] << endl;
      cout << fe->GetName() <<  " has bad pixels: " << badPixels[fe->GetName()] << endl;
      fe->SetRunMode(false);
      fe->ConfigPixelThresholds();
      Send(fe);

      if(tunedPixels[fe->GetName()]>0.99*336*80){all_done=true;}

    }
    cout << "Iterations: " << n_iterations << endl;
    if (n_iterations > 16){
      all_done = true;
    }
    if(all_done) break;
  }

  //Results

  for(auto fe : GetFEs()){

    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        tdac[fe->GetName()]->Fill(col+1,row+1,mthr[fe->GetName()][col][row]);
        tdac_1d[fe->GetName()]->Fill(mthr[fe->GetName()][col][row]);
      }
    }
    for (unsigned int i=1;i<=allOccs[fe->GetName()].size();i++){
      allOccs[fe->GetName()][i]->Write();
    }
    iter[fe->GetName()]->Write();
    occ[fe->GetName()]->Write();
    tdac[fe->GetName()]->Write();
    tdac_1d[fe->GetName()]->Write();
    tdac_vs_iter[fe->GetName()]->Write();
    occ_vs_iter[fe->GetName()]->Write();
  }


}
