#include "FEI4Emulator/DataRecord.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

vector<vector<uint32_t> > DataRecord::m_hdc2tot={
  {{1,2,3,4,5,6,7,8,9,10,11,12,13,14,14,0}},
  {{2,3,4,5,6,7,8,9,10,11,12,13,14,15,1,0}},
  {{3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,0}}
};

vector<vector<uint32_t> > DataRecord::m_tot2hdc={
  {{15,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14}},
  {{15,14,0,1,2,3,4,5,6,7,8,9,10,11,12,13}},
  {{15,14,13,0,1,2,3,4,5,6,7,8,9,10,11,12}}
};

DataRecord::DataRecord(){
  m_col=0;
  m_row=0;
  m_tot1=0;
  m_tot2=0;
  m_hdc=0;
}

DataRecord::DataRecord(uint32_t col,uint32_t row,uint32_t tot1,uint32_t tot2,uint32_t hdc){
  m_col=col;
  m_row=row;
  m_tot1=tot1;
  m_tot2=tot2;
  m_hdc=hdc;
}

DataRecord::DataRecord(DataRecord *copy){
  m_col=copy->m_col;
  m_row=copy->m_row;
  m_tot1=copy->m_tot1;
  m_tot2=copy->m_tot2;
  m_hdc=copy->m_hdc;
}

DataRecord::~DataRecord(){}

string DataRecord::ToString(){
  ostringstream os;
  os << "DataRecord"
     << " Col: " << m_col
     << " Row: " << m_row
     << " Tot1: " << m_tot1
     << " Tot2: " << m_tot2;
  return os.str();
}
  
uint32_t DataRecord::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<3) return 0;
  
  m_col =(bytes[0]>>1)&0x7F;
  m_row =((bytes[0]&0x1)<<8) | ((bytes[1]>>0)&0xFF);
  m_tot1=(bytes[2]>>4)&0x0F;
  m_tot2=(bytes[2]>>0)&0x0F;
  m_tot1=m_hdc2tot[m_hdc][m_tot1];
  m_tot2=m_hdc2tot[m_hdc][m_tot2];

  //if(m_col<0x001 or m_col>0x050){return 0;} //error column out of range
  //if(m_row<0x001 or m_row>0x150){return 0;} //error row out of range
  
  return 3;
}

uint32_t DataRecord::Pack(uint8_t * bytes){
  uint32_t tot1=m_tot2hdc[m_hdc][m_tot1];
  uint32_t tot2=m_tot2hdc[m_hdc][m_tot2];
  bytes[0] =((m_col&0x7F)<<1) | ((m_row>>8)&0x01);
  bytes[1] =(m_row&0xFF)<<0;
  bytes[2] =(  tot1&0x0F)<<4;
  bytes[2]|=(  tot2&0x0F)<<0;
  return 3;
}

uint32_t DataRecord::GetType(){
  return Record::DATA_RECORD;
}

void DataRecord::SetColumn(uint32_t column){
  m_col=column;
}

uint32_t DataRecord::GetColumn(){
  return m_col;
}

void DataRecord::SetRow(uint32_t row){
  m_row=row;
}

uint32_t DataRecord::GetRow(){
  return m_row;
}
 
void DataRecord::SetTOT1(uint32_t tot){
  m_tot1=tot;
}

uint32_t DataRecord::GetTOT1(){
  return m_tot1;
}
  
void DataRecord::SetTOT2(uint32_t tot){
  m_tot2=tot;
}

uint32_t DataRecord::GetTOT2(){
  return m_tot2;
}

void DataRecord::SetHDC(uint32_t hdc){
  m_hdc=hdc;
}

uint32_t DataRecord::GetHDC(){
  return m_hdc;
}
  
