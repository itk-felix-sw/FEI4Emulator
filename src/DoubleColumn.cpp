#include "FEI4Emulator/DoubleColumn.h"
#include <iomanip>
#include <sstream>

using namespace std;
using namespace fei4b;

DoubleColumn::DoubleColumn(){
  for(uint32_t i=0; i<672; i++){
    m_pixels.push_back(new Pixel());
  }
}

DoubleColumn::~DoubleColumn(){
  for(auto p : m_pixels){
    delete p;
  }
}

void DoubleColumn::SetRegister(vector<bool> reg){
  m_register=reg;
}

void DoubleColumn::SetLatches(uint32_t latch){
  for(uint32_t i=0; i<m_pixels.size(); i++){
    m_pixels[i]->SetLatch(latch, m_register[i]);
  }
}

void DoubleColumn::SetRegister(uint32_t latch, vector<bool> reg){
  for(uint32_t i=0; i<m_pixels.size(); i++){
    m_pixels[i]->SetLatch(latch, reg[i]);
  }
}

vector<bool> DoubleColumn::GetRegister(uint32_t latch){
  vector<bool> v(672,0);
  for(uint32_t i=0; i<m_pixels.size(); i++){
    v[i]=m_pixels[i]->GetLatch(latch);
  }
  return v;
}

Pixel * DoubleColumn::GetPixel(uint32_t col, uint32_t row){
  uint32_t pos=(col==0?336+row:335-row);
  return m_pixels[pos];
}

Pixel * DoubleColumn::GetPixel(uint32_t pos){
  return m_pixels[pos];
}

void DoubleColumn::SetPixels(uint32_t type, vector<uint32_t> values){
  for(uint32_t i=0; i<m_pixels.size(); i++){m_pixels[i]->SetRegister(type,values[i]);}
}

void DoubleColumn::SetPixels(string name, vector<uint32_t> values){
  for(uint32_t i=0; i<m_pixels.size(); i++){m_pixels[i]->SetRegister(name,values[i]);}
}

void DoubleColumn::Shift(uint32_t latch){
  /*
  bool val=m_pixels[0]->GetLatch(latch);
  for(uint32_t i=1; i<m_pixels.size(); i++){
    m_pixels[i-1]->SetLatch(latch, m_pixels[i]->GetLatch(latch));
  }
  m_pixels[m_pixels.size()-1]->SetLatch(latch,val);
  */
  bool val=m_pixels[m_pixels.size()-1]->GetLatch(latch);
  for(uint32_t i=0; i<m_pixels.size()-1; i++){
    m_pixels[i+1]->SetLatch(latch, m_pixels[i]->GetLatch(latch));
  }
  m_pixels[0]->SetLatch(latch,val);
}
