#include "FEI4Emulator/TriggerAnalogScan.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

TriggerAnalogScan::TriggerAnalogScan(){}

TriggerAnalogScan::~TriggerAnalogScan(){}

void TriggerAnalogScan::Run(){

  uint32_t mask_step = 8; //0,1,2,4,8,16
  uint32_t dc_mode = 1; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 4; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 20;

  float charge = 50000;
  bool use_scap = true;
  bool use_lcap = true;

  uint32_t vcal = Tools::toVcal(charge, use_scap, use_lcap);

  map<string,uint32_t> mem_thres;

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,5); // maybe more needed?
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-50);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
    fe->GetConfig()->SetField(Configuration::CalPulseWidth,20); // before 5, maybe 20?
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelThreshold(col,row,16); 
      }
    }
    mem_thres[fe->GetName()]=fe->GetGlobalThreshold();
    fe->SetGlobalThreshold(100);
    fe->ConfigGlobal();
    fe->ConfigPixelThresholds();
    if (use_lcap) fe->EnableLCap();
    if (use_scap) fe->EnableSCap();
    fe->SetMask(3,0);
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger();

  //change the mask
  for(uint32_t mask=0; mask<mask_step; mask++){
    cout << "Mask: " << (mask+1) << "/" << mask_step << endl;

    for(auto fe : GetFEs()){
      fe->SetMask(mask_step,mask);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    //select the double column
    for(uint32_t dc=0; dc<dc_max; dc++){
      cout << "DC: " << (dc+1) << "/" << dc_max << endl;
      for(auto fe : GetFEs()){
        fe->SelectDoubleColumn(dc_mode,dc);
        fe->ConfigGlobal();
        fe->SetRunMode(true);
        Send(fe);
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
      //Trigger and read-out
      for(uint32_t trig=0;trig<n_trigs; trig++){
        if (mask == 1){
          Trigger();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(150));
      }
      for(auto fe : GetFEs()){
        fe->SetRunMode(false);
        Send(fe);
      }
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
