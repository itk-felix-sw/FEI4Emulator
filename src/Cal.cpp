#include "FEI4Emulator/Cal.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

Cal::Cal(){
  m_field1=0x16;
  m_field2=0x04;
}

Cal::Cal(Cal *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
}

Cal::~Cal(){}

Cal * Cal::Clone(){
  return new Cal(this);
}

string Cal::ToString(){
  ostringstream os;
  os << "Cal";
  return os.str();
}

Position Cal::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "Cal::Unpack" << endl;
  Position newpos = FromByteArray(bytes,&m_field1,5,pos);
  //cout << "Unpacked field1: 0x" << hex << m_field1 << dec << endl; 
  if(m_field1!=0x16){
    //cout << "This is not a Cal" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field2,4,newpos);
  if(m_field2!=0x04){
    //cout << "This is not a Cal" << endl;
    return pos;
  }
  return newpos;  
}

Position Cal::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1,5,pos);
  pos = ToByteArray(bytes,m_field2,4,pos);
  return pos;
}

uint32_t Cal::GetType(){
  return Command::CAL;
}

