#include <iostream>
#include <ctime>
#include "FEI4Emulator/Decoder.h"

using namespace std;
using namespace fei4b;

int main() {
  
  cout << "#################################" << endl
       << "# test_fei4b_decoder            #" << endl
       << "#################################" << endl;
  
  cout << "Create FEI4B decoder" << endl;
  Decoder *dec = new Decoder();

  cout << "Adding records to the encoder" << endl;  
  dec->AddRecord(new DataHeader(0,5));
  dec->AddRecord(new DataRecord(20,30,4,5));
  dec->AddRecord(new DataRecord(20,32,4,5));
  dec->AddRecord(new DataRecord(40,44,4,5));
  dec->AddRecord(new DataHeader(1,5));
  dec->AddRecord(new DataRecord(20,30,4,5));
  dec->AddRecord(new DataRecord(20,32,4,5));
  dec->AddRecord(new DataRecord(40,44,4,5));
  dec->AddRecord(new DataHeader(2,5));
  dec->AddRecord(new DataRecord(20,30,4,5));
  dec->AddRecord(new DataRecord(20,32,4,5));
  dec->AddRecord(new DataRecord(40,44,4,5));
  dec->AddRecord(ServiceRecord::DataCounter(1,0));
  dec->AddRecord(new DataHeader(0,5));
  dec->AddRecord(new DataRecord(20,30,4,5));
  dec->AddRecord(new DataRecord(20,32,4,5));
  dec->AddRecord(new DataRecord(40,44,4,5));
  dec->AddRecord(ServiceRecord::DataCounter(2,0));
  
  cout << "Decoded records: " << endl;
  for(uint32_t i=0;i<dec->GetRecords().size();i++){
    cout << "-- " << dec->GetRecords()[i]->ToString() << endl; 
  }
  
  cout << "Encode" << endl;
  dec->Encode();
  
  cout << "Byte stream: " << dec->GetByteString() << endl;

  cout << "Get the bytes" << endl;
  uint8_t *bytes = dec->GetBytes();
  uint32_t len = dec->GetLength();

  cout << "Set the bytes" << endl;
  dec->SetBytes(bytes,len);

  cout << "Decode" << endl;
  dec->Decode();
  
  cout << "Decoded records: " << endl;
  for(uint32_t i=0;i<dec->GetRecords().size();i++){
    cout << "-- " << dec->GetRecords()[i]->ToString() << endl; 
  }
    
  uint32_t n_test=1E6;
  clock_t start;
  double duration, frequency;
  
  cout << "Encoding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    dec->Encode(); 
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;

  
  cout << "Decoding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    dec->Decode(); 
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;
  
  
  cout << "Cleaning the house" << endl;
  delete dec;
  
  cout << "Have a nice day" << endl;
  return 0;
}
