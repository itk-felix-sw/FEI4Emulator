#include "FEI4Emulator/GlobalThresholdTune.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"
#include "TGraph.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

GlobalThresholdTune::GlobalThresholdTune(){}

GlobalThresholdTune::~GlobalThresholdTune(){}

void GlobalThresholdTune::Run(){

  
  uint32_t mask_step = 8; //0,1,2,4,8,16
  uint32_t dc_mode = 3; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 1; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 100;
  uint32_t n_iterations = 0;
  uint32_t trig_delay = 50;
  float charge = GetCharge();
  bool use_scap = true;
  bool use_lcap = true;
  bool retune = GetRetune();
  uint32_t thr = Tools::toVcal(charge/2);

  //create histograms
  map<string,TH2I*> occ;
  map<string,map<uint32_t,TH2I*> > allOccs;
  map<string,TH1I*> iter;
  map<string,TH1I*> l1id;
  map<string,TH1I*> bcid;
  map<string,TGraph*> par;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    iter[fe->GetName()]=new TH1I(("iter_"+fe->GetName()).c_str(),";Iterations",11,-0.5,10+0.5);
    l1id[fe->GetName()]=new TH1I(("l1id_"+fe->GetName()).c_str(),";L1ID",10001,-0.5,10000+0.5);
    bcid[fe->GetName()]=new TH1I(("bcid_"+fe->GetName()).c_str(),";BCID",0xDEC,-0.5,0xDEC+0.5);
    par[fe->GetName()]=new TGraph();
    par[fe->GetName()]->SetTitle(";Threshold [DAC];Occupancy [%]");
    par[fe->GetName()]->SetName(("OccVsThreshold_"+fe->GetName()).c_str());
  }

  map<string,uint32_t> mthr;
  map<string,uint32_t> mvlo;
  map<string,uint32_t> mvhi;
  map<string,map<uint32_t,float> > mpar;
  uint32_t vcal = Tools::toVcal(charge, use_scap, use_lcap);

  cout << "Setting target charge to " << charge << " electrons (" << vcal << " DACs)" << endl;
  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,15);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-trig_delay-5);
    fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
    fe->GetConfig()->SetField(Configuration::CalPulseWidth,20);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    if (not retune){
      fe->SetGlobalThreshold(thr);
    }
    mthr[fe->GetName()]=fe->GetGlobalThreshold();
    mvlo[fe->GetName()]=40;
    mvhi[fe->GetName()]=(2*mthr[fe->GetName()]+mvlo[fe->GetName()]);
    fe->ConfigGlobal();

    if (not retune){
      for(uint32_t col=0;col<80;col++){
        for(uint32_t row=0;row<336;row++){
          fe->SetPixelThreshold(col,row,16);
        }
      }
      fe->ConfigPixelThresholds();
    }
    if (use_lcap) fe->EnableLCap();
    if (use_scap) fe->EnableSCap();
    fe->SetMask(0,0);
    Send(fe);
    
    cout << fe->GetName() << " threshold: " << mthr[fe->GetName()] << endl;
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger(trig_delay);

  //keep going until we converge or we give up
  while(true){
    //change the mask
    for(uint32_t mask=0; mask<mask_step; mask++){
      cout << "Mask: " << (mask+1) << "/" << mask_step << endl;
      
      for(auto fe : GetFEs()){
        fe->SetMask(mask_step,mask);
      }
      
      //select the double column
      for(uint32_t dc=0; dc<dc_max; dc++){
        cout << "DC: " << (dc+1) << "/" << dc_max << endl;
        for(auto fe : GetFEs()){
          fe->SelectDoubleColumn(dc_mode,dc);
          fe->SetRunMode(true);
          Send(fe);
        }
        uint32_t nhits2=0;

        //Trigger and read-out
        for(uint32_t trig=0;trig<n_trigs; trig++){

          Trigger();

          auto start = chrono::steady_clock::now();

          //histogram it
          uint32_t nhits=0;
          while(true){
            for(auto fe : GetFEs()){
              if(!fe->HasHits()) continue;
              Hit* hit = fe->GetHit();
              //cout << hit->ToString() << endl;
              if(hit and hit!=0){
                occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
                l1id[fe->GetName()]->Fill(hit->GetL1ID());
                bcid[fe->GetName()]->Fill(hit->GetBCID());
                nhits++;
              }
              fe->NextHit();
            }
            auto end = chrono::steady_clock::now();
            uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
            if(ms>30){break;}
          }
          nhits2+=nhits;
          //cout << "NHits per trigger: " << nhits << endl;
        }
        cout << "NHits in step: " << nhits2 << endl;
        for(auto fe : GetFEs()){
          fe->SetRunMode(false);
          Send(fe);
        }
      }
    }

    bool all_done=true;
    n_iterations++;
    for(auto fe : GetFEs()){
      cout << fe->GetName() << " Analyse" << endl;
      iter[fe->GetName()]->Fill(n_iterations);
      uint32_t thr = mthr[fe->GetName()];
      uint32_t vlo = mvlo[fe->GetName()];
      uint32_t vhi = mvhi[fe->GetName()];
      
      float mean = occ[fe->GetName()]->Integral() / (80.0 * 336.0 * (double)n_trigs);
      cout << fe->GetName()
           << " thr:" << thr
           << " vlo:" << vlo
           << " vhi:" << vhi
           << " mean: " << mean
           << endl;
      
      mpar[fe->GetName()][thr]=mean;
      if(mean >= 0.49 and mean < 0.51){
        cout << fe->GetName() << " Cannot get better than this" << endl;
      }else{
        if (mean >= 0.51) {
          vlo = thr;
        }else if (mean < 0.49) {
          vhi = thr;
        }
        if ((vhi - vlo) > 1) {
          thr = ((unsigned)(vhi + vlo))/2;
          while(mpar[fe->GetName()].find(thr) != mpar[fe->GetName()].end()){
            thr += 1;
            if (thr >= 0xFFFF) thr = 0x1FF;
          }
          mthr[fe->GetName()]=thr;
          mvlo[fe->GetName()]=vlo;
          mvhi[fe->GetName()]=vhi;
          
          allOccs[fe->GetName()][n_iterations] = (TH2I*) occ[fe->GetName()]->Clone();
          string s = fe->GetName() + "_Occ_it_" + to_string(n_iterations);
          allOccs[fe->GetName()][n_iterations]->SetName(s.c_str());
          allOccs[fe->GetName()][n_iterations]->GetZaxis()->SetRangeUser(0,1.5 * n_trigs);
          
          occ[fe->GetName()]->Reset();
          occ[fe->GetName()]->ResetStats();
          all_done=false;
          cout << fe->GetName()
               << " thr:" << thr
               << " vlo:" << vlo
               << " vhi:" << vhi
               << " --- "
               << endl;
          //send the new value
          fe->SetGlobalThreshold(thr);
          fe->ConfigGlobal();
          Send(fe);
        }
        else {
          cout << fe->GetName() << " keep threshold: " << thr
               << " vlo:" << vlo << " vhi:" << vhi << endl;
        }
      }
    }
    if(all_done) break;
  }

  //Results

  for(auto fe : GetFEs()){
    for(auto it : mpar[fe->GetName()]){
      par[fe->GetName()]->SetPoint(par[fe->GetName()]->GetN(),it.first,it.second*100);
    }
    for (unsigned int i=1;i<=allOccs[fe->GetName()].size();i++){
      allOccs[fe->GetName()][i]->Write();
    }
    iter[fe->GetName()]->Write();
    l1id[fe->GetName()]->Write();
    bcid[fe->GetName()]->Write();
    occ[fe->GetName()]->Write();
    par[fe->GetName()]->Write();
  }


}
