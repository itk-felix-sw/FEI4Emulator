#include "FEI4Emulator/Encoder.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

Encoder::Encoder(){
  m_bytes.resize(100000,0);
  m_length = 0;
}

Encoder::~Encoder(){
  Clear();
}

void Encoder::AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len){
  for(uint32_t i=pos;i<pos+len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void Encoder::SetBytes(uint8_t *bytes, uint32_t len){
  m_length=0;
  for(uint32_t i=0;i<len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void Encoder::AddCommand(Command *cmd){
  m_cmds.push_back(cmd);
}
  
void Encoder::ClearBytes(){
  m_length=0;
}
  
void Encoder::ClearCommands(){

  while(!m_cmds.empty()){
    Command* cmd = m_cmds.back();
    m_cmds.pop_back();
    delete cmd;
  }

  m_cmds.clear();
}

void Encoder::Clear(){
  ClearBytes();
  ClearCommands();
}
  
string Encoder::GetByteString(){
  ostringstream os;
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos];
  }
  return os.str();
}

uint8_t * Encoder::GetBytes(){
  return m_bytes.data();
}
  
uint32_t Encoder::GetLength(){
  return m_length;
}
  
vector<Command*> & Encoder::GetCommands(){
  return m_cmds;
}

void Encoder::Encode(){
  Position pos;
  for(uint32_t i=0;i<m_cmds.size();i++){
    pos=m_cmds[i]->Pack(&m_bytes[0],pos);
  }
  if     (pos.GetBit()==1){m_bytes[pos.GetByte()] &= ~0x7F;}
  else if(pos.GetBit()==2){m_bytes[pos.GetByte()] &= ~0x3F;}
  else if(pos.GetBit()==3){m_bytes[pos.GetByte()] &= ~0x1F;}
  else if(pos.GetBit()==4){m_bytes[pos.GetByte()] &= ~0x0F;}
  else if(pos.GetBit()==5){m_bytes[pos.GetByte()] &= ~0x07;}
  else if(pos.GetBit()==6){m_bytes[pos.GetByte()] &= ~0x03;}
  else if(pos.GetBit()==7){m_bytes[pos.GetByte()] &= ~0x01;}
  m_length=pos.GetByte()+(pos.GetBit()>0?1:0);
  //cout << "Encoder::Encode Pos: " << pos << endl;
}

void Encoder::Decode(){
  ClearCommands();
  Position pos;
  Position newpos;
  uint32_t tnb=m_length;
  while(pos.GetByte()<tnb){
    //inspect the symbol
    Command* cmd=0;

    newpos=m_trg.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_trg.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_bcr.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_bcr.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_ecr.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_ecr.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_cal.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_cal.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_rdreg.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_rdreg.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_wrreg.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_wrreg.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_wrfe.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_wrfe.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_reset.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_reset.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_pulse.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_pulse.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_run.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_run.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    newpos=m_ukn.UnPack(&m_bytes[0],pos,tnb);
    if(newpos!=pos){cmd=m_ukn.Clone(); pos=newpos; m_cmds.push_back(cmd); continue;}

    pos+=1;
    
  }
  //cout << "Encoder::Decode Pos: " << pos << endl;
}
