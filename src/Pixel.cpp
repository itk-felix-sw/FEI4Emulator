#include "FEI4Emulator/Pixel.h"

using namespace std;
using namespace fei4b;

std::map<std::string, uint32_t> Pixel::m_name2index={
    {"Output",Output},
    {"Enable",Output},
    {"SmallCap",SmallCap},
    {"SCap",SmallCap},
    {"LargeCap",LargeCap},
    {"LCap",LargeCap},
    {"HitBus",HitBus},
    {"Hitbus",HitBus},
    {"TDAC",TDAC_0},
    {"TDAC_0",TDAC_0},
    {"TDAC_1",TDAC_1},
    {"TDAC_2",TDAC_2},
    {"TDAC_3",TDAC_3},
    {"TDAC_4",TDAC_4},
    {"FDAC",FDAC_0},
    {"FDAC_0",FDAC_0},
    {"FDAC_1",FDAC_1},
    {"FDAC_2",FDAC_2},
    {"FDAC_3",FDAC_3},
};

Pixel::Pixel(){
  m_masked=0;
  m_output=0;
  m_largecap=0;
  m_smallcap=0;
  m_hitbus=0;
  m_tdac=0;
  m_fdac=0;
}

Pixel::~Pixel(){}

void Pixel::SetRegister(uint32_t index, uint32_t value){
  switch(index){
  case  0: m_output  =value; break;
  case  6: m_largecap=value; break;
  case  7: m_smallcap=value; break;
  case  8: m_hitbus  =value; break;
  case  1:
  case  2:
  case  3:
  case  4:
  case  5: m_tdac    =value; break;
  case  9:
  case 10:
  case 11:
  case 12: m_fdac    =value; break;
  }
}

void Pixel::SetRegister(string name, uint32_t value){
  SetRegister(m_name2index[name],value);
}

void Pixel::SetLatch(uint32_t pos, bool value){
  switch(pos){
  case  0: m_output  =value; break;
  case  6: m_largecap=value; break;
  case  7: m_smallcap=value; break;
  case  8: m_hitbus  =value; break;
  case  1: if(value) {m_tdac|=0x10;} else { m_tdac&=0x0F; } break;
  case  2: if(value) {m_tdac|=0x08;} else { m_tdac&=0x17; } break;
  case  3: if(value) {m_tdac|=0x04;} else { m_tdac&=0x1B; } break;
  case  4: if(value) {m_tdac|=0x02;} else { m_tdac&=0x1D; } break;
  case  5: if(value) {m_tdac|=0x01;} else { m_tdac&=0x1E; } break;
  case  9: if(value) {m_fdac|=0x01;} else { m_fdac&=0x0E; } break;
  case 10: if(value) {m_fdac|=0x02;} else { m_fdac&=0x0D; } break;
  case 11: if(value) {m_fdac|=0x04;} else { m_fdac&=0x0B; } break;
  case 12: if(value) {m_fdac|=0x08;} else { m_fdac&=0x07; } break;
  }
}

bool Pixel::GetLatch(uint32_t pos){
  switch(pos){
    case  0: return m_output;      break;
    case  6: return m_largecap;    break;
    case  7: return m_smallcap;    break;
    case  8: return m_hitbus;      break;
    case  1: return (m_tdac&0x10); break;
    case  2: return (m_tdac&0x08); break;
    case  3: return (m_tdac&0x04); break;
    case  4: return (m_tdac&0x02); break;
    case  5: return (m_tdac&0x01); break;
    case  9: return (m_fdac&0x01); break;
    case 10: return (m_fdac&0x02); break;
    case 11: return (m_fdac&0x04); break;
    case 12: return (m_fdac&0x08); break;
  }
  return false;
}

uint32_t Pixel::GetRegister(uint32_t index){
  switch(index){
  case  0: return m_output; break;
  case  6: return m_largecap; break;
  case  7: return m_smallcap; break;
  case  8: return m_hitbus; break;
  case  1:
  case  2:
  case  3:
  case  4:
  case  5: return m_tdac; break;
  case  9:
  case 10:
  case 11:
  case 12: return m_fdac; break;
  }
  return 0;
}

uint32_t Pixel::GetRegister(string name){
  return GetRegister(m_name2index[name]);
}

uint32_t Pixel::GetTDAC(){
  return m_tdac;
}

bool Pixel::GetOutput(){
  return m_output;
}

bool Pixel::GetLargeCap(){
  return m_largecap;
}

bool Pixel::GetSmallCap(){
  return m_smallcap;
}

bool Pixel::GetHitBus(){
  return m_hitbus;
}

uint32_t Pixel::GetFDAC(){
  return m_fdac;
}

bool Pixel::GetMasked(){
  return m_masked;
}

void Pixel::SetTDAC(uint32_t tdac){
  m_tdac=tdac;
}

void Pixel::SetOutput(bool output){
  m_output=output;
}

void Pixel::SetLargeCap(bool largecap){
  m_largecap=largecap;
}

void Pixel::SetSmallCap(bool smallcap){
  m_smallcap=smallcap;
}

void Pixel::SetHitBus(bool hitbus){
  m_hitbus=hitbus;
}

void Pixel::SetFDAC(uint32_t fdac){
  m_fdac=fdac;
}

void Pixel::SetMasked(bool masked){
  m_masked=masked;
}
