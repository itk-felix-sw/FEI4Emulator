#include "FEI4Emulator/Reset.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

Reset::Reset(){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x08;
  m_field4=0x00;
}

Reset::Reset(uint32_t chipid){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x08;
  m_field4=chipid;
}
/*
Reset::Reset(Reset *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
  m_field3=copy->m_field3;
  m_field4=copy->m_field4;
}
*/
Reset::~Reset(){}

Reset * Reset::Clone(){
  return new Reset(GetChipID());
}

string Reset::ToString(){
  ostringstream os;
  os << "Reset";
  return os.str();
}

Position Reset::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "Reset::Unpack" << endl;
  Position newpos=pos;
  newpos = FromByteArray(bytes,&m_field1, 5,newpos);
  newpos = FromByteArray(bytes,&m_field2, 4,newpos);
  newpos = FromByteArray(bytes,&m_field3, 4,newpos);
  if(m_field1!=0x16 or m_field2!=0x08 or m_field3!=0x08){
    //cout << "This is not a Reset" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field4, 4,newpos);
  return newpos;  
}

Position Reset::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1, 5,pos);
  pos = ToByteArray(bytes,m_field2, 4,pos);
  pos = ToByteArray(bytes,m_field3, 4,pos);
  pos = ToByteArray(bytes,m_field4, 4,pos);
  return pos;
}

uint32_t Reset::GetType(){
  return Command::RESET;
}

void Reset::SetChipID(uint32_t chipid){
  m_field4=chipid;
}

uint32_t Reset::GetChipID(){
  return m_field4;
}

