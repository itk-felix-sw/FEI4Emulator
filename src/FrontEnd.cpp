#include "FEI4Emulator/FrontEnd.h"
#include "FEI4Emulator/Trigger.h"

#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;
using namespace fei4b;

map<uint32_t,uint32_t> FrontEnd::mode_dc_max = {{0,40},{1,4},{2,8},{3,1}};

map<uint32_t,map<uint32_t,uint32_t> > FrontEnd::mode_dc_it={
    {0,{{ 0, 0},{ 1, 1},{ 2, 2},{ 3, 3},{ 4, 4},{ 5, 5},{ 6, 6},{ 7, 7},{ 8, 8},{ 9, 9},
        {10,10},{11,11},{12,12},{13,13},{14,14},{15,15},{16,16},{17,17},{18,18},{19,19},
        {20,20},{21,21},{22,22},{23,23},{24,24},{25,25},{26,26},{27,27},{28,28},{29,29},
        {30,30},{31,31},{32,32},{33,33},{34,34},{35,35},{36,36},{37,37},{38,38},{39,39}}},
    {1,{{ 0, 0},{ 1, 1},{ 2, 2},{ 3, 3},
        { 4, 0},{ 5, 1},{ 6, 2},{ 7, 3},
        { 8, 0},{ 9, 1},{10, 2},{11, 3},
        {12, 0},{13, 1},{14, 2},{15, 3},
        {16, 0},{17, 1},{18, 2},{19, 3},
        {20, 0},{21, 1},{22, 2},{23, 3},
        {24, 0},{25, 1},{26, 2},{27, 3},
        {28, 0},{29, 1},{30, 2},{31, 3},
        {32, 0},{33, 1},{34, 2},{35, 3},
        {36, 0},{37, 1},{38, 2},{39, 3}}},
    {2,{{ 0, 0},{ 1, 1},{ 2, 2},{ 3, 3},{ 4, 4},{ 5, 5},{ 6, 6},{ 7, 7},
        { 8, 0},{ 9, 1},{10, 2},{11, 3},{12, 4},{13, 5},{14, 6},{15, 7},
        {16, 0},{17, 1},{18, 2},{19, 3},{20, 4},{21, 5},{22, 6},{23, 7},
        {24, 0},{25, 1},{26, 2},{27, 3},{28, 4},{29, 5},{30, 6},{31, 7},
        {32, 0},{33, 1},{34, 2},{35, 3},{36, 4},{37, 5},{38, 6},{39, 7}}},
    {3,{{ 0, 0},{ 1, 0},{ 2, 0},{ 3, 0},{ 4, 0},{ 5, 0},{ 6, 0},{ 7, 0},
        { 8, 0},{ 9, 0},{10, 0},{11, 0},{12, 0},{13, 0},{14, 0},{15, 0},
        {16, 0},{17, 0},{18, 0},{19, 0},{20, 0},{21, 0},{22, 0},{23, 0},
        {24, 0},{25, 0},{26, 0},{27, 0},{28, 0},{29, 0},{30, 0},{31, 0},
        {32, 0},{33, 0},{34, 0},{35, 0},{36, 0},{37, 0},{38, 0},{39, 0}}}};

FrontEnd::FrontEnd(){
  m_decoder = new Decoder();
  m_encoder = new Encoder();
  m_config  = new Configuration();
  for(uint32_t i=0;i<40;i++){m_dcs.push_back(new DoubleColumn());}
  m_verbose = 1;
  m_smallCap = 1.9;
  m_largeCap = 3.8;
  m_vcalOffset = 0;
  m_vcalSlope = 1.5;
  m_chipid = 0;
  m_name = "FEI4B";
  m_active=true;
}

FrontEnd::~FrontEnd(){
  delete m_decoder;
  delete m_encoder;
  delete m_config;
  for(auto p: m_dcs){delete p;}
}

void FrontEnd::SetVerbose(bool enable){
  m_verbose = enable;
}

void FrontEnd::SetChipID(uint32_t chipid){
  m_chipid=chipid;
}

uint32_t FrontEnd::GetChipID(){
  return m_chipid;
}

void FrontEnd::SetName(string name){
   m_name=name;
}

string FrontEnd::GetName(){
  return m_name;
}

Configuration * FrontEnd::GetConfig(){
  return m_config;
}

void FrontEnd::Clear(){
  m_encoder->Clear();
}

bool FrontEnd::IsActive(){
  return m_active;
}

void FrontEnd::SetActive(bool active){
  m_active=active;
}

uint32_t FrontEnd::GetGlobalThreshold(){
  uint32_t thr = 0;
  thr |= m_config->GetField(Configuration::Vthin_Coarse)->GetValue()<<8;
  thr |= m_config->GetField(Configuration::Vthin_Fine)->GetValue();
  return thr;
}

void FrontEnd::SetGlobalThreshold(uint32_t threshold){
  uint32_t thr_hi = (threshold>>8)&0xFF;
  uint32_t thr_lo = (threshold>>0)&0xFF;
  m_config->GetField(Configuration::Vthin_Coarse)->SetValue(thr_hi);
  m_config->GetField(Configuration::Vthin_Fine)->SetValue(thr_lo);
}

void FrontEnd::SetPixelEnable(uint32_t col, uint32_t row, bool enable){
  m_dcs[col/2]->GetPixel(col%2,row)->SetRegister(Pixel::Enable,enable);
}

bool FrontEnd::GetPixelEnable(uint32_t col, uint32_t row){
  return m_dcs[col/2]->GetPixel(col%2,row)->GetRegister(Pixel::Enable);
}

void FrontEnd::SetPixelMask(uint32_t col, uint32_t row, bool enable){
  m_dcs[col/2]->GetPixel(col%2,row)->SetMasked(enable);
}

bool FrontEnd::GetPixelMask(uint32_t col, uint32_t row){
  return m_dcs[col/2]->GetPixel(col%2,row)->GetMasked();
}

void FrontEnd::SetPixelHitBus(uint32_t col, uint32_t row, bool enable){
  m_dcs[col/2]->GetPixel(col%2,row)->SetRegister(Pixel::HitBus,enable);
}

bool FrontEnd::GetPixelHitBus(uint32_t col, uint32_t row){
  return m_dcs[col/2]->GetPixel(col%2,row)->GetRegister(Pixel::HitBus);
}

uint32_t FrontEnd::GetPixelThreshold(uint32_t col, uint32_t row){
  return m_dcs[col/2]->GetPixel(col%2,row)->GetRegister(Pixel::TDAC);
}

void FrontEnd::SetPixelThreshold(uint32_t col, uint32_t row, uint32_t threshold){
  m_dcs[col/2]->GetPixel(col%2,row)->SetRegister(Pixel::TDAC,threshold);
}

uint32_t FrontEnd::GetGlobalPreampFeedback(){
  return m_config->GetField(Configuration::PrmpVbpf)->GetValue();
}

void FrontEnd::SetGlobalPreampFeedback(uint32_t value){
  return m_config->GetField(Configuration::PrmpVbpf)->SetValue(value);
}

uint32_t FrontEnd::GetPixelPreampFeedback(uint32_t col, uint32_t row){
  return m_dcs[col/2]->GetPixel(col%2,row)->GetRegister(Pixel::FDAC);
}

void FrontEnd::SetPixelPreampFeedback(uint32_t col, uint32_t row, uint32_t value){
  m_dcs[col/2]->GetPixel(col%2,row)->SetRegister(Pixel::FDAC,value);
}

void FrontEnd::Trigger(uint32_t delay){
  //for(uint32_t i=0;i<3;i++){
  //  m_encoder->AddCommand(new Unknown());
  //}
  m_encoder->AddCommand(new BCR());
  if(delay!=0){
    m_encoder->AddCommand(new Cal());
    for(uint32_t i=0;i<delay;i++){
        m_encoder->AddCommand(new Unknown());
    }
  }
  
  m_encoder->AddCommand(new fei4b::Trigger());
  for(uint32_t i=0;i<7;i++){
    m_encoder->AddCommand(new Unknown());
  }
}

uint32_t FrontEnd::GetLength(){
  return m_encoder->GetLength();
}

uint8_t * FrontEnd::GetBytes(){
  return m_encoder->GetBytes();
}

void FrontEnd::ProcessCommands(){
  if(m_verbose){
    for(auto cmd: m_encoder->GetCommands()){
      cout << "Command: " << cmd->ToString() << endl;
    }
  }
  m_encoder->Encode();
  if(m_verbose){
    cout << "Byte Stream: " << m_encoder->GetByteString() << endl;
  }
}

void FrontEnd::SetRunMode(bool enable){
  m_encoder->AddCommand(new RunMode(m_chipid,(enable? RunMode::RUNMODE : RunMode::CONFMODE)));
}



void FrontEnd::SetMask(uint32_t mask_mode, uint32_t mask_iter){

  vector<bool> reg(672,0);
  for(uint32_t i=0;i<672;i++){
    if(mask_mode==0)continue;
    if((i+mask_iter)%mask_mode==0){reg[i]=true;}
  }
  
  for(auto dc : GetActiveDoubleColumns(3,0)){
    m_dcs[dc]->SetRegister(Pixel::Enable,reg);
  }

  m_config->GetField(Configuration::Colpr_Mode)->SetValue(3);
  m_config->GetField(Configuration::Colpr_Addr)->SetValue(0);

  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }

  m_encoder->AddCommand(new WrFE(m_chipid,reg));
  ConfigPixel(Pixel::Enable);

}

void FrontEnd::SelectDoubleColumn(uint32_t dc_mode, uint32_t dc){

  m_config->GetField(Configuration::Colpr_Mode)->SetValue(dc_mode);
  m_config->GetField(Configuration::Colpr_Addr)->SetValue(dc);

  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }

  //I thought I had read this in the manual,
  //but maybe I didn't
  //vector<bool> reg(672,1);
  //m_encoder->AddCommand(new WrFE(m_chipid,reg));

}

void FrontEnd::ShiftMask(){

  m_config->GetField(Configuration::S0)->SetValue(0);
  m_config->GetField(Configuration::S1)->SetValue(0);
  m_config->GetField(Configuration::SR_Clock)->SetValue(1);

  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }

  m_encoder->AddCommand(new Pulse(m_chipid,10));

  m_config->GetField(Configuration::SR_Clock)->SetValue(0);

  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }

}

void FrontEnd::ConfigGlobal(){
  for(auto addr : m_config->GetUpdatedRegisters()){
  //for(uint32_t addr=1;addr<36;addr++){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }
}

void FrontEnd::ReadGlobal(){
  for(uint32_t addr=1;addr<36;addr++){
    m_encoder->AddCommand(new RdReg(m_chipid,addr));
  }
}

void FrontEnd::ConfigPixels(){

  for(uint32_t dc=0; dc<m_dcs.size(); dc++){
    m_config->GetField(Configuration::Colpr_Mode)->SetValue(0);
    m_config->GetField(Configuration::Colpr_Addr)->SetValue(dc);

    for(auto addr : m_config->GetUpdatedRegisters()){
      m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
    }

    for(uint32_t i=0; i<13;i++){
      m_encoder->AddCommand(new WrFE(m_chipid,m_dcs[dc]->GetRegister(i)));
      ConfigPixel(i);
    }
  }
}

void FrontEnd::ConfigPixelThresholds(){

  for(uint32_t dc=0; dc<m_dcs.size(); dc++){
    m_config->GetField(Configuration::Colpr_Mode)->SetValue(0);
    m_config->GetField(Configuration::Colpr_Addr)->SetValue(dc);

    for(auto addr : m_config->GetUpdatedRegisters()){
      m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
    }

    for(uint32_t i=Pixel::TDAC_0; i<=Pixel::TDAC_4;i++){
      m_encoder->AddCommand(new WrFE(m_chipid,m_dcs[dc]->GetRegister(i)));
      ConfigPixel(i);
    }
  }
}

void FrontEnd::ConfigPixelPreampFeedback(){

  for(uint32_t dc=0; dc<m_dcs.size(); dc++){
    m_config->GetField(Configuration::Colpr_Mode)->SetValue(0);
    m_config->GetField(Configuration::Colpr_Addr)->SetValue(dc);

    for(auto addr : m_config->GetUpdatedRegisters()){
      m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
    }

    for(uint32_t i=Pixel::FDAC_3; i<=Pixel::FDAC_0;i++){
      m_encoder->AddCommand(new WrFE(m_chipid,m_dcs[dc]->GetRegister(i)));
      ConfigPixel(i);
    }
  }
}

void FrontEnd::ConfigPixelHitBus(){

  for(uint32_t dc=0; dc<m_dcs.size(); dc++){
    m_config->GetField(Configuration::Colpr_Mode)->SetValue(0);
    m_config->GetField(Configuration::Colpr_Addr)->SetValue(dc);

    for(auto addr : m_config->GetUpdatedRegisters()){
      m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
    }

    for(uint32_t i=Pixel::HitBus; i<=Pixel::HitBus;i++){
      m_encoder->AddCommand(new WrFE(m_chipid,m_dcs[dc]->GetRegister(i)));
      ConfigPixel(i);
    }
  }
}

void FrontEnd::ConfigDoubleColumn(uint32_t mode, uint32_t dc, uint32_t bit){

  m_config->GetField(Configuration::Colpr_Mode)->SetValue(mode);
  m_config->GetField(Configuration::Colpr_Addr)->SetValue(dc);

  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }

  for(uint32_t i=0; i<13;i++){
    if(bit!=13 and bit!=i){continue;}
    m_encoder->AddCommand(new WrFE(m_chipid,m_dcs[dc]->GetRegister(i)));
    ConfigPixel(i);
  }
}

void FrontEnd::ConfigPixel(uint32_t pos){

  m_config->GetField(Configuration::PixelLatch)->SetValue(1<<pos);
  m_config->GetField(Configuration::LatchEn)->SetValue(1);

  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }

  m_encoder->AddCommand(new Pulse(m_chipid,10));

  m_config->GetField(Configuration::PixelLatch)->SetValue(0);
  m_config->GetField(Configuration::LatchEn)->SetValue(0);

  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegister(addr)));
  }

}

void FrontEnd::LoadGlobalConfig(map<string,uint32_t> config){

  for (auto it=config.begin();it!=config.end();it++){
    Field* field=m_config->GetField(it->first);
    if(!field){
      cout << "Ignore field: " << it->first << endl;
      continue;
    }
    if(m_verbose) cout << "Set Field: " << it->first <<  " value: " << it->second << endl;
    field->SetValue(it->second);
  }

}

void FrontEnd::LoadPixelConfig(uint32_t column, uint32_t row, std::string name, uint32_t value){
  m_dcs[column/2]->GetPixel(column%2,row)->SetRegister(name,value);
}

uint32_t FrontEnd::GetPixelConfig(uint32_t column, uint32_t row, std::string name){
  return m_dcs[column/2]->GetPixel(column%2,row)->GetRegister(name);
}

void FrontEnd::EnableLCap(uint32_t mode, uint32_t dc){
  vector<bool> reg(672,true);
  for(auto dc : GetActiveDoubleColumns(mode,dc)){
    m_dcs[dc]->SetRegister(Pixel::LCap,reg);
  }
  ConfigDoubleColumn(mode,dc,Pixel::LCap);
}

void FrontEnd::EnableSCap(uint32_t mode, uint32_t dc){
  vector<bool> reg(672,true);
  for(auto dc : GetActiveDoubleColumns(mode,dc)){
    m_dcs[dc]->SetRegister(Pixel::SCap,reg);
  }
  ConfigDoubleColumn(mode,dc,Pixel::SCap);
}

Hit * FrontEnd::GetHit(){
  Hit * tmp = NULL;
  m_mutex.lock();
  tmp = m_hits.front();
  m_mutex.unlock();
  return tmp;
}

bool FrontEnd::NextHit(){
  if (m_hits.empty()) return false;
  m_mutex.lock();
  Hit * hit = m_hits.front();
  m_hits.pop_front();
  delete hit;
  m_mutex.unlock();
  return (!m_hits.empty());
}

bool FrontEnd::HasHits(){
  return (not m_hits.empty());
}

void FrontEnd::HandleData(uint8_t *recv_data, uint32_t recv_size){
  m_decoder->SetBytes(recv_data,recv_size);
  //if(m_verbose){cout << "Decode" << endl;}
  m_decoder->Decode();
  uint32_t addr=0;
  uint32_t cntr=0;
  Hit hit;
  for(auto record: m_decoder->GetRecords()){
    //cout << record->ToString() << endl;
    if(record->GetType()==Record::ADDRESS_RECORD){
      AddressRecord * rec=dynamic_cast<AddressRecord*>(record);
      addr=rec->GetAddress();
    }else if(record->GetType()==Record::VALUE_RECORD){
      ValueRecord * rec=dynamic_cast<ValueRecord*>(record);
      cout << "Update register"
           << " addr: " << setw(2) << addr
           << " value: 0x" << setw(4) << setfill('0') << hex << rec->GetValue() << dec << endl;
      m_config->SetRegister(addr,rec->GetValue());
    }else if(record->GetType()==Record::DATA_HEADER){
      DataHeader * rec=dynamic_cast<DataHeader*>(record);
      hit.Update(rec->GetL1ID(),rec->GetBCID());
    }else if(record->GetType()==Record::SERVICE_RECORD){
      ServiceRecord * rec=dynamic_cast<ServiceRecord*>(record);
      if(rec->GetCode()==14){
        /*cout << "MSB update: "
             << " Counter: 0x" << hex << rec->GetCounter() << dec
             << " L1ID: 0x" << hex << rec->GetL1IDbits(true) << dec
             << " BCID: 0x" << hex << rec->GetBCIDbits(true) << dec
             << endl;*/
        hit.Update(rec->GetL1IDbits(true),rec->GetBCIDbits(true),true);
      }
    }else if(record->GetType()==Record::DATA_RECORD){
      DataRecord * rec=dynamic_cast<DataRecord*>(record);
      cntr++;
      if(rec->GetTOT1()>0){
        Hit * nhit = hit.Clone();
        nhit->Set(rec->GetColumn(),rec->GetRow(),rec->GetTOT1());
        m_mutex.lock();
        m_hits.push_back(nhit);
        m_mutex.unlock();
      }
      if(rec->GetTOT2()>0){
        Hit * nhit = hit.Clone();
        nhit->Set(rec->GetColumn(),rec->GetRow()+1,rec->GetTOT2());
        m_mutex.lock();
        m_hits.push_back(nhit);
        m_mutex.unlock();
      }
    }
  }
}

std::vector<uint32_t> FrontEnd::GetActiveDoubleColumns(uint32_t mode, uint32_t addr){
  vector<uint32_t> ret;
  if(mode==0xFF){mode=m_config->GetField(Configuration::Colpr_Mode)->GetValue();}
  if(addr==0xFF){addr=m_config->GetField(Configuration::Colpr_Addr)->GetValue();}
  if(mode==0){
    ret.push_back(addr);
  }else if(mode==1){
    for(uint32_t i=0;i<10;i++){
      ret.push_back(addr+4*i);
    }
  }else if(mode==2){
    for(uint32_t i=0;i<5;i++){
      ret.push_back(addr+8*i);
    }
  }else if(mode==3){
    for(uint32_t i=0;i<40;i++){
      ret.push_back(i);
    }
  }

  return ret;
}

void FrontEnd::Reset(){
  m_encoder->AddCommand(new BCR());
  m_encoder->AddCommand(new ECR());
}
