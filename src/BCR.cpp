#include "FEI4Emulator/BCR.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

BCR::BCR(){
  m_field1=0x16;
  m_field2=0x01;
}

BCR::BCR(BCR *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
}

BCR::~BCR(){}

BCR * BCR::Clone(){
  return new BCR(this);
}

string BCR::ToString(){
  ostringstream os;
  os << "BCR";
  return os.str();
}

Position BCR::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "BCR::Unpack" << endl;
  Position newpos = FromByteArray(bytes,&m_field1,5,pos);
  //cout << "Unpacked field1: 0x" << hex << m_field1 << dec << endl; 
  if(m_field1!=0x16){
    //cout << "This is not a BCR" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field2,4,newpos);
  if(m_field2!=0x01){
    //cout << "This is not a BCR" << endl;
    return pos;
  }
  return newpos;  
}

Position BCR::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1,5,pos);
  pos = ToByteArray(bytes,m_field2,4,pos);
  return pos;
}

uint32_t BCR::GetType(){
  return Command::BCR;
}

