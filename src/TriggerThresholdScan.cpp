#include "FEI4Emulator/TriggerThresholdScan.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"
#include "TH1I.h"
#include "TF1.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

TriggerThresholdScan::TriggerThresholdScan(){}

TriggerThresholdScan::~TriggerThresholdScan(){}

void TriggerThresholdScan::Run(){

  uint32_t mask_step = 4; //0,1,2,4,8,16
  uint32_t dc_mode = 3; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 1; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 10;
  bool use_scap = true;
  bool use_lcap = true;
  uint32_t vcal_min = Tools::toVcal(30000, use_scap, use_lcap);;
  uint32_t vcal_max = Tools::toVcal(50000, use_scap, use_lcap);;
  uint32_t vcal_stp = 5;
  cout << "Scanning from vcal_min=" << vcal_min << " up to vcal_max=" << vcal_max << " with a step width=" << vcal_stp << "... Enjoy the ride!" << endl;
  uint32_t totalTriggers = 0;

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,15);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-50-5);
    fe->GetConfig()->SetField(Configuration::CalPulseWidth,20);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    fe->ConfigGlobal();
    Send(fe);

    if (use_lcap) fe->EnableLCap();
    if (use_scap) fe->EnableSCap();
    fe->SetMask(0,0);
    Send(fe);
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger(50);

  //keep going until we converge or we give up
  for(uint32_t vcal=vcal_min; vcal<vcal_max; vcal+=vcal_stp){
    cout << "Vcal: " << vcal << endl;
    for(auto fe : GetFEs()){
      fe->SetRunMode(false);
      fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
      fe->ConfigGlobal();
      Send(fe);
      
    }

    //change the mask
    for(uint32_t mask=0; mask<mask_step; mask++){
      cout << "Mask: " << (mask+1) << "/" << mask_step << endl;

      for(auto fe : GetFEs()){
        fe->SetMask(mask_step,mask);
      }

      //select the double column
      for(uint32_t dc=0; dc<dc_max; dc++){
        cout << "DC: " << (dc+1) << "/" << dc_max << endl;
        for(auto fe : GetFEs()){
          fe->SelectDoubleColumn(dc_mode,dc);
          fe->ConfigGlobal();
          fe->SetRunMode(true);
          Send(fe);
        }
        

        //Trigger and read-out
        for(uint32_t trig=0;trig<n_trigs; trig++){
          Trigger();
          cout << "Triggered for the " << totalTriggers++ << "th time..." << endl;
          std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
        for(auto fe : GetFEs()){
          fe->SetRunMode(false);
          Send(fe);
        }
      }
    }
  }
}
