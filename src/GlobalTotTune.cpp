#include "FEI4Emulator/GlobalTotTune.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

GlobalTotTune::GlobalTotTune(){}

GlobalTotTune::~GlobalTotTune(){}

void GlobalTotTune::Run(){

  uint32_t tt_ini = 100;
  uint32_t tt_min =  64;
  uint32_t tt_max = 128;
  float target_tot = 7;
  float target_tot_err = 0.1;
  float target_tot_min = target_tot - target_tot_err;
  float target_tot_max = target_tot + target_tot_err;

  uint32_t mask_step = 4; //0,1,2,4,8,16
  uint32_t dc_mode = 1; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 4; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 3;
  float charge = GetCharge();
  bool retune = GetRetune();
  bool use_scap = true;
  bool use_lcap = true;


  //create histograms
  map<string,TH1I*> tot;
  for(auto fe : GetFEs()){
    tot[fe->GetName()]=new TH1I(("tot_"+fe->GetName()).c_str(),";TOT",16,-0.5,16.5);
  }

  map<string,uint32_t> mthr;
  map<string,uint32_t> mvlo;
  map<string,uint32_t> mvhi;
  map<string,map<uint32_t,float> > mpar;
  uint32_t vcal = Tools::toVcal(charge, use_scap, use_lcap);

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,5);
    fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    if(not retune){
      fe->SetGlobalPreampFeedback(tt_ini);
    }
    mthr[fe->GetName()]=fe->GetGlobalPreampFeedback();
    mvlo[fe->GetName()]=tt_min;
    mvhi[fe->GetName()]=tt_max;
    fe->SetMask(3,0);
    Send(fe);
    cout << fe->GetName() << " threshold: 0x" << hex << mthr[fe->GetName()] << dec << endl;
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger();//Might need to do something here

  //keep going until we converge or we give up
  while(true){

    //change the mask
    for(uint32_t mask=0; mask<mask_step; mask++){
      cout << "Mask: " << (mask+1) << "/" << mask_step << endl;

      for(auto fe : GetFEs()){
        fe->SetMask(mask_step,mask);
      }
      
      //select the double column
      for(uint32_t dc=0; dc<dc_max; dc++){
        cout << "DC: " << (dc+1) << "/" << dc_max << endl;
        for(auto fe : GetFEs()){
          fe->SelectDoubleColumn(dc_mode,dc);
          fe->SetRunMode(true);
          Send(fe);
        }
        uint32_t nhits2=0;

        //Trigger and read-out
        for(uint32_t trig=0;trig<n_trigs; trig++){

          Trigger();

          auto start = chrono::steady_clock::now();

          //histogram it
          uint32_t nhits=0;
          while(true){
            for(auto fe : GetFEs()){
              if(!fe->HasHits()) continue;
              Hit* hit = fe->GetHit();
              //cout << hit->ToString() << endl;
              if(hit and hit!=0){
                tot[fe->GetName()]->Fill(hit->GetTOT());
                nhits++;
              }
              fe->NextHit();
            }
            auto end = chrono::steady_clock::now();
            uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
            if(ms>50){break;}
          }
          nhits2+=nhits;
          //cout << "NHits per trigger: " << nhits << endl;
        }
        cout << "NHits in step: " << nhits2 << endl;
        for(auto fe : GetFEs()){
          fe->SetRunMode(false);
          Send(fe);
        }
      }
    }

    bool all_done=true;
    for(auto fe : GetFEs()){
      cout << fe->GetName() << " Analyse" << endl;
      uint32_t thr = mthr[fe->GetName()];
      uint32_t vlo = mvlo[fe->GetName()];
      uint32_t vhi = mvhi[fe->GetName()];
      float mean = tot[fe->GetName()]->GetMean();
      cout << fe->GetName()
           << " thr:" << thr
           << " vlo:" << vlo
           << " vhi:" << vhi
           << " mean: " << mean
           << endl;
      mpar[fe->GetName()][thr]=mean;
      if(mean >= target_tot_min and mean < target_tot_max){
        cout << fe->GetName() << " Cannot get better than this" << endl;
      }else{
        if (mean >= target_tot_max) {
          vlo = thr;
        }else if (mean < target_tot_min) {
          vhi = thr;
        }
        if ((vhi - vlo) > 1) {
          thr = ((unsigned)(vhi + vlo))/2;
          while(mpar[fe->GetName()].find(thr) != mpar[fe->GetName()].end()){
            thr += 1;
            if (thr >= 0xFF) thr = 0xFF/2;
          }
          mthr[fe->GetName()]=thr;
          mvlo[fe->GetName()]=vlo;
          mvhi[fe->GetName()]=vhi;
          tot[fe->GetName()]->Reset();
          tot[fe->GetName()]->ResetStats();
          cout << fe->GetName()
               << " thr:" << thr
               << " vlo:" << vlo
               << " vhi:" << vhi
               << " --- "
               << endl;
          //send the new value
          fe->SetGlobalPreampFeedback(thr);
          Send(fe);
          all_done=false;
        }
        else {
          cout << fe->GetName() << " keep threshold: " << thr
               << " vlo:" << vlo << " vhi:" << vhi << endl;
        }
      }
    }
    if(all_done) break;
  }

  //Results

  for(auto fe : GetFEs()){
    //tot[fe->GetName()]->SaveAs(("Tot_"+fe->GetName()+".C").c_str());
    tot[fe->GetName()]->Write();
  }


}
