#include <iostream>
#include <ctime>
#include "FEI4Emulator/Encoder.h"

using namespace std;
using namespace fei4b;

int main() {
  
  cout << "#################################" << endl
       << "# test_fei4b_encoder            #" << endl
       << "#################################" << endl;

  cout << "Create FEI4B encoder" << endl;
  //create encoder
  Encoder *enc = new Encoder();
  //add a command
  cout << "Adding commands to the encoder" << endl;


  enc->AddCommand(new WrReg(0xA,0xA,0xAAAA));
  enc->AddCommand(new WrReg(0xF,0xF,0xFFFF));
  enc->AddCommand(new BCR());
  enc->AddCommand(new Trigger());
  enc->AddCommand(new Trigger());
  enc->AddCommand(new ECR());
  enc->AddCommand(new Trigger());
  enc->AddCommand(new Trigger());
  enc->AddCommand(new Cal());
  enc->AddCommand(new Unknown());
  enc->AddCommand(new Unknown());
  enc->AddCommand(new Trigger());
  enc->AddCommand(new RunMode(8,RunMode::CONFMODE));
  enc->AddCommand(new RdReg(0,0xF));
  enc->AddCommand(new Reset(0));

  enc->Encode();
  enc->Clear();

  enc->AddCommand(new WrReg(4,22,0x0000));
  enc->AddCommand(new WrFE (4,vector<bool>(672,0)));
  enc->AddCommand(new WrReg(4,13,0x0000));
  enc->AddCommand(new WrReg(4,27,0x0002));
  enc->AddCommand(new Pulse(4,10));
  enc->AddCommand(new WrReg(4,13,0x0000));
  enc->AddCommand(new WrReg(4,27,0x0000));
  enc->AddCommand(new RunMode(8,RunMode::RUNMODE));
  enc->AddCommand(new RunMode(8,RunMode::CONFMODE));

  enc->Encode();
  enc->Clear();

  enc->AddCommand(new RunMode(4,RunMode::CONFMODE));
  enc->AddCommand(new WrReg(4,22,0x0200));
  enc->AddCommand(new WrFE (4,vector<bool>(672,0)));
  enc->AddCommand(new WrReg(4,13,0x0001));
  enc->AddCommand(new WrReg(4,27,0x0004));
  enc->AddCommand(new Pulse(4,10));
  enc->AddCommand(new WrReg(4,13,0x0000));
  enc->AddCommand(new WrReg(4,27,0x0000));
  enc->AddCommand(new RunMode(4,RunMode::RUNMODE));

  //check the contents
  cout << "Encoded commands: " << endl;
  for(uint32_t i=0;i<enc->GetCommands().size();i++){
    cout << "-- " << enc->GetCommands()[i]->ToString() << endl; 
  }
  
  cout << "Encode" << endl;
  enc->Encode();
  cout << "Byte stream: " << enc->GetByteString() << endl;

  cout << "Get the bytes (for FELIX)" << endl;
  uint8_t *bytes = enc->GetBytes();
  uint32_t len = enc->GetLength();
  cout << " Length: " << len << endl;

  enc->Clear();

  cout << "Set the bytes (for emulator)" << endl;
  enc->SetBytes(bytes,len);

  cout << "Decode" << endl;
  enc->Decode();
  
  cout << "Decoded commands: " << endl;
  for(uint32_t i=0;i<enc->GetCommands().size();i++){
    cout << "-- " << enc->GetCommands()[i]->ToString() << endl; 
  }

  uint32_t n_test=1E6;
  clock_t start;
  double duration, frequency;
  
  cout << "Econding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    enc->Encode(); 
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;

  
  cout << "Decoding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    enc->Decode(); 
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;
  
  cout << "Cleaning the house" << endl;
  delete enc;
  
  cout << "Have a nice day" << endl;
  return 0;
}

