#include "FEI4Emulator/ValueRecord.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

ValueRecord::ValueRecord(){
  m_id=0xEC;
  m_value=0;
}

ValueRecord::ValueRecord(ValueRecord *copy){
  m_id=copy->m_id;
  m_value=copy->m_value;
}

ValueRecord::ValueRecord(uint32_t value){
  m_id=0xEC;
  m_value=value;
}

ValueRecord::~ValueRecord(){}

string ValueRecord::ToString(){
  ostringstream os;
  os << "ValueRecord"
     << " Value: " << m_value;
  return os.str();
}
  
uint32_t ValueRecord::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<3) return 0;
  if(bytes[0]!=0xEC and
     bytes[0]!=0xE4 and
     bytes[0]!=0xFC and
     bytes[0]!=0xCC and
     bytes[0]!=0xAC /*and
     bytes[0]!=0x6C*/){
    //cout << "Value record not recognized: 0x" << hex << (uint32_t)bytes[0] << dec << endl;
    return 0;
  }
  m_id    = bytes[0];
  m_value =(bytes[1]&0xFF)<<8;
  m_value|=(bytes[2]&0xFF)<<0;
  return 3;
}

uint32_t ValueRecord::Pack(uint8_t * bytes){
  bytes[0] =m_id;
  bytes[1] =(m_value>>8)&0xFF;
  bytes[2] =(m_value>>0)&0xFF;
  return 3;
}

uint32_t ValueRecord::GetType(){
  return Record::VALUE_RECORD;
}

void ValueRecord::SetValue(uint32_t value){
  m_value=value;
}

uint32_t ValueRecord::GetValue(){
  return m_value;
}
 
