#include "FEI4Emulator/WrFE.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

WrFE::WrFE(){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x04;
  m_field4=0x00;
  m_field5=0x00;
  m_field6.resize(672,0);
}

WrFE::WrFE(WrFE *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
  m_field3=copy->m_field3;
  m_field4=copy->m_field4;
  m_field5=copy->m_field5;
  m_field6=copy->m_field6;
}

WrFE::WrFE(uint32_t chipid, vector<bool> values){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x04;
  m_field4=chipid;
  m_field5=0x00;
  m_field6=values;
}

WrFE::~WrFE(){}

WrFE * WrFE::Clone(){
  return new WrFE(this);
}

string WrFE::ToString(){
  ostringstream os;
  os << "WrFE  "
     << "chipid: " << m_field4 << " "
     << "values: 0x";
  for(uint32_t i=0;i<672;i+=4){
    uint32_t val=0;
    for(uint32_t j=0;j<4;j++){
      val|=m_field6[i+j]<<j;
    }
    os << hex << val;
  }
  return os.str();
}

Position WrFE::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "WrFE::Unpack" << endl;
  Position newpos=pos;
  newpos = FromByteArray(bytes,&m_field1, 5,newpos);
  newpos = FromByteArray(bytes,&m_field2, 4,newpos);
  newpos = FromByteArray(bytes,&m_field3, 4,newpos);
  if(m_field1!=0x16 or m_field2!=0x08 or m_field3!=0x04){
    //cout << "This is not a WrFE" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field4, 4,newpos);
  newpos = FromByteArray(bytes,&m_field5, 6,newpos);
  for(uint32_t i=0;i<672;i++){
    uint32_t word=0;
    newpos = FromByteArray(bytes,&word,1,newpos);
    m_field6[i]=word&0x1;
  }
  return newpos;  
}

Position WrFE::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1, 5,pos);
  pos = ToByteArray(bytes,m_field2, 4,pos);
  pos = ToByteArray(bytes,m_field3, 4,pos);
  pos = ToByteArray(bytes,m_field4, 4,pos);
  pos = ToByteArray(bytes,m_field5, 6,pos);
  for(uint32_t i=0;i<672;i++){
    pos = ToByteArray(bytes,m_field6[i],1,pos);
  }
  return pos;
}

uint32_t WrFE::GetType(){
  return Command::WRFE;
}

void WrFE::SetChipID(uint32_t chipid){
  m_field4=chipid;
}

uint32_t WrFE::GetChipID(){
  return m_field4;
}

void WrFE::SetAddress(uint32_t address){
  m_field5=address;
}

uint32_t WrFE::GetAddress(){
  return m_field5;
}

void WrFE::SetValue(uint32_t bit, bool enable){
  if(bit>=m_field6.size()) return;
  m_field6[bit]=enable;
}

bool WrFE::GetValue(uint32_t bit){
  if(bit>=m_field6.size()) return false;
  return m_field6[bit];
}

vector<bool> WrFE::GetValues(){
  return m_field6;
}
