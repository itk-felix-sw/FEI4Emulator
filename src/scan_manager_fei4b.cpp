#include <FEI4Emulator/Handler.h>
#include <FEI4Emulator/RegisterScan.h>
#include <FEI4Emulator/DigitalScan.h>
#include <FEI4Emulator/AnalogScan.h>
#include <FEI4Emulator/SelfTriggerScan.h>
#include <FEI4Emulator/NoiseScan.h>
#include <FEI4Emulator/GlobalThresholdTune.h>
#include <FEI4Emulator/PixelThresholdTune.h>
#include <FEI4Emulator/ThresholdScan.h>
#include <FEI4Emulator/GlobalTotTune.h>
#include <FEI4Emulator/PixelTotTune.h>
#include <FEI4Emulator/TotScan.h>
#include <FEI4Emulator/OliverScan.h>

#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>

using namespace std;
using namespace fei4b;

bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to scan_manager_fei4b     #" << endl
       << "#####################################" << endl;

  CmdArgStr  cScan(     's',"scan","scan","scan name",CmdArg::isREQ);
  CmdArgStrList cConfs( 'c',"config","chips","chip names",CmdArg::isREQ);
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgBool cConfig(   'C',"configure","configure the front-ends");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgStr  cInterface('i',"interface","network-card","Default enp24s0f0");
  CmdArgStr  cNetFile(  'n',"netfile","file","Network file. Default network.json");
  CmdArgInt  cCharge(   'q',"charge","electrons","Injection charge");
  CmdArgBool cRetune(   'r',"retune","enable retune mode");
  CmdArgStr  cOutPath(  'o',"outpath","path","output path. Default $ITK_DATA_PATH");

  CmdLine cmdl(*argv,&cScan,&cConfs,&cConfig,&cVerbose,&cBackend,&cInterface,&cNetFile,&cCharge,&cRetune,&cOutPath,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string scan(cScan);
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"enp24s0f0");
  string netfile=(cNetFile.flags()&CmdArg::GIVEN?cNetFile:"connectivity.json");

  Handler * handler = 0;

  if     (scan.find("Reg")!=string::npos) {handler=new RegisterScan();}
  else if(scan.find("Oliver")!=string::npos) {handler=new OliverScan();}
  else if(scan.find("Dig")!=string::npos) {handler=new DigitalScan();}
  else if(scan.find("Ana")!=string::npos) {handler=new AnalogScan();}
  else if(scan.find("Noi")!=string::npos) {handler=new NoiseScan();}
  else if(scan.find("Glob")!=string::npos and
          scan.find("Thr")!=string::npos){handler=new GlobalThresholdTune();}
  else if(scan.find("Pix")!=string::npos and
          scan.find("Thr")!=string::npos){handler=new PixelThresholdTune();}
  else if(scan.find("Thr")!=string::npos and
          scan.find("Scan")!=string::npos){handler=new ThresholdScan();}
  else if(scan.find("Glob")!=string::npos and
          scan.find("Tot")!=string::npos){handler=new GlobalTotTune();}
  else if(scan.find("Pix")!=string::npos and
          scan.find("Tot")!=string::npos){handler=new PixelTotTune();}
  else if(scan.find("Tot")!=string::npos and
          scan.find("Scan")!=string::npos){handler=new TotScan();}
  else if(scan.find("Self")!=string::npos) {handler=new SelfTriggerScan();}
  else{
    cout << "Scan not recognized: " << scan << endl;
    return 0;
  }

  handler->SetVerbose(cVerbose);
  handler->SetContext(backend);
  handler->SetInterface(interface);
  handler->SetMapping(netfile);
  handler->SetRetune(cRetune);

  for(uint32_t i=0;i<cConfs.count();i++){
    handler->AddFE(string(cConfs[i]));
  }

  cout << "Configure the scan" << endl;
  if(cOutPath.flags()&CmdArg::GIVEN){
    handler->SetOutPath(string(cOutPath));
  }
  if(cCharge.flags()&CmdArg::GIVEN){
    handler->SetCharge(cCharge);
  }

  cout << "Connect" << endl;
  handler->Connect();

  if(cConfig){
    cout << "Config" << endl;
    handler->Config();
  }

  cout << "Run" << endl;
  handler->Run();

  cout << "Disconnect" << endl;
  handler->Disconnect();

  cout << "Save" << endl;
  handler->Save();

  cout << "Cleaning the house" << endl;
  delete handler;

  cout << "Have a nice day" << endl;
  return 0;
}
