#include "FEI4Emulator/Pulse.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

Pulse::Pulse(){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x09;
  m_field4=0x00;
  m_field5=0x00;
}

Pulse::Pulse(uint32_t chipid, uint32_t width){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x09;
  m_field4=chipid;
  m_field5=width;
}

Pulse::Pulse(Pulse *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
  m_field3=copy->m_field3;
  m_field4=copy->m_field4;
  m_field5=copy->m_field5;
}

Pulse::~Pulse(){}

Pulse * Pulse::Clone(){
  return new Pulse(this);
}

string Pulse::ToString(){
  ostringstream os;
  os << "Pulse "
      << "chipid: " << m_field4 << " "
      << "width: " << m_field5;
  return os.str();
}

Position Pulse::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "Pulse::Unpack" << endl;
  Position newpos=pos;
  newpos = FromByteArray(bytes,&m_field1, 5,newpos);
  newpos = FromByteArray(bytes,&m_field2, 4,newpos);
  newpos = FromByteArray(bytes,&m_field3, 4,newpos);
  if(m_field1!=0x16 or m_field2!=0x08 or m_field3!=0x09){
    //cout << "This is not a Pulse" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field4, 4,newpos);
  newpos = FromByteArray(bytes,&m_field5, 6,newpos);
  return newpos;  
}

Position Pulse::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1, 5,pos);
  pos = ToByteArray(bytes,m_field2, 4,pos);
  pos = ToByteArray(bytes,m_field3, 4,pos);
  pos = ToByteArray(bytes,m_field4, 4,pos);
  pos = ToByteArray(bytes,m_field5, 6,pos);
  return pos;
}

uint32_t Pulse::GetType(){
  return Command::PULSE;
}

void Pulse::SetChipID(uint32_t chipid){
  m_field4=chipid;
}

uint32_t Pulse::GetChipID(){
  return m_field4;
}

void Pulse::SetWidth(uint32_t width){
  m_field5=width;
}

uint32_t Pulse::GetWidth(){
  return m_field5;
}
