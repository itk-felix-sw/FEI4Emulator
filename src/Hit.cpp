#include "FEI4Emulator/Hit.h"

#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;
using namespace fei4b;

Hit::Hit(){
  m_col=0;
  m_row=0;
  m_tot=0;
  m_bcid=0;
  m_l1id=0;
  m_evnum=0;
}

Hit::Hit(uint32_t l1id, uint32_t bcid){
  m_col=0;
  m_row=0;
  m_tot=0;
  m_bcid=bcid;
  m_l1id=l1id;
  m_evnum=0;
}

Hit::~Hit(){}

Hit * Hit::Clone(){
  Hit * hit = new Hit();
  hit->m_col=m_col;
  hit->m_row=m_row;
  hit->m_tot=m_tot;
  hit->m_bcid=m_bcid;
  hit->m_l1id=m_l1id;
  hit->m_evnum=m_evnum;
  return hit;
}

void Hit::Update(uint32_t l1id, uint32_t bcid, bool top){
  if(top){
    m_l1id|=l1id;
    m_bcid|=bcid;
  }else{
    m_l1id=l1id;
    m_bcid=bcid;
  }
}

void Hit::Set(uint32_t col, uint32_t row, uint32_t tot){
  m_col=col;
  m_row=row;
  m_tot=tot;
}

uint32_t Hit::GetCol(){
  return m_col;
}

uint32_t Hit::GetRow(){
  return m_row;
}

uint32_t Hit::GetTOT(){
  return m_tot;
}

uint32_t Hit::GetL1ID(){
  return m_l1id;
}

uint32_t Hit::GetEvNum(){
  return m_evnum;
}

uint32_t Hit::GetBCID(){
  return m_bcid;
}

void Hit::SetCol(uint32_t col){
  m_col=col;
}

void Hit::SetRow(uint32_t row){
  m_row=row;
}

void Hit::SetTOT(uint32_t tot){
  m_tot=tot;
}

void Hit::SetL1ID(uint32_t l1id){
  m_l1id=l1id;
}

void Hit::SetEvNum(uint32_t evnum){
  m_evnum=evnum;
}

void Hit::SetBCID(uint32_t bcid){
  m_bcid=bcid;
}

std::string Hit::ToString(){
  ostringstream os;
  os << "Hit "
     << "evnum: " << m_evnum << " "
     << "col: " << m_col << " "
     << "row: " << m_row << " "
     << "tot: " << m_tot << " "
     << "l1id: " << m_l1id << " "
     << "bcid: " << m_bcid << " ";
  return os.str();
}

