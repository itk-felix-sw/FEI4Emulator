#include "FEI4Emulator/RdReg.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

RdReg::RdReg(){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x01;
  m_field4=0x00;
  m_field5=0x00;
}

RdReg::RdReg(uint32_t chipid, uint32_t address){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x01;
  m_field4=chipid;
  m_field5=address;
}

RdReg::RdReg(RdReg *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
  m_field3=copy->m_field3;
  m_field4=copy->m_field4;
  m_field5=copy->m_field5;
}

RdReg::~RdReg(){}

RdReg * RdReg::Clone(){
  return new RdReg(this);
}

string RdReg::ToString(){
  ostringstream os;
  os << "RdReg";
  return os.str();
}

Position RdReg::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "RdReg::Unpack" << endl;
  Position newpos=pos;
  newpos = FromByteArray(bytes,&m_field1,5,newpos);
  newpos = FromByteArray(bytes,&m_field2,4,newpos);
  newpos = FromByteArray(bytes,&m_field3,4,newpos);
  if(m_field1!=0x16 or m_field2!=0x08 or m_field3!=0x01){
    //cout << "This is not a RdReg" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field4,4,newpos);
  newpos = FromByteArray(bytes,&m_field5,6,newpos);
  return newpos;  
}

Position RdReg::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1,5,pos);
  pos = ToByteArray(bytes,m_field2,4,pos);
  pos = ToByteArray(bytes,m_field3,4,pos);
  pos = ToByteArray(bytes,m_field4,4,pos);
  pos = ToByteArray(bytes,m_field5,6,pos);
  return pos;
}

uint32_t RdReg::GetType(){
  return Command::RDREG;
}

void RdReg::SetChipID(uint32_t chipid){
  m_field4=chipid;
}

uint32_t RdReg::GetChipID(){
  return m_field4;
}

void RdReg::SetAddress(uint32_t address){
  m_field5=address;
}

uint32_t RdReg::GetAddress(){
  return m_field5;
}


