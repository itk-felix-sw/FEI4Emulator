#include "FEI4Emulator/AddressRecord.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

AddressRecord::AddressRecord(){
  m_id=0xEA;
  m_type=0;
  m_addr=0;
}

AddressRecord::AddressRecord(AddressRecord *copy){
  m_id=copy->m_id;
  m_type=copy->m_type;
  m_addr=copy->m_addr;
}

AddressRecord::AddressRecord(uint32_t addr, uint32_t type){
  m_id=0xEA;
  m_type=type;
  m_addr=addr;
}

AddressRecord::~AddressRecord(){}

string AddressRecord::ToString(){
  ostringstream os;
  os << "AddressRecord"
     << " Type: " << m_type
     << " Addr: " << m_addr;
  return os.str();
}
  
uint32_t AddressRecord::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<3) return 0;
  if(bytes[0]!=0xEA and
     bytes[0]!=0xE2 and
     bytes[0]!=0xFA and
     bytes[0]!=0xCA and
     bytes[0]!=0xAA /*and
     bytes[0]!=0x6A*/){
    //cout << "Address record not recognized: 0x" << hex << (uint32_t)bytes[0] << dec << endl;
    return 0;
  }
  m_id   = bytes[0];
  m_type =(bytes[1]&0x80)>>7;
  m_addr =(bytes[1]&0x7F)<<8;
  m_addr|=(bytes[2]&0xFF)>>0;
  return 3;
}

uint32_t AddressRecord::Pack(uint8_t * bytes){
  bytes[0] =m_id;
  bytes[1] =(m_type&0x01)<<7;
  bytes[1]|=(m_addr&0x7F)>>8;
  bytes[2] =(m_addr&0xFF)<<0;
  return 3;
}

uint32_t AddressRecord::GetType(){
  return Record::ADDRESS_RECORD;
}

void AddressRecord::SetAddressType(uint32_t type){
  m_type=type;
}

uint32_t AddressRecord::GetAddressType(){
  return m_type;
}

void AddressRecord::SetAddress(uint32_t addr){
  m_addr=addr;
}

uint32_t AddressRecord::GetAddress(){
  return m_addr;
}
 
