#include "FEI4Emulator/RunMode.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

RunMode::RunMode(){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x0A;
  m_field4=0x00;
  m_field5=RUNMODE;
}

RunMode::RunMode(uint32_t chipid, uint32_t mode){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x0A;
  m_field4=chipid;
  m_field5=mode;
}

RunMode::RunMode(RunMode *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
  m_field3=copy->m_field3;
  m_field4=copy->m_field4;
  m_field5=copy->m_field5;
}

RunMode::~RunMode(){}

RunMode * RunMode::Clone(){
  return new RunMode(this);
}

string RunMode::ToString(){
  ostringstream os;
  os << "RunMode "
     << "chipid: " << m_field4 << " "
     << "mode: " << (m_field5==RUNMODE?"RUN":"CONFIG");
  return os.str();
}

Position RunMode::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "RunMode::Unpack" << endl;
  Position newpos=pos;
  newpos = FromByteArray(bytes,&m_field1, 5,newpos);
  newpos = FromByteArray(bytes,&m_field2, 4,newpos);
  newpos = FromByteArray(bytes,&m_field3, 4,newpos);
  if(m_field1!=0x16 or m_field2!=0x08 or m_field3!=0x0A){
    //cout << "This is not a RunMode" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field4, 4,newpos);
  newpos = FromByteArray(bytes,&m_field5, 6,newpos);
  return newpos;  
}

Position RunMode::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1, 5,pos);
  pos = ToByteArray(bytes,m_field2, 4,pos);
  pos = ToByteArray(bytes,m_field3, 4,pos);
  pos = ToByteArray(bytes,m_field4, 4,pos);
  pos = ToByteArray(bytes,m_field5, 6,pos);
  return pos;
}

uint32_t RunMode::GetType(){
  return Command::RUNMODE;
}

void RunMode::SetChipID(uint32_t chipid){
  m_field4=chipid;
}

uint32_t RunMode::GetChipID(){
  return m_field4;
}

void RunMode::SetMode(uint32_t mode){
  m_field5=mode;
}

uint32_t RunMode::GetMode(){
  return m_field5;
}

