#include "FEI4Emulator/WrReg.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

WrReg::WrReg(){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x02;
  m_field4=0x00;
  m_field5=0x00;
  m_field6=0x00;
}

WrReg::WrReg(uint32_t chipid, uint32_t address,uint32_t value){
  m_field1=0x16;
  m_field2=0x08;
  m_field3=0x02;
  m_field4=chipid;
  m_field5=address;
  m_field6=value;
}

WrReg::WrReg(WrReg *copy){
  m_field1=copy->m_field1;
  m_field2=copy->m_field2;
  m_field3=copy->m_field3;
  m_field4=copy->m_field4;
  m_field5=copy->m_field5;
  m_field6=copy->m_field6;
}

WrReg::~WrReg(){}

WrReg * WrReg::Clone(){
  return new WrReg(this);
}

string WrReg::ToString(){
  ostringstream os;
  os << "WrReg "
      << "chipid: " << m_field4 << " "
      << "address: " << m_field5 << " "
      << "value: 0x" << hex << setw(4) << setfill('0') << m_field6;
  return os.str();
}

Position WrReg::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  //cout << "WrReg::Unpack" << endl;
  Position newpos=pos;
  newpos = FromByteArray(bytes,&m_field1, 5,newpos);
  newpos = FromByteArray(bytes,&m_field2, 4,newpos);
  newpos = FromByteArray(bytes,&m_field3, 4,newpos);
  if(m_field1!=0x16 or m_field2!=0x08 or m_field3!=0x02){
    //cout << "This is not a WrReg" << endl;
    return pos;
  }
  newpos = FromByteArray(bytes,&m_field4, 4,newpos);
  newpos = FromByteArray(bytes,&m_field5, 6,newpos);
  uint32_t word1,word2=0;
  newpos = FromByteArray(bytes,&word1,8,newpos); 
  newpos = FromByteArray(bytes,&word2,8,newpos); 
  m_field6=(word1<<8)|word2;
  return newpos;  
}

Position WrReg::Pack(uint8_t * bytes, Position pos){
  pos = ToByteArray(bytes,m_field1, 5,pos);
  pos = ToByteArray(bytes,m_field2, 4,pos);
  pos = ToByteArray(bytes,m_field3, 4,pos);
  pos = ToByteArray(bytes,m_field4, 4,pos);
  pos = ToByteArray(bytes,m_field5, 6,pos);
  pos = ToByteArray(bytes,m_field6>>8,8,pos);
  pos = ToByteArray(bytes,m_field6&0xFF,8,pos);
  return pos;
}

uint32_t WrReg::GetType(){
  return Command::WRREG;
}

void WrReg::SetChipID(uint32_t chipid){
  m_field4=chipid;
}

uint32_t WrReg::GetChipID(){
  return m_field4;
}

void WrReg::SetAddress(uint32_t address){
  m_field5=address;
}

uint32_t WrReg::GetAddress(){
  return m_field5;
}

void WrReg::SetValue(uint32_t value){
  m_field6=value;
}

uint32_t WrReg::GetValue(){
  return m_field6;
}
