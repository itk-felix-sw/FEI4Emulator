#include "FEI4Emulator/SelfTriggerScan.h"
#include "TH2I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

SelfTriggerScan::SelfTriggerScan(){}

SelfTriggerScan::~SelfTriggerScan(){}

void SelfTriggerScan::Run(){

  //create histograms
  map<string,TH2I*> occ;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
  }

  uint32_t run_time = 300;

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    cout << "Read the global registers" << endl;
    fe->SetRunMode(false);
    fe->ReadGlobal();
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    cout << "Enable the whole matrix" << endl;
    fe->GetConfig()->SetField(Configuration::Trig_Count,15);
    fe->SetMask(1,0); // was 3010
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelHitBus(col,row,true);
      }
    }
    fe->ConfigPixelHitBus();
    Send(fe);
    cout << "Enable auto trigger" << endl;
    fe->GetConfig()->SetField(Configuration::HitOr,1);
    fe->ConfigGlobal();
    fe->SetRunMode(true);
    Send(fe);
    
  }
  PrepareTrigger(0);
  //read-out
  uint32_t nhits=0;
  auto start = chrono::steady_clock::now();
  auto now = start;
  auto last = start;
  while(true){

    //trigger after 1 ms
    now=chrono::steady_clock::now();
    if(chrono::duration_cast<chrono::milliseconds>(now - last).count()>1){
      //cout << "Rate: " << (float)(nhits/0.100) << " Hz" << endl;
      cout << "Hits since last iteration: " << nhits << endl;
      nhits=0;
      Trigger();
      last = now;
    }

    //histogram
    //for(uint i=0;i<1E6;i++){
      for(auto fe : GetFEs()){
        if(!fe->HasHits()) continue;
        Hit* hit = fe->GetHit();
        if(hit and hit!=0){
          nhits++;
          occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
        }
        fe->NextHit();
      }
    //}

    if(chrono::duration_cast<chrono::seconds>(now - start).count()>run_time){
      break;
    }

  }

  //Results
  cout << "Post-Scan" << endl;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]->Write();
  }


}
