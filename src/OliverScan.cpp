#include "FEI4Emulator/OliverScan.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"
#include "TH1I.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

OliverScan::OliverScan(){}

OliverScan::~OliverScan(){}

void OliverScan::Run(){

  uint32_t trig_delay = 50;
  //uint32_t mask_step = 1; //0,1,2,4,8,16
  //uint32_t dc_mode = 3; //0=every 1, 1=every 4, 2=every 8, 3=all
  //uint32_t dc_max = 1; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 50;


  float charge = 10000;
  bool use_scap = true;
  bool use_lcap = true;
  uint32_t vcal = Tools::toVcal(charge, use_scap, use_lcap);

  //create histograms
  map<string,TH2I*> occ;
  map<string,TH2I*> mask;
  map<string,TH2I*> tdac;
  map<string,TH1I*> l1id;
  map<string,TH1I*> bcid;
  for(auto fe : GetFEs()){
    //Note we use col-1 and row-1
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,-0.5,79.5,336,-0.5,335.5);
    mask[fe->GetName()]=new TH2I(("mask_"+fe->GetName()).c_str(),";Column;Row",80,-0.5,79.5,336,-0.5,335.5);
    tdac[fe->GetName()]=new TH2I(("tdac_"+fe->GetName()).c_str(),";Column;Row",80,-0.5,79.5,336,-0.5,335.5);
    l1id[fe->GetName()]=new TH1I(("l1id_"+fe->GetName()).c_str(),";L1ID",10001,-0.5,10000+0.5);
    bcid[fe->GetName()]=new TH1I(("bcid_"+fe->GetName()).c_str(),";BCID",0xDEC,-0.5,0xDEC+0.5);
  }

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->SetRunMode(false);
    fe->GetConfig()->SetField(Configuration::Trig_Count,15);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-trig_delay-5);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
    fe->GetConfig()->SetField(Configuration::CalPulseWidth,20);
    fe->SetGlobalThreshold(80);
    fe->SetMask(3,0);
    if (use_lcap) fe->EnableLCap();
    if (use_scap) fe->EnableSCap();
    Send(fe);

    /*for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelEnable(col,row,0);
        fe->SetPixelThreshold(col,row,16);
      }
    }

    for(uint32_t col=0;col<5;col++){
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelEnable(col,row,1);
        fe->SetPixelThreshold(col,row,24);
      }
    }

    uint32_t tt=14;
    for(uint32_t col=5;col<30;col++){
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelEnable(col,row,1);
        fe->SetPixelThreshold(col,row,tt);
      }
      if(col%2==0)tt++;
    }

    for(uint32_t col=40;col<60;col++){
      tt=0;
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelEnable(col,row,1);
        fe->SetPixelThreshold(col,row,tt);
        if(row%8==0)tt++;
        tt&=0x1F;
      }
    }

    for(uint32_t col=78;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelEnable(col,row,1);
        fe->SetPixelThreshold(col,row,24);
      }
    }*/
    
    for(uint32_t col=0;col<80;col++){
      unsigned int tt = 0;
      for(uint32_t row=0;row<336;row++){
        fe->SetPixelEnable(col,row,1);
        fe->SetPixelThreshold(col,row,20);
        if (row%5==0)tt++;
        tt&=0x1F;
      }
    }

    fe->ConfigGlobal();
    Send(fe);

    //fe->ConfigPixels();
    for(uint32_t dc=0;dc<40;dc++){
      fe->ConfigDoubleColumn(0,dc);
      Send(fe);
    }
    fe->GetConfig()->SetField(Configuration::Colpr_Mode,3);
    fe->GetConfig()->SetField(Configuration::Colpr_Addr,0);
    fe->SetRunMode(true);
    Send(fe);
  }
  
  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger(trig_delay);

  //Trigger and read-out
  for(uint32_t trig=0;trig<n_trigs; trig++){

    Trigger();

    auto start = chrono::steady_clock::now();

    //histogram it
    uint32_t nhits=0;
    while(true){

      for(auto fe : GetFEs()){
        if(!fe->HasHits()) continue;
        Hit* hit = fe->GetHit();
        //cout << hit->ToString() << endl;
        if(hit!=0){
          occ[fe->GetName()]->Fill(hit->GetCol()-1,hit->GetRow()-1);
          l1id[fe->GetName()]->Fill(hit->GetL1ID());
          bcid[fe->GetName()]->Fill(hit->GetBCID());
          nhits++;
          
        }
        fe->NextHit();
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(ms>50){break;}
    }
    cout << "Nhits: " << nhits << endl;
  }

  cout << "Results" << endl;
  for(auto fe : GetFEs()){
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        mask[fe->GetName()]->Fill(col,row,fe->GetPixelEnable(col,row));
        tdac[fe->GetName()]->Fill(col,row,fe->GetPixelThreshold(col,row));
      }
    }
    occ[fe->GetName()]->Write();
    mask[fe->GetName()]->Write();
    tdac[fe->GetName()]->Write();
    l1id[fe->GetName()]->Write();
    bcid[fe->GetName()]->Write();
  }


}
