#include "FEI4Emulator/Unknown.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

Unknown::Unknown(){
  m_field1=0x0;
}

Unknown::Unknown(Unknown *copy){
  m_field1=copy->m_field1;
}

Unknown::~Unknown(){}

Unknown * Unknown::Clone(){
  return new Unknown(this);
}

string Unknown::ToString(){
  ostringstream os;
  os << "Unknown";
  return os.str();
}

Position Unknown::UnPack(uint8_t * bytes, Position pos, uint32_t maxlen){
  return FromByteArray(bytes,&m_field1,1,pos);  
}

Position Unknown::Pack(uint8_t * bytes, Position pos){
  return ToByteArray(bytes,m_field1,1,pos);
}

uint32_t Unknown::GetType(){
  return Command::UNKNOWN;
}

