#include "FEI4Emulator/ThresholdScan.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TH1I.h"
#include "TF1.h"
#include <math.h>

#include <iostream>
#include <iomanip>
#include <chrono>

using namespace std;
using namespace fei4b;

ThresholdScan::ThresholdScan(){}

ThresholdScan::~ThresholdScan(){}

void ThresholdScan::Run(){

  uint32_t mask_step = 8; //0,1,2,4,8,16
  uint32_t dc_mode = 3; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 1; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 100;
  uint32_t trig_delay = 50;
  bool use_scap = true;
  bool use_lcap = true;
  //uint32_t vcal_nom = Tools::toVcal(GetCharge(), use_scap, use_lcap); // 3000 = 56
  uint32_t vcal_min = Tools::toVcal(6000);//vcal_nom*0.3;
  uint32_t vcal_max = Tools::toVcal(21000);//vcal_nom*1.7;
  uint32_t vcal_stp = 10;//5;
  uint32_t vcal_stps= (vcal_max-vcal_min)/vcal_stp + 1;
  

  //create histograms
  map<string,TH2I*> occ;
  map<string,TH2F*> thres;
  map<string,TH2F*> noise;
  map<string,TH1F*> thres_1d;
  map<string,TH1F*> noise_1d;
  map<string,TH2I*> occ_vs_vcal;
  map<string,map<uint32_t,TH2I*> > allOccs;
  map<string,map<uint32_t,TH1I*> > scur;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    thres[fe->GetName()]=new TH2F(("thres_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    noise[fe->GetName()]=new TH2F(("noise_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    thres_1d[fe->GetName()]=new TH1F(("thres_1d_"+fe->GetName()).c_str(),";Threshold (electrons)",(Tools::toCharge(vcal_max) - Tools::toCharge(vcal_min))/20,Tools::toCharge(vcal_min),Tools::toCharge(vcal_max));
    noise_1d[fe->GetName()]=new TH1F(("noise_1d_"+fe->GetName()).c_str(),";Noise (electrons)",200,200,400);
    occ_vs_vcal[fe->GetName()]=new TH2I(("occ_vs_vcal_"+fe->GetName()).c_str(),";Charge (vcal);Occupancy",vcal_stps,vcal_min,vcal_max+vcal_stp,10,-0.5,100.5);
  }

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,15);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,255-trig_delay-5);
    fe->GetConfig()->SetField(Configuration::CalPulseWidth,20);
    fe->GetConfig()->SetField(Configuration::DigHitIn_Sel,0);
    fe->ConfigGlobal();
    Send(fe);

    if (use_lcap) fe->EnableLCap();
    if (use_scap) fe->EnableSCap();
    fe->SetMask(0,0);
    Send(fe);
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger(trig_delay);

  //keep going until we converge or we give up
  for(uint32_t vcal=vcal_min; vcal<=vcal_max; vcal+=vcal_stp){

    for(auto fe : GetFEs()){
      fe->SetRunMode(false);
      fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
      fe->ConfigGlobal();
      Send(fe);
      cout << "Vcal: " << vcal << endl;
    }

    //change the mask
    for(uint32_t mask=0; mask<mask_step; mask++){
      cout << "Mask: " << (mask+1) << "/" << mask_step << endl;

      for(auto fe : GetFEs()){
        fe->SetMask(mask_step,mask);
      }
      
      //select the double column
      for(uint32_t dc=0; dc<dc_max; dc++){
        cout << "DC: " << (dc+1) << "/" << dc_max << endl;
        for(auto fe : GetFEs()){
          fe->SelectDoubleColumn(dc_mode,dc);
          fe->ConfigGlobal();
          fe->SetRunMode(true);
          Send(fe);
        }
        uint32_t nhits2=0;

        //Trigger and read-out
        for(uint32_t trig=0;trig<n_trigs; trig++){

          Trigger();

          auto start = chrono::steady_clock::now();

          //histogram it
          uint32_t nhits=0;
          while(true){
            for(auto fe : GetFEs()){
              if(!fe->HasHits()) continue;
              Hit* hit = fe->GetHit();
              //cout << hit->ToString() << endl;
              if(hit and hit!=0){
                //if (fe->GetPixelMask(hit->GetCol()-1,hit->GetRow()-1)){continue;}
                occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
                nhits++;
              }
              fe->NextHit();
            }
            auto end = chrono::steady_clock::now();
            uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
            if(ms>30){break;}
          }
          nhits2+=nhits;
          //cout << "NHits per trigger: " << nhits << endl;
        }
        cout << "NHits in step: " << nhits2 << endl;
        for(auto fe : GetFEs()){
          fe->SetRunMode(false);
          Send(fe);
        }
      }
    }

    for(auto fe : GetFEs()){
      for(uint32_t col=0;col<80;col++){
        for(uint32_t row=0;row<336;row++){
          if (fe->GetPixelMask(col,row)){continue;}
          uint32_t bin=occ[fe->GetName()]->GetBin(col+1,row+1);
          uint32_t cnt=occ[fe->GetName()]->GetBinContent(bin);
          TH1I * hh=scur[fe->GetName()][bin];
          if(hh==NULL){
            ostringstream sname;
            sname << "scur_" << fe->GetName() << "_" << bin;
            hh=new TH1I(sname.str().c_str(),";Charge (Vcal); Num hits",ceil(float(vcal_max-vcal_min)/(float)vcal_stp +1),vcal_min-(float)vcal_stp/2,vcal_max+(float)vcal_stp/2);
            scur[fe->GetName()][bin]=hh;
          }
          for (uint32_t i=0;i<cnt;i++){ hh->Fill(vcal); }
          float mean = cnt/(float)n_trigs;
          occ_vs_vcal[fe->GetName()]->Fill(vcal,mean*100);
        }
      }
      ostringstream oname;
      oname << "occ_vcal_" << setfill('0') << setw(3) << vcal << "_" << fe->GetName();
      allOccs[fe->GetName()][vcal] = (TH2I*) occ[fe->GetName()]->Clone();
      allOccs[fe->GetName()][vcal]->SetName(oname.str().c_str());
      allOccs[fe->GetName()][vcal]->GetZaxis()->SetRangeUser(0,1.5 * n_trigs);
      occ[fe->GetName()]->Reset();
      occ[fe->GetName()]->ResetStats();
    }
  }

  //Results
  cout << "Analysis" << endl;

  TF1 * f1=new TF1("scurve","[0]*0.5*(1+erf((x-[1])/[2]))");

  for(auto fe : GetFEs()){
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        if (fe->GetPixelMask(col,row)){continue;}
        uint32_t bin=occ[fe->GetName()]->GetBin(col+1,row+1);
        f1->SetParameter(0,n_trigs);
        f1->SetParameter(1,(scur[fe->GetName()][bin]->GetXaxis()->GetXmax()+scur[fe->GetName()][bin]->GetXaxis()->GetXmin())/2.);
        f1->SetParameter(2,5);
        // check if there is data in, otherwise skip. 
        //cout << "Entries = " << scur[fe->GetName()][bin]->GetEntries() << endl;
        if (scur[fe->GetName()][bin]->GetEntries()<1){continue;}
        scur[fe->GetName()][bin]->Fit("scurve","Q"); // Q = no output-spam of the fitting. 
        float mean=f1->GetParameter(1);
        float sigma=f1->GetParameter(2);
        thres[fe->GetName()]->Fill(col,row,Tools::toCharge(mean));
        noise[fe->GetName()]->Fill(col,row,Tools::toCharge(sigma));
        thres_1d[fe->GetName()]->Fill(Tools::toCharge(mean));
        noise_1d[fe->GetName()]->Fill(Tools::toCharge(sigma));
        if ((rand()%400)==0){
          scur[fe->GetName()][bin]->Write();
        }else{
          delete scur[fe->GetName()][bin];
        }
      }
    }
    for (unsigned int i=vcal_min;i<=vcal_max;i+=vcal_stp){
      allOccs[fe->GetName()][i]->Write();
    }
    thres[fe->GetName()]->Write();
    noise[fe->GetName()]->Write();
    thres_1d[fe->GetName()]->Write();
    noise_1d[fe->GetName()]->Write();
    occ_vs_vcal[fe->GetName()]->Write();
    delete occ[fe->GetName()];
  }
  delete f1;

}
