#include "FEI4Emulator/EmptyRecord.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

EmptyRecord::EmptyRecord(){
  m_pattern=0;
}

EmptyRecord::EmptyRecord(EmptyRecord *copy){
  m_pattern=copy->m_pattern;
}

EmptyRecord::~EmptyRecord(){}

string EmptyRecord::ToString(){
  ostringstream os;
  os << "EmptyRecord"
     << " Pattern: " << m_pattern;
  return os.str();
}
  
uint32_t EmptyRecord::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<3) return 0;
  if(bytes[0]!=m_pattern || bytes[1]!=m_pattern || bytes[1]!=m_pattern){
    //cout << "Empty record not recognized: 0x" << hex << (uint32_t)bytes[0] << dec << endl;
    return 0;
  }
  return 3;
}

uint32_t EmptyRecord::Pack(uint8_t * bytes){
  bytes[0] = m_pattern;
  bytes[1] = m_pattern;
  bytes[2] = m_pattern;
  return 3;
}

uint32_t EmptyRecord::GetType(){
  return Record::EMPTY_RECORD;
}

void EmptyRecord::SetPattern(uint32_t pattern){
  m_pattern=pattern;
}

uint32_t EmptyRecord::GetPattern(){
  return m_pattern;
}

