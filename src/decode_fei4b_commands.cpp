#include "FEI4Emulator/Encoder.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <fstream>
#include <bitset>
#include <map>
#include <cmdl/cmdargs.h>

using namespace std;
using namespace fei4b;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# decode_fei4b_commands         #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose(   'v',"verbose","turn on verbose mode");
  CmdArgStr  cFilePath(  'f',"file","path","file to parse", CmdArg::isREQ);
  CmdArgStr  cFileFormat('F',"format","format","text,binary. Default text");

  CmdLine cmdl(*argv,&cVerbose,&cFilePath,&cFileFormat,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string format=(cFileFormat.flags()&CmdArg::GIVEN?cFileFormat:"text");

  cout << "Create new encoder object" << endl;
  Encoder *encoder=new Encoder();

  cout << "Create a byte stream" << endl;
  vector<uint8_t> bytes;

  cout << "Loop over file contents" << endl;

  if(format=="text"){
    ifstream fr(cFilePath);
    fr.seekg(0, std::ios::end);
    cout << "File size (bytes): " << fr.tellg() << endl;
    fr.seekg(0, std::ios::beg);
    char c1;
    char c2;
    map<char,uint32_t> c2n={
        {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4},
        {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9},
        {'A',10}, {'B',11}, {'C',12}, {'D',13}, {'E',14}, {'F',15},
        {'a',10}, {'b',11}, {'c',12}, {'d',13}, {'e',14}, {'f',15}
    };
    while(fr){
      fr >> c1 >> c2;
      if(fr.eof())break;
      uint8_t byte = c2n[c1]*16+c2n[c2];
      if(cVerbose) cout << "byte: 0x" << hex << (uint32_t) byte << dec << endl;
      bytes.push_back(byte);
    }
    fr.close();
  }else if(format=="binary"){
    ifstream fr(cFilePath, ios::binary);
    fr.seekg(0, std::ios::end);
    cout << "File size (bytes): " << fr.tellg() << endl;
    fr.seekg(0, std::ios::beg);

    char * buff = new char[8];
    uint8_t byte;

    while(fr){
      fr.read(buff,8);
      bitset<8> bstr(buff);
      byte=bstr.to_ulong()&0xFF;
      bytes.push_back(byte);
      if(cVerbose) cout << "byte: 0x" << hex << (uint32_t) byte << dec << endl;
    }

    delete[] buff;
    fr.close();
  }

  cout << "Set bytes: size: " << bytes.size() << endl;
  encoder->SetBytes(&bytes[0],bytes.size());

  if(cVerbose) cout << "Byte stream: 0x" << encoder->GetByteString() << endl;

  cout << "Decode" << endl;
  encoder->Decode();

  cout << "Contents" << endl;
  for(uint32_t i=0;i<encoder->GetCommands().size();i++){
    cout << encoder->GetCommands().at(i)->ToString() << endl;
  }

  delete encoder;

  return 0;
}
