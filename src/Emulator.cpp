#include "FEI4Emulator/Emulator.h"

#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;
using namespace fei4b;

Emulator::Emulator(uint32_t chipid){
  m_decoder = new Decoder();
  m_encoder = new Encoder();
  m_config  = new Configuration();
  m_verbose = 1;
  m_mode = RunMode::RUNMODE;
  m_l1id = 0;
  m_bcid = 0;
  m_nhits = 10;
  m_chipid=chipid;
  for(uint32_t i=0;i<40;i++){m_dcs.push_back(new DoubleColumn());}
  m_smallCap = 1.9;
  m_largeCap = 3.8;
  m_vcalOffset = 0;
  m_vcalSlope = 1.5;
  m_debug = false;
}

Emulator::~Emulator(){
  delete m_decoder;
  delete m_encoder;
  delete m_config;
  for(auto p: m_dcs){delete p;}
}

void Emulator::SetVerbose(bool enable){
   m_verbose = enable;
}

void Emulator::SetChipID(uint32_t chipid){
   m_chipid=chipid;
}

uint32_t Emulator::GetChipID(){
  return m_chipid;
}

void Emulator::SetNHits(uint32_t nhits){
  m_nhits=nhits;
  m_config->GetField(Configuration::HitOr)->SetValue((m_nhits>0?true:false));
}

uint32_t Emulator::GetNHits(){
  return m_nhits;
}

bool Emulator::IsMsgForMe(uint32_t chipid){
  if(chipid==m_chipid or (chipid&0x8)==0x8) return true;
  return false;
}

uint8_t * Emulator::GetBytes(){
  return m_decoder->GetBytes();
}

uint32_t Emulator::GetLength(){
  return m_decoder->GetLength();
}

void Emulator::HandleCommand(uint8_t *recv_data, uint32_t recv_size){
  if(recv_size==0){return;}
  cout << "Emulator received commands size : " << recv_size << endl;
  m_encoder->Clear();
  m_encoder->SetBytes(recv_data, recv_size);
  cout << "Byte stream: " << m_encoder->GetByteString() << endl;
  m_encoder->Decode();
  
  for(uint32_t i=0;i<m_encoder->GetCommands().size();i++){
    Command * cmd=m_encoder->GetCommands().at(i);
    if(m_verbose) cout << setw(2) << i << " " << cmd->ToString() << endl;
    if(cmd->GetType()==Command::LVL1 or
        cmd->GetType()==Command::CAL or
        cmd->GetType()==Command::ECR or
        cmd->GetType()==Command::BCR or
        cmd->GetType()==Command::UNKNOWN){
      m_cmds.push_back(cmd->Clone());
    }else if(cmd->GetType()==Command::RDREG){
      if(m_mode!=RunMode::CONFMODE){continue;}
      RdReg * rdreg = static_cast<RdReg*>(cmd);
      if(!IsMsgForMe(rdreg->GetChipID())){continue;}
      m_mutex.lock();
      m_addrs.push_back(rdreg->GetAddress());
      m_mutex.unlock();
    }else if(cmd->GetType()==Command::WRREG){
      if(m_mode!=RunMode::CONFMODE){continue;}
      WrReg * wrreg = static_cast<WrReg*>(cmd);
      if(!IsMsgForMe(wrreg->GetChipID())){continue;}
      m_mutex.lock();
      m_config->SetRegister(wrreg->GetAddress(),wrreg->GetValue());
      m_mutex.unlock();
    }else if(cmd->GetType()==Command::WRFE){
      if(m_mode!=RunMode::CONFMODE){continue;}
      WrFE * wrfe = static_cast<WrFE*>(cmd);
      if(!IsMsgForMe(wrfe->GetChipID())){continue;}
      for(auto dc: GetActiveDoubleColumns()){
        if(m_verbose) cout << "Write shift register to DC: " << dc << endl;
        m_dcs[dc]->SetRegister(wrfe->GetValues());
      }
    }else if(cmd->GetType()==Command::PULSE){
      uint32_t S0=m_config->GetField(Configuration::S0)->GetValue();
      uint32_t S1=m_config->GetField(Configuration::S1)->GetValue();
      uint32_t SC=m_config->GetField(Configuration::SR_Clock)->GetValue();
      uint32_t LE=m_config->GetField(Configuration::LatchEn)->GetValue();
      if(S0==0 and S1==0 and LE==1){
        //load the shift registers
        for(uint32_t i=0;i<13;i++){
          if(((m_config->GetField(Configuration::PixelLatch)->GetValue()>>i)&0x1)==0){continue;}
          for(auto dc: GetActiveDoubleColumns()){
            if(m_verbose) cout << "Load the shift register to DC: " << dc << " bit: " << i << endl;
            m_dcs[dc]->SetLatches(i);
          }
        }
      }else if(S0==0 and S1==0 and SC==1){
        //shift the shift registers
        for(uint32_t i=0;i<13;i++){
          if(((m_config->GetField(Configuration::PixelLatch)->GetValue()>>i)&0x1)==0){continue;}
          for(auto dc: GetActiveDoubleColumns()){
            if(m_verbose) cout << "Shift the shift register in DC: " << dc << " bit: " << i << endl;
            m_dcs[dc]->Shift(i);
          }
        }
      }
    }else if(cmd->GetType()==Command::RUNMODE){
      RunMode * mode = static_cast<RunMode*>(cmd);
      if(!IsMsgForMe(mode->GetChipID())){continue;}
      if(m_mode!=RunMode::RUNMODE && m_mode!=RunMode::CONFMODE){continue;}
      m_mode=mode->GetMode();
    }
  }
}

void Emulator::ProcessTrigger(){
  m_decoder->Clear();
  if(m_mode==RunMode::RUNMODE){

    //Auto-trigger mode
    if(m_config->GetField(Configuration::HitOr)->GetValue()==1){
      cout << "Generate a random hit"<<endl;
      //Generate event at random time
      m_bcid += (rand() % 0xDEB);
      m_bcid = (m_bcid % 0xDEC);
      m_bcid = 0;
      m_decoder->AddRecord(new DataHeader(m_l1id,m_bcid));
      m_decoder->AddRecord(ServiceRecord::DataCounter(m_l1id,m_bcid,true));
      uint32_t m_hdc=m_config->GetField(Configuration::HitDiscCfg)->GetValue();
      for(uint32_t i=0;i<m_nhits;i++){
        uint32_t col = (rand() % 80)+1;
        uint32_t row = (rand() % 336)+1;
        uint32_t tot1 = (rand() % 14)+1;
        uint32_t tot2 = (rand() % 15);
        m_decoder->AddRecord(new DataRecord(col,row,tot1,tot2,m_hdc));
      }
      m_l1id++;
    }else{
      //Process command queue
      while(!m_cmds.empty()){
        Command * cmd = m_cmds.front();
        cout << "Command: " << cmd->ToString() << endl;
        if(cmd->GetType()==Command::CAL){
          //Configure the CAL pulse
          m_bcid += 9;
          m_bcid = (m_bcid % 0xDEC);
          m_cal_hi = m_bcid+m_config->GetField(Configuration::CalPulseDelay)->GetValue();
          m_cal_lo = m_cal_hi+m_config->GetField(Configuration::CalPulseWidth)->GetValue();
          m_cal_hi = m_cal_hi % 0xDEC;
          m_cal_lo = m_cal_lo % 0xDEC;
        }else if(cmd->GetType()==Command::ECR){
          m_bcid += 9;
          m_bcid = (m_bcid % 0xDEC);
          m_l1id = 0;
        }else if(cmd->GetType()==Command::BCR){
          m_bcid = 0;
        }else if(cmd->GetType()==Command::LVL1){
          m_bcid += 5;
          m_bcid = (m_bcid % 0xDEC);
          m_decoder->AddRecord(new DataHeader(m_l1id,m_bcid));
          m_decoder->AddRecord(ServiceRecord::DataCounter(m_l1id,m_bcid,true));
          uint32_t m_hdc=m_config->GetField(Configuration::HitDiscCfg)->GetValue();
          bool dig=m_config->GetField(Configuration::DigHitIn_Sel)->GetValue();
          cout << "Build event" << endl;
          uint32_t nhits=0;
          for(uint32_t dc=0;dc<40;dc++){
            //Skip if the column is disabled
            if(dc<16){if((m_config->GetField(Configuration::DisableColCfg_0)->GetValue()>>dc)&0x1){continue;}}
            else if(dc<32){if((m_config->GetField(Configuration::DisableColCfg_1)->GetValue()>>(dc-16))&0x1){continue;}}
            else{if((m_config->GetField(Configuration::DisableColCfg_2)->GetValue()>>(dc-32))&0x1){continue;}}
            //Process each pixel
            if(m_verbose or m_debug) cout << "Process dc: " << dc << endl;
            for(uint32_t col=0;col<2; col++){
              for(uint32_t row=0;row<336;row++){
                if(m_dcs[dc]->GetPixel(col,row)->GetOutput()==0){continue;}
                //Add noise
                uint32_t qe = (rand() % 100 + 200);
                //Add calibration signal
                if(m_debug) { cout << endl
                                   << "injection:"
                                   << " bcid:" << m_bcid
                                   << " cal_hi:" << m_cal_hi
                                   << " cal_lo:" << m_cal_lo;
                }
                if(m_bcid>m_cal_hi and m_bcid<m_cal_lo){
                  qe += toCharge(m_config->GetField(Configuration::PlsrDac)->GetValue());
                }
                if(m_debug) cout << " qe:" << qe;
                //is it above threshold
                uint32_t thr_hi = m_config->GetField(Configuration::Vthin_Coarse)->GetValue();
                uint32_t thr_lo = m_config->GetField(Configuration::Vthin_Fine)->GetValue();
                uint32_t thr = thr_hi << 8 | thr_lo;
                thr += (rand() % 10 - 5);
                float qthr = toCharge(thr);
                if(m_debug) cout << " qthr:" << qthr << " thr:" << thr;
                //thr += m_dcs[dc]->GetPixel(col,row)->GetTDAC();
                if(m_verbose or m_debug){
                  cout << "[" << (dc*2+col+1) << "," << (row+1) << "]";
                  if(qe>qthr) { cout << "(analog)";  }
                  else if(dig){ cout << "(digital)"; }
                  else        { cout << "(ignored)"; }
                }
                if(qe<qthr && !dig){continue;}
                nhits++;
                uint32_t tot=5;
                m_decoder->AddRecord(new DataRecord(dc*2+col+1,row+1,tot,0,m_hdc));
              }
            }
            if(m_debug) cout << endl;
          }
          cout << "Pixels fired: " << nhits << endl;
          m_l1id++;
        }
        m_cmds.pop_front();
        delete cmd;
      }
    }
  }else{
    //config mode
    m_mutex.lock();
    while(!m_addrs.empty()){
      uint32_t addr=m_addrs.front();
      m_addrs.pop_front();
      if(m_config->GetField(Configuration::Conf_AddrEnable)->GetValue()==1){
        //cout << "Return AddressRecord: " << addr << endl;
        m_decoder->AddRecord(new AddressRecord(addr));
      }
      //cout << "Return ValueRecord: " << m_config->GetRegister(addr) << endl;
      m_decoder->AddRecord(new ValueRecord(m_config->GetRegister(addr)));
    }
    m_mutex.unlock();
  }
  
  m_decoder->Encode(); 

}

double Emulator::toCharge(double vcal, bool smallcap, bool largecap){
  double C = (smallcap?m_smallCap:0)+(largecap?m_largeCap:0);
  double V = (m_vcalSlope*vcal)/1.6;
  return 10*C*V;
}

uint32_t Emulator::toVcal(double charge, bool smallcap, bool largecap){
  double C = (smallcap?m_smallCap:0)+(largecap?m_largeCap:0);
  double Q = charge*1.6/m_vcalSlope;
  return Q/(10*C);
}

std::vector<uint32_t> Emulator::GetActiveDoubleColumns(uint32_t mode, uint32_t addr){
  vector<uint32_t> ret;
  if(mode==0xFF){mode=m_config->GetField(Configuration::Colpr_Mode)->GetValue();}
  if(addr==0xFF){addr=m_config->GetField(Configuration::Colpr_Addr)->GetValue();}
  if(mode==0){
    ret.push_back(addr);
  }else if(mode==1){
    for(uint32_t i=0;i<10;i++){
      ret.push_back(addr+4*i);
    }
  }else if(mode==2){
    for(uint32_t i=0;i<5;i++){
      ret.push_back(addr+8*i);
    }
  }else if(mode==3){
    for(uint32_t i=0;i<40;i++){
      ret.push_back(i);
    }
  }
  return ret;
}



