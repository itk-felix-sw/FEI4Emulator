#include "FEI4Emulator/Position.h"

using namespace std;
using namespace fei4b;

Position::Position(){
  m_byte=0;
  m_bit=0;
}

Position::Position(uint32_t byte, uint32_t bit){
  m_byte=byte;
  m_bit=bit;
}

Position::~Position(){}

uint32_t Position::GetByte(){
  return m_byte;
}

void Position::SetByte(uint32_t byte){
  m_byte=byte;
}

uint32_t Position::GetTotalBit(){
  return (m_byte*8+m_bit);
}

uint32_t Position::GetBit(){
  return m_bit;
}

void Position::SetBit(uint32_t bit){
  m_bit=bit;
}

void Position::Pack(){
  m_byte += m_bit>>3;
  m_bit  -= m_bit&0xFFFFFFF8;
}

Position Position::operator+(const Position &pos){
  Position ret;
  ret.m_byte = m_byte + pos.m_byte;
  ret.m_bit  = m_bit  + pos.m_bit;
  ret.Pack();
  return ret;
}

Position Position::operator+(uint32_t bits){
  Position ret(m_byte,m_bit+bits);
  ret.Pack();
  return ret;
}

Position& Position::operator+=(const Position &pos){
  m_byte += pos.m_byte;
  m_bit += pos.m_bit;
  Pack();
  return *this;
}

Position& Position::operator+=(uint32_t bits){
  m_bit += bits;
  Pack();
  return *this;
}

bool Position::operator==(Position& pos){
  return (m_byte==pos.m_byte && m_bit==pos.m_bit);
}

bool Position::operator!=(Position& pos){
  return !(m_byte==pos.m_byte && m_bit==pos.m_bit);
}

bool Position::operator<(Position& right){
  return (GetTotalBit()<right.GetTotalBit());
}

bool Position::operator>(Position& right){
  return (GetTotalBit()>right.GetTotalBit());
}

std::ostream& fei4b::operator<<(std::ostream &os, fei4b::Position &pos){
  os << "(" << pos.GetByte() << "," << pos.GetBit() << ")";
  return os;
}
