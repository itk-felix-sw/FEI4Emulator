#include "FEI4Emulator/Emulator.h"
#include <cmdl/cmdargs.h>
#include <netio/netio.hpp>
#include <map>
#include <chrono>
#include <signal.h>

using namespace std;

namespace felix::base{ 
  struct FromFELIXHeader { uint16_t length; uint16_t status; uint32_t elinkid:6; uint32_t gbtid:26;};
  struct ToFELIXHeader {uint32_t length; uint32_t reserved; uint64_t elinkid; };
}

bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){
  
  cout << "#####################################" << endl
       << "# Welcome to felix_fei4b_emulator   #" << endl
       << "#####################################" << endl;
  
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgInt  cCmdPort(  'c',"cmdport","number","Command port number. Default 12350");
  CmdArgInt  cDataPort( 'd',"dataport","number","Data port number. Default 12360");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgInt  cMaxRate(  'm',"max-rate","rate","max rate in Hz. Default 5 Hz");
  
  CmdLine cmdl(*argv,&cVerbose,&cCmdPort,&cDataPort,&cBackend,&cMaxRate,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
 
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  uint32_t port_cmd=(cCmdPort.flags()&CmdArg::GIVEN?cCmdPort:12350);
  uint32_t port_data=(cDataPort.flags()&CmdArg::GIVEN?cDataPort:12360);
  uint32_t max_rate=(cMaxRate.flags()&CmdArg::GIVEN?cMaxRate:5);
  uint32_t milis=1000./(float)max_rate;
    
  netio::context ctx(backend.c_str());
  netio::sockcfg cfg = netio::sockcfg::cfg();
  cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, 16);
  cfg(netio::sockcfg::PAGESIZE, 2000);
 
  map<netio::tag,netio::endpoint> elinks;
  map<netio::tag,fei4b::Emulator*> frontends;
  
  cout << "Create a publish socket" << endl;
  netio::publish_socket * data_socket=new netio::publish_socket(&ctx, port_data, cfg);
  
  data_socket->register_subscribe_callback([&](netio::tag t, netio::endpoint ep){
      cout << "Subscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
      elinks[t]=ep;
      frontends[t]=new fei4b::Emulator(0);
      frontends[t]->SetVerbose(cVerbose);  
    });

  data_socket->register_unsubscribe_callback([&](netio::tag t, netio::endpoint ep){
      cout << "Unsubscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
      elinks.erase(t);
      delete frontends[t];
      frontends.erase(t);
    });
  
  cout << "Create a receiver socket" << endl;
  netio::low_latency_recv_socket cmd_socket(&ctx, port_cmd, [&](netio::endpoint &ep, netio::message &msg){
      if(cVerbose) cout << "New message received" << endl;
      vector<uint8_t> cmd = msg.data_copy();
      felix::base::ToFELIXHeader hdr; 
      memcpy(&hdr,(const void*)&cmd[0], sizeof(hdr));
      uint32_t pos=sizeof(hdr);
      if(frontends.count(hdr.elinkid)==0){return;}
      if(cVerbose) cout << "Handle command" << endl;
      frontends[hdr.elinkid]->HandleCommand(&cmd[pos],cmd.size()-pos);
    });
  
  g_cont=true;
  signal(SIGINT,handler);
  signal(SIGTERM,handler);

  //Context for netio
  cout << "Create the context for netio" << endl;
  std::thread t_context([&ctx](){ctx.event_loop()->run_forever();});
  
  //Data sender
  cout << "Loop" << endl;
  while(g_cont){
    map<netio::tag,fei4b::Emulator*>::iterator it;
    for(it=frontends.begin();it!=frontends.end();it++){
      fei4b::Emulator * emu=it->second;
      emu->ProcessTrigger();
      felix::base::FromFELIXHeader hdr;
      hdr.length=emu->GetLength()+8;
      hdr.elinkid=(it->first)&0x3F;
      hdr.gbtid=(it->first)>>6;
      netio::message msg;      
      msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
      msg.add_fragment(emu->GetBytes(), emu->GetLength());      
      data_socket->publish(it->first,msg); 
    }
    if(cVerbose) cout << "Sleep for: " << milis << " ms" << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(milis));
  }
  
  cout << "Clean the house" << endl;
  //Cannot delete the data socket until netio is not fixed
  //cout << "delete data socket" << endl;
  //delete data_socket;
  cout << "Stop event loop" << endl;
  ctx.event_loop()->stop();
  cout << "Join context" << endl;
  t_context.join();  
  
  cout << "Delete front-ends" << endl;
  map<netio::tag,fei4b::Emulator*>::iterator it;
  for(it=frontends.begin();it!=frontends.end();it++){  
    delete it->second;
    frontends.erase(it);
  }
  
  cout << "Have a nice day" << endl;
  return 0;
}
