#include "FEI4Emulator/Decoder.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace fei4b;

Decoder::Decoder(){
  m_bytes.reserve(100000);
  m_bytes.resize(100000,0);
  m_length = 0;
  m_DH=new DataHeader();
  m_DR=new DataRecord();
  m_AR=new AddressRecord();
  m_VR=new ValueRecord();
  m_SR=new ServiceRecord();
  m_ER=new EmptyRecord();
}

Decoder::~Decoder(){
  Clear();
  delete m_DH;
  delete m_DR;
  delete m_AR;
  delete m_VR;
  delete m_SR;
  delete m_ER;
}

void Decoder::SetBytes(uint8_t *bytes, uint32_t len){
  m_length=0;
  for(uint32_t i=0;i<len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void Decoder::AddRecord(Record *record){
  m_records.push_back(record);
}
  
void Decoder::ClearBytes(){
  m_length=0;
}
  
void Decoder::ClearRecords(){
  while(!m_records.empty()){
    Record* record = m_records.back();
    delete record;
    m_records.pop_back();
  }
}

void Decoder::Clear(){
  ClearBytes();
  ClearRecords();
}
  
string Decoder::GetByteString(){
  ostringstream os;
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << " ";
  }
  return os.str();
}

uint8_t * Decoder::GetBytes(){
  return m_bytes.data();
}
  
uint32_t Decoder::GetLength(){
  return m_length;
}
  
vector<Record*> & Decoder::GetRecords(){
  return m_records;
}

void Decoder::Encode(){
  uint32_t pos=0;
  for(uint32_t i=0;i<m_records.size();i++){
    pos+=m_records[i]->Pack(&m_bytes[pos]);
  }
  m_length=pos;
}

void Decoder::Decode(){
  ClearRecords();
  uint32_t pos=0;
  uint32_t nb=0;
  uint32_t tnb=m_length;
  while(pos!=tnb){
    Record * record=0;
    if     ((nb=m_DH->UnPack(&m_bytes[pos],tnb-pos))>0){record=m_DH; m_DH=new DataHeader();}
    else if((nb=m_AR->UnPack(&m_bytes[pos],tnb-pos))>0){record=m_AR; m_AR=new AddressRecord();}
    else if((nb=m_VR->UnPack(&m_bytes[pos],tnb-pos))>0){record=m_VR; m_VR=new ValueRecord();}
    else if((nb=m_SR->UnPack(&m_bytes[pos],tnb-pos))>0){record=m_SR; m_SR=new ServiceRecord();}
    else if((nb=m_ER->UnPack(&m_bytes[pos],tnb-pos))>0){record=m_ER; m_ER=new EmptyRecord();}
    else if((nb=m_DR->UnPack(&m_bytes[pos],tnb-pos))>0){record=m_DR; m_DR=new DataRecord();}
    else{
      cout << "Cannot decode byte sequence: "
           << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec 
           << " at index: " << pos 
           << " ...skipping" << endl;
      pos++;
      continue;
    }
    if(!record){pos++; continue;}
    m_records.push_back(record);
    pos+=nb;
  }
  //Check if there is bytes left
}
