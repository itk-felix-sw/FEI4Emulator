#include "FEI4Emulator/Configuration.h"
#include <iostream>

using namespace std;
using namespace fei4b;

std::map<std::string, uint32_t> Configuration::m_name2index={
    {"Reg1Spare",Reg1Spare},
    {"SmallHitErase",SmallHitErase}, {"SME",SmallHitErase},
    {"EventLimit",EventLimit},{"Eventlimit",EventLimit},
    {"Trig_Count",Trig_Count}, {"TrgCnt",Trig_Count}, {"TriggerCount",Trig_Count},
    {"Conf_AddrEnable",Conf_AddrEnable}, {"ConfigAddressEnable",Conf_AddrEnable},
    {"Reg2Spare",Reg2Spare},
    {"ErrorMask_0",ErrorMask_0}, {"ErrMask_0",ErrorMask_0},
    {"ErrorMask_1",ErrorMask_1}, {"ErrMask_1",ErrorMask_1},
    {"PrmpVbp_R",PrmpVbp_R}, {"PrmpVbpRight",PrmpVbp_R},
    {"BufVgOpAmp",BufVgOpAmp}, {"GADCVref",BufVgOpAmp},
    {"Reg6Spare",Reg6Spare},
    {"PrmpVbp",PrmpVbp},
    {"TDACVbp",TdacVbn}, {"TdacVbp",TdacVbn}, {"TdacVbn",TdacVbn},
    {"DisVbn",DisVbn},
    {"Amp2Vbn",Amp2Vbn},
    {"Amp2VbpFol",Amp2VbpFol},
    {"Reg9Spare",Reg9Spare},
    {"Amp2Vbp",Amp2Vbp},
    {"FdacVbn",FdacVbn}, {"FDACVbn",FdacVbn},
    {"Amp2Vbpff",Amp2Vbpff}, {"Amp2Vbpf",Amp2Vbpff},
    {"PrmpVbnFol",PrmpVbnFol},
    {"PrmpVbp_L",PrmpVbp_L},{"PrmpVbpLeft",PrmpVbp_L},
    {"PrmpVbpf",PrmpVbpf},
    {"PrmpVbnLcc",PrmpVbnLcc},{"PrmpVbnLCC",PrmpVbnLcc},
    {"Reg13Spare",Reg13Spare},
    {"PixelLatch",PixelLatch},{"Pixel_latch_strobe",PixelLatch},{"PxStrobes",PixelLatch},
    {"S0",S0},
    {"S1",S1},
    {"LVDSDrvIref",LVDSDrvIref},
    {"GADCCompBias",GADCCompBias},{"GADCOpAmp",GADCCompBias},
    {"PllIbias",PllIbias},
    {"LVDSDrvVos",LVDSDrvVos},
    {"TempSensIbias",TempSensIbias},{"TempSensBias",TempSensIbias},
    {"PllIcp",PllIcp},
    {"Reg17Spare",Reg17Spare},
    {"PlsrIDacRamp",PlsrIDacRamp},{"PlsrIDACRamp",PlsrIDacRamp},{"PlsrIdacRamp",PlsrIDacRamp},
    {"VrefDigTune",VrefDigTune},
    {"PlsrVgOPamp",PlsrVgOPamp},{"PlsrVgOpAmp",PlsrVgOPamp},
    {"PlsrDacBias",PlsrDacBias},{"PlsrDACbias",PlsrDacBias},
    {"VrefAnTune",VrefAnTune},
    {"Vthin_Coarse",Vthin_Coarse},{"Vthin_AltCoarse",Vthin_Coarse},
    {"Vthin_Fine",Vthin_Fine},{"Vthin_AltFine",Vthin_Fine},
    {"PlsrDac",PlsrDac},{"PlsrDAC",PlsrDac},
    {"DigHitIn_Sel",DigHitIn_Sel},{"DIGHITIN_Sel",DigHitIn_Sel},
    {"DINJ_Override",DINJ_Override},{"DJO",DINJ_Override},
    {"HITLD_IN",HITLD_IN},{"HitLD",HITLD_IN},{"HITLD_In",HITLD_IN},
    {"Reg21Spare",Reg21Spare},
    {"Reg22Spare2",Reg22Spare2},
    {"Colpr_Mode",Colpr_Mode},
    {"Colpr_Addr",Colpr_Addr},
    {"Reg22Spare1",Reg22Spare1},
    {"DisableColCfg_0",DisableColCfg_0},{"DisableColCnfg0",DisableColCfg_0},{"DisableColumnCnfg0",DisableColCfg_0},
    {"DisableColCfg_1",DisableColCfg_1},{"DisableColCnfg1",DisableColCfg_1},{"DisableColumnCnfg1",DisableColCfg_1},
    {"DisableColCfg_2",DisableColCfg_2},{"DisableColCnfg2",DisableColCfg_2},{"DisableColumnCnfg2",DisableColCfg_2},
    {"Trig_Latency",Trig_Latency},{"Trig_Lat",Trig_Latency},{"TrigLat",Trig_Latency},
    {"CalPulseWidth",CalPulseWidth},
    {"CalPulseDelay",CalPulseDelay},
    {"CMDcnt",CMDcnt},
    {"StopModeCnfg",StopModeCnfg},{"StopModeConfig",StopModeCnfg},
    {"HitDiscCfg",HitDiscCfg}, {"HitDiscCnfg",HitDiscCfg},
    {"PLLEn",PLLEn},{"PLL_Enable",PLLEn},{"EN_PLL",PLLEn},
    {"EFUSE_Sense",EFUSE_Sense},{"EFS",EFUSE_Sense},{"Efuse_sense",EFUSE_Sense},
    {"StopClkPulse",StopClkPulse},{"Stop_Clk",StopClkPulse},
    {"ReadErrorReq",ReadErrorReq},
    {"Reg27Spare1",Reg27Spare1},
    {"GADC_En",GADCStart}, {"GADCStart",GADCStart}, {"GADC_Enable",GADCStart},
    {"SRRead",SRRead},{"ShiftReadBack",SRRead},{"SR_Read",SRRead},
    {"Reg27Spare2",Reg27Spare2},
    {"HitOr",HitOr},{"GateHitOr",HitOr},
    {"CalEn",CalEn},
    {"SRClr",SRClr},{"SR_clr",SRClr},
    {"LatchEn",LatchEn},{"Latch_Enable",LatchEn},{"Latch_en",LatchEn},
    {"SR_Clock",SR_Clock},
    {"LVDSDrvSet06",LVDSDrvSet06},
    {"Reg28Spare",Reg28Spare},
    {"EN_40M",EN_40M},{"EN40M",EN_40M},
    {"EN_80M",EN_80M},{"EN80M",EN_80M},
    {"CLK0_S2",CLK0_S2},
    {"CLK0_S1",CLK0_S1},
    {"CLK0_S0",CLK0_S0},
    {"CLK1_S2",CLK1_S2},
    {"CLK1_S1",CLK1_S1},
    {"CLK1_S0",CLK1_S0},
    {"EN_160M",EN_160M},{"EN160M",EN_160M},{"EN_160",EN_160M},
    {"EN_320M",EN_320M},{"EN320M",EN_320M},{"EN_320",EN_320M},
    {"Reg29Spare1",Reg29Spare1},
    {"No8b10b",No8b10b},{"no8b10b",No8b10b},
    {"Clk2Out",Clk2Out},{"Clk2OutCnfg",Clk2Out},
    {"EmptyRecord",EmptyRecord},{"EmptyRecordCnfg",EmptyRecord},
    {"Reg29Spare2",Reg29Spare2},
    {"LVDSDrvEn",LVDSDrvEn},
    {"LVDSDrvSet30",LVDSDrvSet30},
    {"LVDSDrvSet12",LVDSDrvSet12},
    {"TempSensDiodeSel",TempSensDiodeSel},{"TmpSensDiodeSel",TempSensDiodeSel},
    {"TempSensDisable",TempSensDisable},{"TmpSensDisable",TempSensDisable},
    {"MonleakRange",MonleakRange},{"IleakRange",MonleakRange},
    {"Reg30Spare",Reg30Spare},
    {"PlsrRiseUpTau",PlsrRiseUpTau},
    {"PlsrPwr",PlsrPwr},
    {"PlsrDelay",PlsrDelay},
    {"ExtDigCalSW",ExtDigCalSW},
    {"ExtAnaCalSW",ExtAnaCalSW},
    {"Reg31Spare",Reg31Spare},
    {"GADCSel",GADCSel},
    {"SELB_0",SELB_0},{"SELB0",SELB_0},
    {"SELB_1",SELB_1},{"SELB1",SELB_1},
    {"SELB_2",SELB_2},{"SELB2",SELB_2},
    {"M13",M13},
    {"Reg34Spare1",Reg34Spare1},
    {"PrmpVbpEn",PrmpVbpEn},{"PrmpVbpMsbEn",PrmpVbpEn},{"PrmpVbpMsnEn",PrmpVbpEn},
    {"Reg34Spare2",Reg34Spare2},
    {"Chip_SN",Chip_SN}
};

Configuration::Configuration(){

  m_registers.resize(36);

  m_fields[Reg1Spare]      = new Field(&m_registers[ 1],0x9, 7,0);
  m_fields[SmallHitErase]  = new Field(&m_registers[ 1],0x8, 1,0);
  m_fields[EventLimit]     = new Field(&m_registers[ 1],0x0, 8,0,true);
  m_fields[Trig_Count]     = new Field(&m_registers[ 2],0xC, 4,1);
  m_fields[Conf_AddrEnable]= new Field(&m_registers[ 2],0xB, 1,1);
  m_fields[Reg2Spare]      = new Field(&m_registers[ 2],0x0,11,1);
  m_fields[ErrorMask_0]    = new Field(&m_registers[ 3],0x0,16,0x4600);
  m_fields[ErrorMask_1]    = new Field(&m_registers[ 4],0x0,16,0x0040);
  m_fields[PrmpVbp_R]      = new Field(&m_registers[ 5],0x8, 8,43,true);
  m_fields[BufVgOpAmp]     = new Field(&m_registers[ 5],0x0, 8,160,true);
  m_fields[Reg6Spare]      = new Field(&m_registers[ 6],0x8, 8,0);
  m_fields[PrmpVbp]        = new Field(&m_registers[ 6],0x0, 8,43,true);
  m_fields[TdacVbn]        = new Field(&m_registers[ 7],0x8, 8,150,true);
  m_fields[DisVbn]         = new Field(&m_registers[ 7],0x0, 8,40,true);
  m_fields[Amp2Vbn]        = new Field(&m_registers[ 8],0x8, 8,79,true);
  m_fields[Amp2VbpFol]     = new Field(&m_registers[ 8],0x0, 8,26,true);
  m_fields[Reg9Spare]      = new Field(&m_registers[ 9],0x8, 8,0);
  m_fields[Amp2Vbp]        = new Field(&m_registers[ 9],0x0, 8,85,true);
  m_fields[FdacVbn]        = new Field(&m_registers[10],0x8, 8,30,true);
  m_fields[Amp2Vbpff]      = new Field(&m_registers[10],0x0, 8,50,true);
  m_fields[PrmpVbnFol]     = new Field(&m_registers[11],0x8, 8,106,true);
  m_fields[PrmpVbp_L]      = new Field(&m_registers[11],0x0, 8,43,true);
  m_fields[PrmpVbpf]       = new Field(&m_registers[12],0x8, 8,40,true);
  m_fields[PrmpVbnLcc]     = new Field(&m_registers[12],0x0, 8,0,true);
  m_fields[S0]             = new Field(&m_registers[13],0xF, 1,0);
  m_fields[S1]             = new Field(&m_registers[13],0xE, 1,0);
  m_fields[PixelLatch]     = new Field(&m_registers[13],0x1,13,0,true);
  m_fields[Reg13Spare]     = new Field(&m_registers[13],0x0, 1,0);
  m_fields[LVDSDrvIref]    = new Field(&m_registers[14],0x8, 8,171,true);
  m_fields[GADCCompBias]   = new Field(&m_registers[14],0x0, 8,100,true);
  m_fields[PllIbias]       = new Field(&m_registers[15],0x8, 8,88,true);
  m_fields[LVDSDrvVos]     = new Field(&m_registers[15],0x0, 8,105,true);
  m_fields[TempSensIbias]  = new Field(&m_registers[16],0x8, 8,0,true);
  m_fields[PllIcp]         = new Field(&m_registers[16],0x0, 8,28,true);
  m_fields[Reg17Spare]     = new Field(&m_registers[17],0x8, 8,0);
  m_fields[PlsrIDacRamp]   = new Field(&m_registers[17],0x0, 8,213,true);
  m_fields[VrefDigTune]    = new Field(&m_registers[18],0x8, 8,110);
  m_fields[PlsrVgOPamp]    = new Field(&m_registers[18],0x0, 8,255);
  m_fields[PlsrDacBias]    = new Field(&m_registers[19],0x8, 8,96);
  m_fields[VrefAnTune]     = new Field(&m_registers[19],0x0, 8,50);
  m_fields[Vthin_Coarse]   = new Field(&m_registers[20],0x8, 8,0,true);
  m_fields[Vthin_Fine]     = new Field(&m_registers[20],0x0, 8,150,true);
  m_fields[Reg21Spare]     = new Field(&m_registers[21],0xD, 3,0);
  m_fields[HITLD_IN]       = new Field(&m_registers[21],0xC, 1,0);
  m_fields[DINJ_Override]  = new Field(&m_registers[21],0xB, 1,0);
  m_fields[DigHitIn_Sel]   = new Field(&m_registers[21],0xA, 1,0);
  m_fields[PlsrDac]        = new Field(&m_registers[21],0x0,10,54,true);
  m_fields[Reg22Spare2]    = new Field(&m_registers[22],0xA, 6,0);
  m_fields[Colpr_Mode]     = new Field(&m_registers[22],0x8, 2,0,true);
  m_fields[Colpr_Addr]     = new Field(&m_registers[22],0x2, 6,0,true);
  m_fields[Reg22Spare1]    = new Field(&m_registers[22],0x0, 2,0);
  m_fields[DisableColCfg_0]= new Field(&m_registers[23],0x0,16,0);
  m_fields[DisableColCfg_1]= new Field(&m_registers[24],0x0,16,0);
  m_fields[Trig_Latency]   = new Field(&m_registers[25],0x8, 8,210);
  m_fields[DisableColCfg_2]= new Field(&m_registers[25],0x0, 8,0);
  m_fields[CalPulseDelay]  = new Field(&m_registers[26],0xB, 5,0);
  m_fields[CalPulseWidth]  = new Field(&m_registers[26],0x3, 8,10);
  m_fields[CMDcnt]         = new Field(&m_registers[26],0x3,13,10);
  m_fields[StopModeCnfg]   = new Field(&m_registers[26],0x2, 1,0);
  m_fields[HitDiscCfg]     = new Field(&m_registers[26],0x0, 2,0);
  m_fields[PLLEn]          = new Field(&m_registers[27],0xF, 1,1);
  m_fields[EFUSE_Sense]    = new Field(&m_registers[27],0xE, 1,0);
  m_fields[StopClkPulse]   = new Field(&m_registers[27],0xD, 1,0);
  m_fields[ReadErrorReq]   = new Field(&m_registers[27],0xC, 1,0);
  m_fields[Reg27Spare2]    = new Field(&m_registers[27],0xB, 1,0);
  m_fields[GADCStart]      = new Field(&m_registers[27],0xA, 1,0);
  m_fields[SRRead]         = new Field(&m_registers[27],0x9, 1,0);
  m_fields[Reg27Spare1]    = new Field(&m_registers[27],0x6, 3,0);
  m_fields[HitOr]          = new Field(&m_registers[27],0x5, 1,0);
  m_fields[CalEn]          = new Field(&m_registers[27],0x4, 1,0);
  m_fields[SRClr]          = new Field(&m_registers[27],0x3, 1,0);
  m_fields[LatchEn]        = new Field(&m_registers[27],0x2, 1,0);
  m_fields[SR_Clock]       = new Field(&m_registers[27],0x1, 1,0);
  m_fields[M13]            = new Field(&m_registers[27],0x0, 1,0);
  m_fields[LVDSDrvSet06]   = new Field(&m_registers[28],0xF, 1,1);
  m_fields[Reg28Spare]     = new Field(&m_registers[28],0xA, 5,0);
  m_fields[EN_40M]         = new Field(&m_registers[28],0x8, 1,1);
  m_fields[EN_80M]         = new Field(&m_registers[28],0x8, 1,0);
  m_fields[CLK1_S0]        = new Field(&m_registers[28],0x7, 1,0);
  m_fields[CLK1_S1]        = new Field(&m_registers[28],0x6, 1,0);
  m_fields[CLK1_S2]        = new Field(&m_registers[28],0x5, 1,0);
  m_fields[CLK0_S0]        = new Field(&m_registers[28],0x4, 1,0);
  m_fields[CLK0_S1]        = new Field(&m_registers[28],0x3, 1,0);
  m_fields[CLK0_S2]        = new Field(&m_registers[28],0x2, 1,0);
  m_fields[EN_160M]        = new Field(&m_registers[28],0x1, 1,1);
  m_fields[EN_320M]        = new Field(&m_registers[28],0x0, 1,0);
  m_fields[Reg29Spare2]    = new Field(&m_registers[29],0xE, 2,0);
  m_fields[No8b10b]        = new Field(&m_registers[29],0xD, 1,0);
  m_fields[Clk2Out]        = new Field(&m_registers[29],0xC, 1,0);
  m_fields[EmptyRecord]    = new Field(&m_registers[29],0x4, 8,0);
  m_fields[Reg29Spare1]    = new Field(&m_registers[29],0x3, 1,0);
  m_fields[LVDSDrvEn]      = new Field(&m_registers[29],0x2, 1,1);
  m_fields[LVDSDrvSet30]   = new Field(&m_registers[29],0x1, 1,1);
  m_fields[LVDSDrvSet12]   = new Field(&m_registers[29],0x0, 1,1);
  m_fields[TempSensDiodeSel]=new Field(&m_registers[30],0xE, 2,0,true);
  m_fields[TempSensDisable]= new Field(&m_registers[30],0xD, 1,0);
  m_fields[MonleakRange]   = new Field(&m_registers[30],0xC, 1,0);
  m_fields[Reg30Spare]     = new Field(&m_registers[30],0xC, 1,0);
  m_fields[PlsrRiseUpTau]  = new Field(&m_registers[31],0xD, 3,7);
  m_fields[PlsrPwr]        = new Field(&m_registers[31],0xC, 1,1);
  m_fields[PlsrDelay]      = new Field(&m_registers[31],0x6, 6,2,true);
  m_fields[ExtDigCalSW]    = new Field(&m_registers[31],0x5, 1,0);
  m_fields[ExtAnaCalSW]    = new Field(&m_registers[31],0x4, 1,0);
  m_fields[Reg30Spare]     = new Field(&m_registers[31],0x3, 1,0);
  m_fields[GADCSel]        = new Field(&m_registers[31],0x0, 3,0);
  m_fields[SELB_0]         = new Field(&m_registers[32],0x0,16,0);
  m_fields[SELB_1]         = new Field(&m_registers[33],0x0,16,0);
  m_fields[SELB_2]         = new Field(&m_registers[34],0x8, 8,0);
  m_fields[Reg34Spare1]    = new Field(&m_registers[34],0x5, 3,0);
  m_fields[PrmpVbpEn]      = new Field(&m_registers[34],0x4, 1,0);
  m_fields[Reg34Spare1]    = new Field(&m_registers[34],0x0, 4,0);
  m_fields[Chip_SN]        = new Field(&m_registers[35],0x0,16,0);
  

  m_names_yarr={
      "Trig_Count","Conf_AddrEnable","ErrorMask_0","ErrorMask_1","PrmpVbp_R","BufVgOpAmp","PrmpVbp","TDACVbp","DisVbn",
      "Amp2Vbn","Amp2VbpFol","Amp2Vbp","FDACVbn","Amp2Vbpff", "PrmpVbnFol", "PrmpVbp_L", "PrmpVbpf", "PrmpVbnLCC",
      "Pixel_latch_strobe","S0","S1","LVDSDrvIref","GADCCompBias","PllIbias","LVDSDrvVos","TempSensIbias","PllIcp",
      "PlsrIDACRamp","VrefDigTune","PlsrVgOpAmp","PlsrDACbias","VrefAnTune","Vthin_Coarse","Vthin_Fine","PlsrDAC",
      "DigHitIn_Sel","DJO","HitLD","Colpr_Addr","Colpr_Mode","DisableColCnfg0","DisableColCnfg1","DisableColCnfg2",
      "Trig_Lat","CalPulseDelay","CalPulseWidth","StopModeConfig","HitDiscCnfg","PLL_Enable","EFS","StopClkPulse",
      "ReadErrorReq","GADC_En","SRRead","HitOr","CalEn","SRClr","Latch_Enable","SR_Clock","LVDSDrvSet06","EN_40M",
      "EN_80M","CLK0_S2","CLK0_S1","CLK0_S0","CLK1_S2","CLK1_S1","CLK1_S0","EN_160","EN_320","No8b10b","Clk2Out",
      "EmptyRecordCnfg","LVDSDrvEn","LVDSDrvSet30","LVDSDrvSet12","TmpSensDiodeSel","TmpSensDisable","IleakRange",
      "PlsrRiseUpTau","PlsrPwr","PlsrDelay","ExtDigCalSW","ExtAnaCalSW","GADCSel","SELB0","SELB1","SELB2",
      "PrmpVbpMsbEn","SME","EventLimit"
  };

  m_names_rce={
      "TrigCnt","Conf_AddrEnable","Reg2Spare","ErrMask0","ErrMask1","PrmpVbpRight","BufVgOpAmp","Reg6Spare","PrmpVbp",
      "TdacVbp","DisVbn","Amp2Vbn","Amp2VbpFol","Reg9Spare","Amp2Vbp","FdacVbn","Amp2Vbpf","PrmpVbnFol","PrmpVbpLeft",
      "PrmpVbpf","PrmpVbnLcc","Reg13Spare","PxStrobes","S0","S1","LVDSDrvIref","GADCOpAmp","PllIbias","LVDSDrvVos",
      "TempSensBias","PllIcp","Reg17Spare","PlsrIdacRamp","VrefDigTune","PlsrVgOPamp","PlsrDacBias","VrefAnTune",
      "Vthin_AltCoarse","Vthin_AltFine","PlsrDAC","DIGHITIN_Sel","DINJ_Override","HITLD_In","Reg21Spare","Reg22Spare2",
      "Colpr_Addr","Colpr_Mode","Reg22Spare1","DisableColumnCnfg0","DisableColumnCnfg1","DisableColumnCnfg2","TrigLat",
      "CMDcnt","StopModeCnfg","HitDiscCnfg","EN_PLL","Efuse_sense","Stop_Clk","ReadErrorReq","Reg27Spare1","GADC_Enable",
      "ShiftReadBack","Reg27Spare2","GateHitOr","CalEn","SR_clr","Latch_en","SR_Clock","LVDSDrvSet06","Reg28Spare",
      "EN40M","EN80M","CLK0_S2","CLK0_S1","CLK0_S0","CLK1_S2","CLK1_S1","CLK1_S0","EN160M","EN320M","Reg29Spare1",
      "no8b10b","Clk2OutCnfg","EmptyRecord","Reg29Spare2","LVDSDrvEn","LVDSDrvSet30","LVDSDrvSet12","TempSensDiodeSel",
      "TempSensDisable","IleakRange","Reg30Spare","PlsrRiseUpTau","PlsrPwr","PlsrDelay","ExtDigCalSW","ExtAnaCalSW",
      "Reg31Spare","GADCSel","SELB0","SELB1","SELB2","Reg34Spare1","PrmpVbpMsnEn","Reg34Spare2","Chip_SN","Reg1Spare",
      "SmallHitErase","Eventlimit"
  };
}

Configuration::~Configuration(){
  for(auto it : m_fields){
    delete it.second;
  }
  m_fields.clear();
}

uint32_t Configuration::Size(){
  return m_registers.size();
}

uint16_t Configuration::GetRegister(uint32_t index){
  return m_registers[index].GetValue();
}
	
void Configuration::SetRegister(uint32_t index, uint16_t value){
  if(index>35){return;}
  m_registers[index].SetValue(value);
}

void Configuration::SetField(string name, uint16_t value){
  auto it=m_name2index.find(name);
  if(it==m_name2index.end()){return;}
  m_fields[it->second]->SetValue(value);
}

void Configuration::SetField(uint32_t index, uint16_t value){
  m_fields[index]->SetValue(value);
}

Field * Configuration::GetField(uint32_t index){
  return m_fields[index];
}

Field * Configuration::GetField(string name){
  auto it=m_name2index.find(name);
  if(it==m_name2index.end()){return 0;}
  return m_fields[it->second];
}

vector<uint32_t> Configuration::GetUpdatedRegisters(){
  vector<uint32_t> ret;
  for(uint32_t i=0;i<m_registers.size();i++){
    if(m_registers[i].IsUpdated()){ret.push_back(i);m_registers[i].Update(false);}
  }
  return ret;
}

map<string,uint32_t> Configuration::GetRegisters(){
  map<string,uint32_t> ret;
  for(auto name : m_names_yarr){
    ret[name]=m_fields[m_name2index[name]]->GetValue();
  }
  return ret;
}

