#include "FEI4Emulator/DataHeader.h"
#include <iostream>
#include <iomanip>
#include <bitset>

using namespace std;
using namespace fei4b;

DataHeader::DataHeader(){
  m_id=0xE9;
  m_flag=0;
  m_l1id=0;
  m_bcid=0;
}

DataHeader::DataHeader(DataHeader *copy){
  m_id=copy->m_id;
  m_flag=copy->m_flag;
  m_l1id=copy->m_l1id;
  m_bcid=copy->m_bcid;
}

DataHeader::DataHeader(uint32_t l1id, uint32_t bcid, bool error){
  m_id=0xE9;
  //cout << "super duper dataheader action: " 
  //     << " l1id = " << l1id 
  //     << " bcid = " << bcid << endl;
  m_flag=error;
  m_l1id=l1id;
  m_bcid=bcid;
}

DataHeader::~DataHeader(){}

string DataHeader::ToString(){
  ostringstream os;
  os << "DataHeader"
     << " L1ID: " << m_l1id
     << " BCID: " << m_bcid
     << " Error: " << m_flag;
  return os.str();
}
  
uint32_t DataHeader::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<3) return 0;
  if(bytes[0]!=0xE9 and bytes[0]!=0xE1 and bytes[0]!=0xF9 and 
     bytes[0]!=0xC9 and bytes[0]!=0xA9 /*and bytes[0]!=0x69*/){
    //cout << "Data header not recognized: 0x" << hex << (uint32_t)bytes[0] << dec << endl;
    return 0;
  }
  m_id   = bytes[0];
  m_flag =(bytes[1]>>7)&0x01;
  m_l1id =(bytes[1]>>2)&0x1F;
  //cout << "DataHeader::SETTINGS BYTE FUNC: l1id = " << m_l1id << endl;
  //cout << "DataHeader::SETTINGS BYTE FUNC: bytes[1] = " << bitset<8*sizeof(bytes[1])>(bytes[1]) << endl;
  m_bcid =(bytes[1]&0x03)<<8;
  m_bcid|=(bytes[2]&0xFF)<<0;
  return 3;
}

uint32_t DataHeader::Pack(uint8_t * bytes){
  bytes[0] =m_id;
  bytes[1] =(m_flag&0x01)<<7;
  bytes[1]|=(m_l1id&0x1F)<<2;
  //cout << "Dataheader::ADDING BYTE FUNC: l1id = " << m_l1id << endl;
  //cout << "Dataheader::ADDING BYTE FUNC: bytes[1] = " << bitset<8*sizeof(bytes[1])>(bytes[1]) << endl;
  bytes[1]|=(m_bcid>>8)&0x03;
  bytes[2] =(m_bcid>>0)&0xFF;
  return 3;
}

uint32_t DataHeader::GetType(){
  return Record::DATA_HEADER;
}

void DataHeader::SetErrorFlag(bool enable){
  m_flag=enable;
}

bool DataHeader::GetErrorFlag(){
  return m_flag;
}

void DataHeader::SetL1ID(uint32_t l1id){
  m_l1id=l1id;
}

uint32_t DataHeader::GetL1ID(){
  return m_l1id;
}
 
void DataHeader::SetBCID(uint32_t bcid){
  m_bcid=bcid;
}

uint32_t DataHeader::GetBCID(){
  return m_bcid;
}
  
