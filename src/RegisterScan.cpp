#include "FEI4Emulator/RegisterScan.h"
#include "TH2I.h"

#include <iostream>
#include <iomanip>
#include <chrono>

using namespace std;
using namespace fei4b;

RegisterScan::RegisterScan(){}

RegisterScan::~RegisterScan(){}

void RegisterScan::Run(){

  for(auto fe : GetFEs()){
    cout << "Registers for: " << fe->GetName() << endl;
    for(uint32_t i=2; i<32; i++){
      cout << setw(2) << setfill(' ') << i << ": 0x" << setw(4) << setfill('0') << hex << fe->GetConfig()->GetRegister(i) << dec << endl;
    }
  }

  cout << "Request the config" << endl;
  for(auto fe : GetFEs()){
    fe->ReadGlobal();
    fe->SetRunMode(false);
    Send(fe);
  }

  this_thread::sleep_for(chrono::seconds(2));
  for(auto fe : GetFEs()){
    cout << "Registers for: " << fe->GetName() << endl;
    for(uint32_t i=2; i<32; i++){
      cout << setw(2) << setfill(' ') << i << ": 0x" << setw(4) << setfill('0') << hex << fe->GetConfig()->GetRegister(i) << dec << endl;
    }
  }

}
