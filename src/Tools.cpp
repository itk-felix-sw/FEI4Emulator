#include "FEI4Emulator/Tools.h"

#include <iostream>

using namespace std;
using namespace fei4b;

Tools::Tools(){}

Tools::~Tools(){}

uint32_t Tools::toVcal(double charge,
    bool use_smallcap, bool use_largecap,
    float smallcap, float largecap, float vcalslope){
  double C = (use_smallcap?smallcap:0)+(use_largecap?largecap:0);
  double Q = charge*1.6/vcalslope;
  return Q/(10*C);
}



double Tools::toCharge(double vcal,
    bool use_smallcap, bool use_largecap,
    float smallcap, float largecap, float vcalslope){
  double C = (use_smallcap?smallcap:0)+(use_largecap?largecap:0);
  double V = (vcalslope*vcal)/1.6;
  return 10*C*V;
}
