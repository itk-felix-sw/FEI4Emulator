#include <FEI4Emulator/TriggerHandler.h>
#include <FEI4Emulator/TriggerThresholdScan.h>

#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>

using namespace std;
using namespace fei4b;

bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to trigger_manager_fei4b  #" << endl
       << "#####################################" << endl;

  CmdArgStr  cScan(     's',"scan","scan","scan name",CmdArg::isREQ);
  CmdArgStrList cConfs( 'c',"config","chips","chip names",CmdArg::isREQ);
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgStr  cInterface('i',"interface","network-card","Default enp24s0f0");
  CmdArgStr  cNetFile(  'n',"netfile","file","Network file. Default network.json");
  CmdArgIntList cThres( 't',"thresholds","values","scan parameters: threshold, tot_charge, tot_value");
  CmdArgInt  cElinkT(   'T',"elinkT","elink-number","Default 0x33B");

  CmdLine cmdl(*argv,&cScan,&cConfs,&cVerbose,&cBackend,&cInterface,&cNetFile,&cThres,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string scan(cScan);
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"enp24s0f0");
  string netfile=(cNetFile.flags()&CmdArg::GIVEN?cNetFile:"connectivity-testbed.json");

  int target_threshold = (cThres.count()>0?cThres[0]:3000);
  int target_charge = (cThres.count()>1?cThres[1]:16000);
  int target_tot = (cThres.count()>2?cThres[2]:10);
  
  uint32_t elinkT=(cElinkT.flags()&CmdArg::GIVEN?cElinkT:0x33B);
  
  TriggerHandler * handler = 0;

  if(scan.find("Scan")!=string::npos and
     scan.find("Thr")!=string::npos){handler=new TriggerThresholdScan();}
  else{
    cout << "No matching scan found. Aborting." << endl;
    return 1;
  }
  handler->SetVerbose(cVerbose);
  handler->SetContext(backend);
  handler->SetInterface(interface);
  handler->SetMapping(netfile);
  handler->SetTriggerElink(elinkT);

  for(uint32_t i=0;i<cConfs.count();i++){
    handler->AddFE(string(cConfs[i]));
  }
  
  cout << "Enabling hardcoded FE's now." << endl;
  // hardcoding AddFE, because i dont have a config atm. 
  for (int i=1;i<17;i++){
    string emuName = "emu";
    if (i < 10){
      emuName += "0";
    }
    emuName += to_string(i);
    cout << "Adding FE: " << emuName << endl;
    handler->AddFE(emuName);
  }

  cout << "Connect" << endl;
  handler->Connect();

  cout << "Config" << endl;
  handler->Config();

  cout << "Run" << endl;
  handler->Run();

  cout << "Disconnect" << endl;
  handler->Disconnect();

  cout << "Save" << endl;
  handler->Save();

  cout << "Cleaning the house" << endl;
  delete handler;

  cout << "Have a nice day" << endl;
  return 0;
}
