#include "FEI4Emulator/TotScan.h"
#include "FEI4Emulator/Tools.h"
#include "TH2I.h"
#include "TH1I.h"
#include "TF1.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace fei4b;

TotScan::TotScan(){}

TotScan::~TotScan(){}

void TotScan::Run(){

  uint32_t mask_step = 4; //0,1,2,4,8,16
  uint32_t dc_mode = 1; //0=every 1, 1=every 4, 2=every 8, 3=all
  uint32_t dc_max = 4; //0=40, 1=4, 2=8, 3=1
  uint32_t n_trigs = 30;
  uint32_t vcal_min = 10;
  uint32_t vcal_max = 80;
  uint32_t vcal_stp = 5;

  //create histograms
  map<string,TH2I*> occ;
  map<string,TH2I*> mask;
  map<string,TH1I*> thres;
  map<string,TH1I*> noise;
  map<string,map<uint32_t,TH1I*> > scur;
  for(auto fe : GetFEs()){
    occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    mask[fe->GetName()]=new TH2I(("mask_"+fe->GetName()).c_str(),";Column;Row",80,0.5,80.5,336,0.5,336.5);
    noise[fe->GetName()]=new TH1I(("noise_"+fe->GetName()).c_str(),";Noise (electrons)",200,200,400);
    thres[fe->GetName()]=new TH1I(("thres_"+fe->GetName()).c_str(),";Threshold (electrons)",2000,2000,20000);
  }

  //pre-scan
  cout << "Pre-Scan" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::Trig_Count,1);
    fe->GetConfig()->SetField(Configuration::Trig_Latency,10);
    fe->SetMask(3,0);
    Send(fe);
  }

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger();

  //keep going until we converge or we give up
  for(uint32_t vcal=vcal_min; vcal<vcal_max; vcal+=vcal_stp){

    for(auto fe : GetFEs()){
      fe->GetConfig()->SetField(Configuration::PlsrDac,vcal);
      Send(fe);
      cout << "Vcal: " << vcal << endl;
    }

    //change the mask
    for(uint32_t mask=0; mask<mask_step; mask++){
      cout << "Mask: " << (mask+1) << "/" << mask_step << endl;
      
      for(auto fe : GetFEs()){
        fe->SetMask(mask_step,mask);
      }
      
      //select the double column
      for(uint32_t dc=0; dc<dc_max; dc++){
        cout << "DC: " << (dc+1) << "/" << dc_max << endl;
        for(auto fe : GetFEs()){
          fe->SelectDoubleColumn(dc_mode,dc);
          fe->SetRunMode(true);
          Send(fe);
        }
        uint32_t nhits2=0;

        //Trigger and read-out
        for(uint32_t trig=0;trig<n_trigs; trig++){

          Trigger();

          auto start = chrono::steady_clock::now();

          //histogram it
          uint32_t nhits=0;
          while(true){
            for(auto fe : GetFEs()){
              if(!fe->HasHits()) continue;
              Hit* hit = fe->GetHit();
              //cout << hit->ToString() << endl;
              if(hit and hit!=0){
                occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
                nhits++;
              }
              fe->NextHit();
            }
            auto end = chrono::steady_clock::now();
            uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
            if(ms>50){break;}
          }
          nhits2+=nhits;
          //cout << "NHits per trigger: " << nhits << endl;
        }
        cout << "NHits in step: " << nhits2 << endl;
        for(auto fe : GetFEs()){
          fe->SetRunMode(false);
          Send(fe);
        }
      }
    }

    for(auto fe : GetFEs()){
      for(uint32_t col=0;col<80;col++){
        for(uint32_t row=0;row<336;row++){
          uint32_t bin=occ[fe->GetName()]->GetBin(col,row);
          uint32_t cnt=occ[fe->GetName()]->GetBinContent(bin);
          if(cnt>n_trigs){cnt=n_trigs;}
          TH1I * hh=scur[fe->GetName()][bin];
          if(hh==NULL){
            ostringstream sname;
            sname << "scur_" << fe->GetName() << "_" << bin;
            hh=new TH1I(sname.str().c_str(),";Charge (Vcal)",(vcal_max-vcal_min)/(float)vcal_stp,vcal_min-(float)vcal_stp/2,vcal_max+(float)vcal_stp/2);
            scur[fe->GetName()][bin]=hh;
          }
          hh->Fill(vcal,cnt);
        }
      }
      occ[fe->GetName()]->Reset();
      occ[fe->GetName()]->ResetStats();
    }
  }

  //Results
  TF1 * f1=new TF1("scurve","[0]*0.5*(1+erf((x-[1])/[2]))");

  for(auto fe : GetFEs()){
    for(uint32_t col=0;col<80;col++){
      for(uint32_t row=0;row<336;row++){
        mask[fe->GetName()]->Fill(col,row,fe->GetPixelConfig(col,row,"Enable"));
        uint32_t bin=occ[fe->GetName()]->GetBin(col,row);
        f1->SetParameter(0,n_trigs);
        f1->SetParameter(1,(scur[fe->GetName()][bin]->GetXaxis()->GetXmax()+scur[fe->GetName()][bin]->GetXaxis()->GetXmin())/2.);
        f1->SetParameter(2,5);
        scur[fe->GetName()][bin]->Fit("scurve");
        float mean=f1->GetParameter(1);
        float sigma=f1->GetParameter(2);
        thres[fe->GetName()]->Fill(Tools::toCharge(mean));
        noise[fe->GetName()]->Fill(Tools::toCharge(sigma));
        if ((rand()%400)==0){
          ostringstream os;
          os << "Scurve_" << fe->GetName() << "_" << bin << ".C";
          scur[fe->GetName()][bin]->SaveAs(os.str().c_str());
        }
        delete scur[fe->GetName()][bin];
      }
    }
    mask[fe->GetName()]->SaveAs(("Mask_"+fe->GetName()+".C").c_str());
    thres[fe->GetName()]->SaveAs(("Thres_"+fe->GetName()+".C").c_str());
    noise[fe->GetName()]->SaveAs(("Noise_"+fe->GetName()+".C").c_str());
    delete mask[fe->GetName()];
    delete thres[fe->GetName()];
    delete noise[fe->GetName()];
    delete occ[fe->GetName()];
   }
  delete f1;

}
