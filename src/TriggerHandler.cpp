#include "FEI4Emulator/TriggerHandler.h"
#include "FEI4Emulator/RunNumber.h"
#include <json.hpp>
#include <iostream>
#include <fstream>
#include <mutex>
#include <chrono>
#include <sstream>
#include <iomanip>
#include "TFile.h"

using namespace std;
using namespace fei4b;

struct FelixL1AMsg {
  FelixL1AMsg(uint32_t elink, uint32_t l1):length(sizeof(FelixL1AMsg)),elinkid(elink),bcid(0),l1id(l1){};
  uint16_t length; uint16_t status; uint32_t elinkid;
  uint16_t fmt:16; uint8_t len:4; uint16_t bcid:12; uint32_t l1id;
  uint32_t orbit; uint16_t ttype; uint16_t res:16; uint32_t l0id;
};

struct FelixDataHeader{
  uint16_t length;
  uint16_t status;
  uint32_t elink;
};

struct FelixCmdHeader{
  uint32_t length;
  uint32_t reserved;
  uint64_t elink;
};

TriggerHandler::TriggerHandler(){
  /*
  string itkpath = getenv("ITK_PATH");//"/home/itkpix/itk-felix-sw";
  m_config_path.push_back("./");
  m_config_path.push_back("./tuned/");
  m_config_path.push_back(itkpath+"/");
  m_config_path.push_back(itkpath+"/tuned/");
  //m_config_path.push_back(itkpath+"/config/");
  m_config_path.push_back(itkpath+"/installed/share/data/config/");
  */

  string itk_inst_path = getenv("ITK_INST_PATH");
  m_config_path.push_back(itk_inst_path+"/share/data/config/");
  
  m_verbose = false;
  m_backend = "posix";
  m_charge = 3000;
  m_retune = false;
  m_rootfile = 0;
  m_outpath = "/tmp"; //string(getenv("ITK_DATA_PATH"));//"/home/adecmos/data";
  m_rn = new RunNumber();
  m_trg_host = "localhost";
  m_trg_port = 12360;
  m_l1id=0;

}

TriggerHandler::~TriggerHandler(){
  while(!m_fes.empty()){
    FrontEnd* fe=m_fes.back();
    m_fes.pop_back();
    delete fe;
  }
  m_fes.clear();
  if(m_rootfile) delete m_rootfile;
  delete m_rn;
}

void TriggerHandler::SetVerbose(bool enable){
  m_verbose=enable;
  for(auto fe: m_fes){
    fe->SetVerbose(enable);
  }
}

void TriggerHandler::SetContext(string context){
  m_backend = context;
}

void TriggerHandler::SetInterface(string interface){
  m_interface = interface;
}

void TriggerHandler::SetMapping(string mapping){
  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Testing: " << sdir << mapping << endl;
   std::ifstream fr(sdir+mapping);
   if(!fr){continue;}
   cout << "Loading: " << sdir << mapping << endl;
   json config;
   fr >> config;
   for(auto node : config["connectivity"]){
     AddMapping(node["name"],node["config"],node["tx"],node["rx"],node["host"],node["cmd_port"],node["host"],node["data_port"]);
   }
   fr.close();
   break;
  }

}

std::vector<FrontEnd*> TriggerHandler::GetFEs(){
  return m_fes;
}

void TriggerHandler::AddMapping(string name, string config, uint32_t cmd_elink, uint32_t data_elink, string cmd_host, uint32_t cmd_port, string data_host, uint32_t data_port){
  if(m_verbose){
    cout << "Add Mapping: "
        << "name: " << name << ", "
        << "config: " << config << ", "
        << "cmd_host: " << cmd_host << ", "
        << "cmd_port: " << cmd_port << ", "
        << "cmd_elink: " << cmd_elink << ", "
        << "data_host: " << data_host << ", "
        << "data_port: " << data_port << ", "
        << "data_elink: " << data_elink << " "
        << endl;
  }
  m_fe[name]=0;
  m_enabled[name]=false;
  m_configs[name]=config;
  m_fe_rx[name]=data_elink;
  m_fe_tx[name]=cmd_elink;
  m_data_port[data_elink]=data_port;
  m_data_host[data_elink]=data_host;
  m_cmd_host[cmd_elink]=cmd_host;
  m_cmd_port[cmd_elink]=cmd_port;
}

void TriggerHandler::AddFE(string name, FrontEnd* fe){
  fe->SetName(name);
  fe->SetActive(true);
  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;
}

void TriggerHandler::AddFE(string name, string path){
  
  if(path==""){path=m_configs[name];}
  if(path==""){path=name+".json";}

  FrontEnd * fe = 0;
  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Testing: " << sdir << path << endl;
   std::ifstream fr(sdir+path);
   if(!fr){continue;}
   cout << "Loading: " << sdir << path << endl;
   json config;
   fr >> config;
   //Create the front-end
   fe = new FrontEnd();
   fe->SetVerbose(m_verbose);
   fe->SetName(name);
   fe->SetChipID(config["FE-I4B"]["Parameter"]["chipId"]);
   fe->SetActive(true);

   //Actually read the file
   //Global config is easy
   map<string,uint32_t> global = config["FE-I4B"]["GlobalConfig"];
   fe->LoadGlobalConfig(global);

   //Pixel config is by rows!!!!
   for(auto prow: config["FE-I4B"]["PixelConfig"]){
     uint32_t row=prow["Row"];
     row--;
     for(uint32_t col=0;col<80;col++){
       fe->LoadPixelConfig(col,row,"Enable",prow["Enable"][col]);
       fe->LoadPixelConfig(col,row,"Hitbus",prow["Hitbus"][col]);
       fe->LoadPixelConfig(col,row,"LCap",prow["LCap"][col]);
       fe->LoadPixelConfig(col,row,"SCap",prow["SCap"][col]);
       fe->LoadPixelConfig(col,row,"TDAC",prow["TDAC"][col]);
       fe->LoadPixelConfig(col,row,"FDAC",prow["FDAC"][col]);
       if (prow["Enable"][col] == 0){
         fe->SetPixelMask(col,row,true);
       }else{
         fe->SetPixelMask(col,row,false);
       }
     }
   }
   
   
   
   m_fe[name]=fe;
   m_fes.push_back(fe);
   m_enabled[name]=true;
   break;
  }
  if(!fe){cout << "File not found: " << path << endl;}
}

void TriggerHandler::SaveFE(FrontEnd * fe, string path){

  cout << "Saving: " << fe->GetName() << " in: " << path << endl;
  json config;
  config["FE-I4B"]["name"]=fe->GetName();
  config["FE-I4B"]["Parameter"]["chipId"]=fe->GetChipID();
  config["FE-I4B"]["GlobalConfig"]=fe->GetConfig()->GetRegisters();

  //Pixel config is by rows!!!!
  for(uint32_t row=0;row<336;row++){
    config["FE-I4B"]["PixelConfig"][row]["Row"]=row+1;
    for(uint32_t col=0;col<80;col++){
      config["FE-I4B"]["PixelConfig"][row]["Enable"][col]=fe->GetPixelConfig(col,row,"Enable");
      config["FE-I4B"]["PixelConfig"][row]["Hitbus"][col]=fe->GetPixelConfig(col,row,"Hitbus");
      config["FE-I4B"]["PixelConfig"][row]["LCap"][col]=fe->GetPixelConfig(col,row,"LCap");
      config["FE-I4B"]["PixelConfig"][row]["SCap"][col]=fe->GetPixelConfig(col,row,"SCap");
      config["FE-I4B"]["PixelConfig"][row]["TDAC"][col]=fe->GetPixelConfig(col,row,"TDAC");
      config["FE-I4B"]["PixelConfig"][row]["FDAC"][col]=fe->GetPixelConfig(col,row,"FDAC");
    }
  }

  ofstream fw(path);
  fw << setw(4) << config;
  fw.close();

}


/*void TriggerHandler::SendExternalTrigger(uint32_t l1id){
  cout << "Process l1id: " << l1id << endl;
  FelixL1AMsg fmsg(m_elinkT,l1id);
  netio::message l1msg((uint8_t*)&fmsg,sizeof(fmsg)); 
  m_trg_sock->publish(m_elinkT,l1msg); 
}*/


void TriggerHandler::Setup(){
  cout << "Create the context" << endl;
  m_context = new netio::context(m_backend.c_str());
  m_context_thread = thread([&](){m_context->event_loop()->run_forever();});

  cout << "Create a trigger-publish socket" << endl;
  m_trg_sock=new netio::publish_socket(m_context, m_trg_port);
  
  m_trg_sock->register_subscribe_callback([&](netio::tag t, netio::endpoint ep){
    cout << "Subscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
  });

  m_trg_sock->register_unsubscribe_callback([&](netio::tag t, netio::endpoint ep){
    cout << "Unsubscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
  });


  /*
  cout << "Create a felix bus" << endl;
  felix::bus::Bus dbus;
  dbus.setVerbose(m_verbose);
  dbus.setZyreVerbose(m_verbose);
  m_dbus=&dbus;
    
  cout << "Get interface" << endl;
  auto address=felix::bus::getIpAddress(m_interface);
  cout << " " << address.first << ":" << address.second << endl;  
  dbus.setInterface(address.first);

  cout << "Connect the bus" << endl;
  dbus.connect();
  

  cout << "Subscribe to the felix table" << endl;
  felix::bus::FelixTable felixtable;
  felix::bus::ElinkTable dataelinks;

  dbus.subscribe("FELIX",&felixtable);
  dbus.subscribe("ELINKS",&dataelinks);
  sleep(1);

  felixtable.print();
  dataelinks.print();
  
  stringstream ss;
  ss<<"tcp://"<<address.second<<":"<<m_trg_port;
  cout << "Add felix: " << ss.str() << endl;
  string felixid=felixtable.addFelix(ss.str());
    
  cout << "Add Elink 0x" << hex << m_elinkT << dec << " to felix " << felixid << endl; 
  dataelinks.addElink(m_elinkT,felixid);
  
  cout << "Publish felix table" << endl;
  dbus.publish("FELIX",felixtable);
  cout << "Publish elink table" << endl;
  dbus.publish("ELINKS",dataelinks);
  */

  cout << "Create a felix bus" << endl;
  m_dbus = new felix::bus::Bus();
  m_dbus->setVerbose(m_verbose);
  m_dbus->setZyreVerbose(m_verbose);
    
  cout << "Get interface" << endl;
  auto address=felix::bus::getIpAddress(m_interface);
  cout << " " << address.first << ":" << address.second << endl;  
  m_dbus->setInterface(address.first);

  cout << "Connect the bus" << endl;
  m_dbus->connect();
  

  cout << "Subscribe to the felix table" << endl;
  felix::bus::FelixTable * felixtable = new felix::bus::FelixTable();
  felix::bus::ElinkTable * dataelinks = new felix::bus::ElinkTable();

  m_dbus->subscribe("FELIX",felixtable);
  m_dbus->subscribe("ELINKS",dataelinks);
  sleep(1);

  felixtable->print();
  dataelinks->print();
  
  stringstream ss;
  ss<<"tcp://"<<address.second<<":"<<m_trg_port;
  cout << "Add felix: " << ss.str() << endl;
  string felixid=felixtable->addFelix(ss.str());
    
  cout << "Add Elink 0x" << hex << m_elinkT << dec << " to felix " << felixid << endl; 
  dataelinks->addElink(m_elinkT,felixid);
  
  cout << "Publish felix table" << endl;
  m_dbus->publish("FELIX",*felixtable);
  cout << "Publish elink table" << endl;
  m_dbus->publish("ELINKS",*dataelinks);

}


void TriggerHandler::Connect(){
  //TX
  for(auto it : m_fe_tx){
    if(m_enabled[it.first]==false){continue;}
    uint32_t tx_elink = it.second;
    if(m_tx.count(tx_elink)==0){
      cout << "Connect to cmd elink: " << tx_elink << " at " << m_cmd_host[tx_elink] << ":" << m_cmd_port[tx_elink] << endl;
      m_tx[tx_elink]=new netio::low_latency_send_socket(m_context);
      m_tx[tx_elink]->connect(netio::endpoint(m_cmd_host[tx_elink],m_cmd_port[tx_elink]));
    }
    m_tx_fes[tx_elink].push_back(m_fe[it.first]);
  }

  //RX
  /*for(auto it : m_fe_rx){
    if(m_enabled[it.first]==false){continue;}
    uint32_t rx_elink = it.second;
    m_rx_fe[rx_elink] = m_fe[it.first];
    m_mutex[rx_elink].unlock();
    m_rx[rx_elink] = new netio::low_latency_subscribe_socket(m_context, [&,rx_elink](netio::endpoint& ep, netio::message& msg){
      m_mutex[rx_elink].lock();
      if(m_verbose) cout << "Received data from " << ep.address() << ":" << ep.port() << " size:" << msg.size() << endl;
      //cout << "Wohoooo I JUST GOT DATA!!!" << endl;
      vector<uint8_t> data = msg.data_copy();
      //We should remove any potential header before decoding
      FelixDataHeader hdr;
      memcpy(&hdr,(const void*)&data[0], sizeof(hdr));
      m_rx_fe[rx_elink]->HandleData(&data[sizeof(hdr)],data.size()-sizeof(hdr));
      m_mutex[rx_elink].unlock();
    });
    cout << "Subscribe to data elink: " << rx_elink << " at " << m_data_host[rx_elink] << ":" << m_data_port[rx_elink] << endl;
    m_rx[rx_elink]->subscribe(rx_elink, netio::endpoint(m_data_host[rx_elink], m_data_port[rx_elink]));
  }*/
  //RX
  
}

void TriggerHandler::SetTriggerElink(uint32_t elinkT){
  m_elinkT=elinkT;
}

void TriggerHandler::Config(){
  cout << "Starting run number: " << m_rn->GetNextRunNumberString(6,true) << endl;

  for(auto fe : m_fes){
    //Set to configuration mode
    fe->SetRunMode(false);
    Send(fe);

    fe->SetRunMode(false);

    //configure global registers
    fe->ConfigGlobal();
    Send(fe);

    //configure double columns
    for(uint32_t i=0;i<40;i++){
      fe->ConfigDoubleColumn(0,i);
      Send(fe);
    }

    //Send a BCR+ECR
    fe->Reset();
    Send(fe);
  }

  //Create the output directory
  ostringstream cmd;
  cmd << "mkdir -p " << m_outpath << "/" << m_rn->GetRunNumberString(6);
  system(cmd.str().c_str());

  //Open the output ROOT file
  ostringstream opath;
  opath << m_outpath << "/" << m_rn->GetRunNumberString(6) << "/" << "output.root";
  m_rootfile=TFile::Open(opath.str().c_str(),"RECREATE");

}

void TriggerHandler::PrepareTrigger(uint32_t cal_delay){
  for(auto it : m_tx_fes){
    FrontEnd * fe = it.second.at(0);
    fe->Trigger(cal_delay);
    fe->ProcessCommands();
    m_trigger_msgs[it.first]=vector<uint8_t>();
    for(uint32_t i=0;i<fe->GetLength();i++){
      m_trigger_msgs[it.first].push_back(fe->GetBytes()[i]);
    }
    for(uint32_t i=0;i<4;i++){
      m_trigger_msgs[it.first].push_back(0);
    }
    fe->Clear();
  }
}

void TriggerHandler::Trigger(){

  if(m_verbose) cout << "Trigger!" << endl;

  FelixL1AMsg fmsg(m_elinkT,m_l1id);
  netio::message l1msg((uint8_t*)&fmsg,sizeof(fmsg)); 
  m_trg_sock->publish(m_elinkT,l1msg); 
  m_l1id++;

  for(auto it : m_tx){
    //FrontEnd * fe = m_tx_fes[it.first].at(0);
    //fe->Trigger(50);
    //fe->ProcessCommands();
    //fe->Clear();


    FelixCmdHeader hdr;
    hdr.elink=it.first;
    hdr.length=m_trigger_msgs[it.first].size();
    netio::message msg;
    msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
    msg.add_fragment(m_trigger_msgs[it.first].data(), m_trigger_msgs[it.first].size());
    //send message
    it.second->send(msg);

    if(m_verbose){
      cout << "Message: 0x" << hex;
      for(uint32_t i=0;i<msg.size();i++){
        cout << setw(2) << setfill('0') << ((uint32_t) msg[i]);
      }
      cout << dec << endl;
    }
    //std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
}

void TriggerHandler::Send(FrontEnd * fe){
  //process the commands from the front-end
  fe->ProcessCommands();
  //quick return
  if(fe->GetLength()==0){return;}
  //figure out the tx_elink
  uint32_t tx_elink=m_fe_tx[fe->GetName()];
  //copy the message
  vector<uint8_t> payload(fe->GetLength());
  for(uint32_t i=0;i<fe->GetLength();i++){payload[i]=fe->GetBytes()[i];};
  //build message
  FelixCmdHeader hdr;
  hdr.elink=tx_elink;
  hdr.length=fe->GetLength();
  netio::message msg;
  msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
  //msg.add_fragment(fe->GetBytes(), fe->GetLength());
  msg.add_fragment(payload.data(), payload.size());
  //send message
  m_tx[tx_elink]->send(msg);
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  fe->Clear();
}

void TriggerHandler::Disconnect(){

  for(auto it : m_tx){
    cout << "Disconnect from cmd elink: " << it.first << " at " << m_cmd_host[it.first] << ":" << m_cmd_port[it.first] << endl;
    it.second->disconnect();
    delete it.second;
  }
  m_tx.clear();

}

void TriggerHandler::Terminate(){

  
  cout << "Stop event loop" << endl;
  m_context->event_loop()->stop();

  cout << "Join context thread" << endl;
  m_context_thread.join();

  cout << "Delete the context" << endl;
  delete m_context;
}

void TriggerHandler::Run(){
  cout << "FIXME: TriggerHandler::Run is supposed to be extended" << endl;
}

void TriggerHandler::Save(){

  string path="./tuned/";

  system("mkdir -p ./tuned");

  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}
    ostringstream os;
    os << path << m_configs[fe->GetName()];
    SaveFE(fe,os.str());
  }

  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}
    ostringstream os;
    os << m_outpath << "/" << m_rn->GetRunNumberString(6) << "/" << m_configs[fe->GetName()];
    SaveFE(fe,os.str());
  }

  if(m_rootfile) m_rootfile->Close();
}

void TriggerHandler::SetOutPath(std::string path){
  m_outpath=path;
}

std::string TriggerHandler::GetOutPath(){
  return m_outpath;
}

void TriggerHandler::SetCharge(uint32_t charge){
  m_charge=charge;
}

uint32_t TriggerHandler::GetCharge(){
  return m_charge;
}

void TriggerHandler::SetRetune(bool retune){
  m_retune=retune;
}

bool TriggerHandler::GetRetune(){
  return m_retune;
}
