tdaq_package()

set(JSONMCPP_DIR "$ENV{LCG_RELEASE_BASE}/LCG_${LCG_VERSION}/jsonmcpp/${JSONMCPP_VERSION}/${BINARY_TAG}/include")
include_directories(${JSONMCPP_DIR} ${JSONMCPP_DIR}/nlohmann)
message(STATUS "Found jsonmcpp: ${JSONMCPP_DIR}")

tdaq_add_library(FEI4B
                 src/Configuration.cpp
                 src/Register.cpp
                 src/Field.cpp
                 src/DoubleColumn.cpp
                 src/Pixel.cpp
                 src/Hit.cpp
                 src/Emulator.cpp
                 src/FrontEnd.cpp
                 src/Handler.cpp
                 src/Encoder.cpp
                 src/Command.cpp
                 src/Trigger.cpp
                 src/BCR.cpp
                 src/ECR.cpp
                 src/Cal.cpp
                 src/RdReg.cpp
                 src/WrReg.cpp
                 src/WrFE.cpp
                 src/Reset.cpp
                 src/Pulse.cpp
                 src/RunMode.cpp
                 src/Unknown.cpp
                 src/Decoder.cpp
                 src/Record.cpp
                 src/DataHeader.cpp
                 src/DataRecord.cpp
                 src/AddressRecord.cpp
                 src/ValueRecord.cpp
                 src/ServiceRecord.cpp
                 src/EmptyRecord.cpp
                 src/Position.cpp
                 src/Tools.cpp
                 src/RunNumber.cpp
                 LINK_LIBRARIES tdaq::netio ROOT::RIO
                )

tdaq_add_executable(scan_manager_fei4b 
                 src/scan_manager_fei4b.cpp
                 src/RegisterScan.cpp
                 src/DigitalScan.cpp
                 src/AnalogScan.cpp
                 src/GlobalThresholdTune.cpp
                 src/PixelThresholdTune.cpp
                 src/ThresholdScan.cpp
                 src/GlobalTotTune.cpp
                 src/PixelTotTune.cpp
                 src/TotScan.cpp
                 src/NoiseScan.cpp
                 src/SelfTriggerScan.cpp
                 src/OliverScan.cpp
                 LINK_LIBRARIES FEI4B tdaq::cmdline tdaq::netio pthread tdaq::felixbus ROOT ROOT::Hist)

tdaq_add_library(TriggerHandler
                src/TriggerHandler.cpp
                src/TriggerDigitalScan.cpp
                src/TriggerAnalogScan.cpp
		        src/TriggerThresholdScan.cpp
		        LINK_LIBRARIES FEI4B tdaq::cmdline tdaq::netio pthread tdaq::felixbus tdaq::json ROOT ROOT::Hist)

tdaq_add_executable(test_fei4b_encoder src/test_fei4b_encoder.cpp LINK_LIBRARIES tdaq::cmdline FEI4B)

tdaq_add_executable(test_fei4b_decoder src/test_fei4b_decoder.cpp LINK_LIBRARIES tdaq::cmdline FEI4B)

tdaq_add_executable(felix_fei4b_emulator src/felix_fei4b_emulator.cpp LINK_LIBRARIES tdaq::cmdline FEI4B tdaq::netio pthread)

tdaq_add_executable(felix_fei4b_emulator2 src/felix_fei4b_emulator2.cpp LINK_LIBRARIES tdaq::cmdline tdaq::felixbus FEI4B tdaq::netio pthread)

tdaq_add_executable(decode_fei4b_commands src/decode_fei4b_commands.cpp LINK_LIBRARIES FEI4B tdaq::cmdline)

tdaq_add_scripts(share/flx*)

tdaq_add_data(share/*.json PACKAGE config)

tdaq_add_data(share/*.md)
