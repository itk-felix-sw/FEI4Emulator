#ifndef FEI4B_EMPTYRECORD_H
#define FEI4B_EMPTYRECORD_H

#include "FEI4Emulator/Record.h"

namespace fei4b{

/**
 * The EmptyRecord eases the recognition of the end of the stream
 * when the 8b10b encoding is not enabled.
 * If the 8b10b encoding is enabled, this record is no present.
 * It contains three sequences of an 8-bit pattern 
 * (EmptyRecord::SetPattern, EmptyRecord::GetPattern).
 * 
 * Encoding (EmptyRecord::AddBytes) and decoding (EmptyRecord::SetBytes) 
 * require that the pattern it defined externally.
 *
 * The bit content is the following:
 *
 * | Bit  | 23 | 16 | 15 |  8 |  7 |  0 |
 * | ---  | -- | -- | -- | -- | -- | -- |
 * | Rel  |  7 |  0 |  7 |  0 |  7 |  0 |
 * | Byte |  0     ||  1     ||  3     ||
 * | Desc |Pattern ||Pattern ||Pattern ||
 * | Size |  8     ||  8     || 8      ||
 * 
 *
 * @brief FEI4B empty record
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class EmptyRecord: public Record{
  
 public:

  /** 
   * Default constructor
   **/
  EmptyRecord();

  /** 
   * Create a new object from another one
   * @param copy Pointer to DataRecord to copy 
   **/
  EmptyRecord(EmptyRecord *copy);

  /**
   * Destructor
   **/
  ~EmptyRecord();

  /**
   * Return a human readable representation of the register
   * @return The human readable representation of the register
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the record pattern
   * @param pattern the record pattern
   **/
  void SetPattern(uint32_t pattern);

  /**
   * Get the record pattern
   * @return the record pattern
   **/
  uint32_t GetPattern();
  
 protected:

  uint32_t m_pattern;

};

}
#endif 
