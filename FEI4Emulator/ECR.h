#ifndef FEI4B_ECR_H
#define FEI4B_ECR_H

#include "FEI4Emulator/Command.h"

namespace fei4b{

/**
 * This is an Event Counter Reset Command.
 * It is composed of a 5-bit identifier 0b10110,
 * that is NOT tolerant to 1-bit flips, followed by a
 * 4-bit field identifier 0b0010.
 *
 * The bit content is the following:
 *
 * | Bit   |  9 |  5 |  4 |  0 |
 * | ---   | -- | -- | -- | -- |
 * | Rel   |  5 |  0 |  4 | -- |
 * | Field |  1     ||  2     ||
 * | Desc  |0b10110 || 0b0010 ||
 * | Size  |  5     ||  4     ||
 *
 *
 * @brief FEI4B ECR command
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class ECR: public Command{
  
 public:

  /** 
   * Default constructor
   **/
  ECR();

  /** 
   * Create a new object from another one
   * @param copy Pointer to copy 
   **/
  ECR(ECR *copy);
  
  /**
   * Destructor
   **/
  ~ECR();

  /**
   * Create a new object from this one
   * @return copy Pointer to copy of this one 
   **/
  ECR * Clone();

  /**
   * Return a human readable representation
   * @return The human readable representation
   **/
  std::string ToString();
  
  /**
   * Extract the contents from the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the new Position in the byte array
   **/
  Position UnPack(uint8_t * bytes, Position pos, uint32_t maxlen);
  
  /**
   * Place the contents in the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @return the new Position in the byte array
   **/
  Position Pack(uint8_t * bytes, Position pos);
  
  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();
    
 protected:

  uint32_t m_field1;
  uint32_t m_field2;

};

}
#endif 
