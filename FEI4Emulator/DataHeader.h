#ifndef FEI4B_DATAHEADER_H
#define FEI4B_DATAHEADER_H

#include "FEI4Emulator/Record.h"

namespace fei4b{

/**
 * The DataHeader Record is the header for the transmission of pixel data.
 * It is composed of an identifier 0b11101 that is tolerant to 1-bit flips,
 * followed by a unique code for the data header 0b001, a 1-bit error 
 * flag (DataHeader::SetErrorFlag,DataHeader::GetErrorFlag),
 * a 5-bit value (DataHeader::SetL1ID, DataHeader::GetL1ID) that corresponds 
 * to the LSBs of the 19-bit Level 1 ID, and a 9-bit value 
 * (DataHeader::SetBCID, DataHeader::GetBCID) that corresponds to the 
 * 13-bit BCID.
 *
 * The bit content is the following:
 *
 * | Bit  | 23 | 19 | 18 | 16 | 15 | 14 | 10 |  9 |  8 |  7 |  0 |
 * | ---  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Rel  |  7 |  0 |  7 |  0 |  7 |  6 |  2 |  1 |  0 |  7 |  0 |
 * | Byte |  0             ||||    1               |||||  2     ||
 * | Desc |0b11101 ||0b001   || EF | L1ID   || BCID           ||||
 * | Size |  5     || 3      || 1  | 5      || 10             ||||
 * 
 * Possible patterns for the header identifier are:
 * 
 * | ID (Bin) | ID (Hex) | Byte (Bin) | Byte (Hex) | 
 * | -------- | -------- | ---------- | ---------- |
 * | 0b11101  | 0x1D     | 0b11101001 | 0xE9       |
 * | 0b11100  | 0x1C     | 0b11100001 | 0xE1       |
 * | 0b11111  | 0x1F     | 0b11111001 | 0xF9       |
 * | 0b11001  | 0x19     | 0b11001001 | 0xC9       |
 * | 0b10101  | 0x15     | 0b10101001 | 0xA9       |
 * | 0b01101  | 0x0D     | 0b01101001 | 0x69       |
 *
 *
 * @brief FEI4B data header record
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class DataHeader: public Record{
  
 public:

  /** 
   * Default constructor
   **/
  DataHeader();

  /** 
   * Create a new object from another one
   * @param copy Pointer to DataRecord to copy 
   **/
  DataHeader(DataHeader *copy);

  /**
   * Create a header with values. 
   * @param l1id The Level 1 ID.
   * @param bcid The BCID.
   * @param error Enable or disable the error flag
   **/
  DataHeader(uint32_t l1id, uint32_t bcid, bool error=0);

  /**
   * Destructor
   **/
  ~DataHeader();

  /**
   * Return a human readable representation of the register
   * @return The human readable representation of the register
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the error flag
   * @param enable Enable or disable the error flag
   **/
  void SetErrorFlag(bool enable);

  /**
   * Get the error flag
   * @return the error flag
   **/
  bool GetErrorFlag();

  /**
   * Set the Level 1 ID. 
   * @param l1id The Level 1 ID.
   **/
  void SetL1ID(uint32_t l1id);

  /**
   * Get the Level 1 ID.
   * @return the Level 1 ID.
   **/
  uint32_t GetL1ID();
  
  /**
   * Set the BCID.
   * @param bcid The BCID.
   **/
  void SetBCID(uint32_t bcid);

  /**
   * Get the BCID
   * @return the BCID 
   **/
  uint32_t GetBCID();
  
 protected:

  uint8_t m_id;
  uint32_t m_l1id;
  uint32_t m_bcid;
  bool m_flag;

};

}
#endif 
