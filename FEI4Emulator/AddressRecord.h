#ifndef FEI4B_ADDRESSRECORD_H
#define FEI4B_ADDRESSRECORD_H

#include "FEI4Emulator/Record.h"

namespace fei4b{

/**
 * The AddressRecord is the address of a global register, 
 * or the position of the shift register.
 * It is composed of an identifier 0b11101 that is tolerant to 1-bit flips,
 * followed by a unique code 0b010, 
 * a 1-bit type (AddressRecord::GLOBAL, AddressRecord::SHIFT) 
 * (DataHeader::SetAddressType,DataHeader::GetAddressType),
 * and a 15-bit address (DataHeader::SetAddress, DataHeader::GetAddress).
 *
 * The bit content is the following:
 *
 * | Bit  | 23 | 19 | 18 | 16 | 15   | 14 |  8 |  7 |  0 |
 * | ---  | -- | -- | -- | -- | --   | -- | -- | -- | -- |
 * | Rel  |  7 |  0 |  7 |  0 |  7   |  6 |  0 |  7 |  0 |
 * | Byte |  0             ||||  1           |||  2     ||
 * | Desc |0b11101 || 0b010  || Type | Address        ||||
 * | Size |  5     || 3      || 1    | 15             ||||
 * 
 * Possible patterns for the header identifier are:
 * 
 * | ID (Bin) | ID (Hex) | Byte (Bin) | Byte (Hex) | 
 * | -------- | -------- | ---------- | ---------- |
 * | 0b11101  | 0x1D     | 0b11101010 | 0xEA       |
 * | 0b11100  | 0x1C     | 0b11100010 | 0xE2       |
 * | 0b11111  | 0x1F     | 0b11111010 | 0xFA       |
 * | 0b11001  | 0x19     | 0b11001010 | 0xCA       |
 * | 0b10101  | 0x15     | 0b10101010 | 0xAA       |
 * | 0b01101  | 0x0D     | 0b01101010 | 0x6A       |
 *
 *
 * @brief FEI4B address record
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class AddressRecord: public Record{
  
 public:

  /** 
   * Identifier for a global register
   **/
  static const uint32_t GLOBAL=0;
  
  /** 
   * Identifier for a shift register
   **/
  static const uint32_t SHIFT=1; 

  /** 
   * Default constructor
   **/
  AddressRecord();

  /** 
   * Create a new object from another one
   * @param copy Pointer to AddressRecord to copy 
   **/
  AddressRecord(AddressRecord *copy);

  /** 
   * Create and AddressRecord with contents
   * @param addr The address
   * @param type The type
   **/
  AddressRecord(uint32_t addr, uint32_t type=GLOBAL);

  /**
   * Destructor
   **/
  ~AddressRecord();

  /**
   * Return a human readable representation of the register
   * @return The human readable representation of the register
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the address type (AddressRecord::GOBAL, AddressRecord::SHIFT) 
   * @param type The type of the address record
   **/
  void SetAddressType(uint32_t type);

  /**
   * Get the address type (AddressRecord::GLOBAL, AddressRecord::SHIFT)
   * @return the type of the address record
   **/
  uint32_t GetAddressType();

  /**
   * Set the address
   * @param addr Address
   **/
  void SetAddress(uint32_t addr);

  /**
   * Get the address
   * @return the address
   **/
  uint32_t GetAddress();
    
 protected:

  uint32_t m_id;
  uint32_t m_type;
  uint32_t m_addr;

};

}
#endif 
