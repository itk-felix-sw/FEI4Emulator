#ifndef FEI4B_DIGITALSCAN_H
#define FEI4B_DIGITALSCAN_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * A DigitalScan injects a signal in the digital circuit of the pixel, and checks for the corresponding hit in the data.
 * Because the output data bandwidth is limited, the injection is done in a few pixels at a time
 * and in groups of double column.
 * The analysis of the digital scan checks that every single pixel has the required number of hits.
 * If there is a discrepancy in the number of hits in a given set of pixels,
 * it is possible to generate a mask to ignore those pixels in subsequent scans.
 *
 * The digital injection pulse is ORed with the comparator output.
 * Therefore, the analog state must be below threshold, or the discriminator must be off (bias current set to zero)
 * for digital injection to work.
 * This is accomplished by setting the threshold to a high value (Configuration::Vthin_Fine=200),
 * enabling digital injection (Configuration::DigHitIn_Sel=1), selecting the double column for the injection
 * (Configuration::Colpr_Addr, Configuration::Colpr_Mode),
 * and writing a 1 to the shift register of the desired pixel in the addressed double column.
 *
 * The addressing of the digital pulse is given by @f$ (2n+1 and 2n+2) @f$ that is accomplished with
 * FrontEnd::SelectDoubleColumn.
 *
 *
 * @brief FEI4B DigitalScan
 * @author Carlos.Solans@cern.ch
 * @date June 2020
 */

class DigitalScan: public Handler{

public:

  /**
   * Create the scan
   */
  DigitalScan();

  /**
   * Delete the Handler
   */
  virtual ~DigitalScan();

  /**
   * Create histograms, disable all pixels per front-end.
   * Loop the matrix in groups of pixels and double columns.
   * Trigger few events and collect the hits per trigger and per FrontEnd.
   * An occupancy histogram is filled per front-end, that is required
   * to have the same amount of entries or less than the number of triggers.
   * A pixel will be masked if it has more than the expected number of entries.
   */
  virtual void Run();

private:

};

}

#endif
