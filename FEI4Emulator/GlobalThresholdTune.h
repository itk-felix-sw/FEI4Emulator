#ifndef FEI4B_GLOBALTHRESHOLDTUNE_H
#define FEI4B_GLOBALTHRESHOLDTUNE_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * The GlobalThresholdTune scan changes the 16-bit global threshold of the FEI4B, composed of
 * an 8-bit Configuration::Vthin_Coarse value and an 8-bit Configuration::Vthin_Fine value,
 * in order to tune a new global threshold for the chip.
 * This is done by injecting a known charge (Handler::SetCharge, Handler::GetCharge),
 * with a default value of 3000 electrons, into the analog circuit of the Pixel,
 * and evaluating the mean occupancy of the chip for each threshold setting
 * until the occupancy is 50%.
 *
 * The initial value for Configuration::Vthin_Coarse is 0, and for Configuration::Vthin_Fine is 50,
 * unless the retune flag has been set, in which case the pre-existing value is read from a configuration file.
 * The analysis provides the next global value for the threshold.
 *
 * The first part of the scan sets the global threshold value (Configuration::Vthin_Coarse, and Configuration::Vthin_Fine),
 * after which the scan loops over the different masks of the chip and the double columns.
 * Charge is then injected and sends the triggers to the chip and the data is subsequently read out from each chip.
 * The occupancy per chip is accumulated, and evaluated after the mask loop.
 * If the mean occupancy of the chip is larger than 50%, the value of the threshold is increased,
 * and if it's lower, the value of the threshold is raised.
 * The algorithm converges when the mean occupancy of the chip is between 49% and 51% of the 98% of the unmasked pixels.
 *
 * This scan does not mask pixels at the end and requires high voltage to be applied on the front-end.
 *
 *
 * @brief FEI4B GlobalThresholdTune
 * @author Carlos.Solans@cern.ch
 * @date June 2020
 */

class GlobalThresholdTune: public Handler{

public:

  /**
   * Create the scan
   */
  GlobalThresholdTune();

  /**
   * Delete the scan
   */
  virtual ~GlobalThresholdTune();

  /**
   * Configure the front-end threshold to the initial value or read from file.
   * Start a loop that will only finish if the new threshold is found,
   * followed by a mask loop, and a double column loop, in which the front-ends
   * are triggered and their hits collected.
   * The hits are analysed for the whole matrix, and a new threshold is proposed,
   * until the algorithm converges.
   *
   * The new threshold is proposed as bisection of high and low values of the threshold,
   * if the mean occupancy is higher than 0.51 % the upper threshold is reduced,
   * and if the mean is lower than 0.49 % the lower threshold is raised.
   * The new threshold is then proposed after the range is defined.
   */
  virtual void Run();

private:

};

}

#endif
