#ifndef FEI4B_VALUERECORD_H
#define FEI4B_VALUERECORD_H

#include "FEI4Emulator/Record.h"

namespace fei4b{

/**
 * The ValueRecord is the value of a global register, 
 * or the value contained in the shift register.
 * It is composed of an identifier 0b11101 that is tolerant to 1-bit flips,
 * followed by a unique code 0b100, 
 * and a 16-bit value (DataHeader::SetValue, DataHeader::GetValue).
 *
 * The bit content is the following:
 *
 * | Bit  | 23 | 19 | 18 | 16 | 15 |  8 |  7 |  0 |
 * | ---  | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Rel  |  7 |  0 |  7 |  0 |  7 |  0 |  7 |  0 |
 * | Byte |  0             ||||  1     ||  2     ||
 * | Desc |0b11101 || 0b100  || Value          ||||
 * | Size |  5     || 3      || 16             ||||
 * 
 * Possible patterns for the header identifier are:
 * 
 * | ID (Bin) | ID (Hex) | Byte (Bin) | Byte (Hex) | 
 * | -------- | -------- | ---------- | ---------- |
 * | 0b11101  | 0x1D     | 0b11101100 | 0xEC       |
 * | 0b11100  | 0x1C     | 0b11100100 | 0xE4       |
 * | 0b11111  | 0x1F     | 0b11111100 | 0xFC       |
 * | 0b11001  | 0x19     | 0b11001100 | 0xCC       |
 * | 0b10101  | 0x15     | 0b10101100 | 0xAC       |
 * | 0b01101  | 0x0D     | 0b01101100 | 0x6C       |
 *
 *
 * @brief FEI4B value record
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class ValueRecord: public Record{
  
 public:

  /** 
   * Default constructor
   **/
  ValueRecord();

  /** 
   * Create a new object from another one
   * @param copy Pointer to ValueRecord to copy 
   **/
  ValueRecord(ValueRecord *copy);

  /**
   * Create a ValueRecord with contents
   * @param value The value for the record
   **/
  ValueRecord(uint32_t value);
  
  /**
   * Destructor
   **/
  ~ValueRecord();

  /**
   * Return a human readable representation of the register
   * @return The human readable representation of the register
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the value
   * @param addr Value
   **/
  void SetValue(uint32_t addr);

  /**
   * Get the value
   * @return the value
   **/
  uint32_t GetValue();
    
 protected:

  uint32_t m_id;
  uint32_t m_value;

};

}
#endif 
