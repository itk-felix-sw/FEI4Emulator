#ifndef FEI4B_HIT_H
#define FEI4B_HIT_H

#include <cstdint>
#include <string>

namespace fei4b{

/**
 * A Hit representation containing the event number, L1ID, BCID, column, row and TOT.
 * A new Hit is created by the FrontEnd::HandleData method by combining
 * DataHeader that contains the LSBs for L1ID and BCID,
 * ServiceRecord of type 14 that contains the MSBs of the L1ID and BCID,
 * and the DataRecord that contains the TOT of the given column and row and
 * from the neighboring row.
 *
 * The event number is not provided in the FEI4B output data format.
 * This is a counter that can be used by the user.
 *
 * @verbatim

 Hit hit;
 vector<Hit*> hits;

 for(auto record: decoder->GetRecords()){
    if(record->GetType()==Record::DATA_HEADER){
      DataHeader * rec=dynamic_cast<DataHeader*>(record);
      hit.Update(rec->GetL1ID(),rec->GetBCID());
    }else if(record->GetType()==Record::SERVICE_RECORD){
      ServiceRecord * rec=dynamic_cast<ServiceRecord*>(record);
      if(rec->GetCode()==14){
        hit.Update(rec->GetL1IDbits(true),rec->GetBCIDbits(true),true);
      }
    }else if(record->GetType()==Record::DATA_RECORD){
      DataRecord * rec=dynamic_cast<DataRecord*>(record);
      if(rec->GetTOT1()>0){
        Hit * nhit = hit.Clone();
        nhit->Set(rec->GetColumn(),rec->GetRow(),rec->GetTOT1());
        hits.push_back(nhit);
      }
      if(rec->GetTOT2()>0){
        Hit * nhit = hit.Clone();
        nhit->Set(rec->GetColumn(),rec->GetRow()+1,rec->GetTOT2());
        hits.push_back(nhit);
      }
    }
  }

   @endverbatim
 *
 * @brief FEI4B Hit
 * @author Carlos.Solans@cern.ch
 * @date June 2020
 **/

class Hit{

public:

  /**
   * Create a new Hit
   */
  Hit();

  /**
   * Delete the Hit
   */
  ~Hit();

  /**
   * Create a Hit with L1ID, BCID
   * @param l1id The L1ID counter
   * @param bcid The BCID counter
   */
  Hit(uint32_t l1id, uint32_t bcid);

  /**
   * Clone the hit
   * @return a Hit pointer cloned from this one
   */
  Hit * Clone();

  /**
   * Update the L1ID and BCID counters
   * @param l1id The L1ID counter
   * @param bcid The BCID counter
   * @param top Only top part of the counters
   * @todo Hit::Update might be flawed
   */
  void Update(uint32_t l1id, uint32_t bcid, bool top=true);

  /**
   * Set the col, row and tot at once
   * @param col the column number
   * @param row the row number
   * @param tot the TOT
   */
  void Set(uint32_t col, uint32_t row, uint32_t tot);

  /**
   * Get the column of this hit
   * @return The column of this hit
   */
  uint32_t GetCol();

  /**
   * Get the row of this hit
   * @return The row of this hit
   */
  uint32_t GetRow();

  /**
   * Get the time over threshold of this hit
   * @return The time over threshold of this hit
   */
  uint32_t GetTOT();

  /**
   * Get the L1ID of this hit
   * @return The L1ID of this hit
   */
  uint32_t GetL1ID();

  /**
   * Get the event number of this hit
   * @return The event number of this hit
   */
  uint32_t GetEvNum();

  /**
   * Get the BCID of this hit
   * @return The BCID of this hit
   */
  uint32_t GetBCID();

  /**
   * Set the column of this hit
   * @param col The column of this hit
   */
  void SetCol(uint32_t col);

  /**
   * Set the row of this hit
   * @param row The row of this hit
   */
  void SetRow(uint32_t row);

  /**
   * Set the time over threshold of this hit
   * @param tot The time over threshold of this hit
   */
  void SetTOT(uint32_t tot);

  /**
   * Set the L1ID of this hit
   * @param l1id The L1ID of this hit
   */
  void SetL1ID(uint32_t l1id);

  /**
   * Set the event number of this hit
   * @param evnum The event number of this hit
   */
  void SetEvNum(uint32_t evnum);

  /**
   * Set the BCID of this hit
   * @param bcid The BCID of this hit
   */
  void SetBCID(uint32_t bcid);

  /**
   * Return the string representation of this hit
   * @return The human readable representation of this hit
   */
  std::string ToString();

private:

  uint32_t m_col;
  uint32_t m_row;
  uint32_t m_tot;
  uint32_t m_l1id;
  uint32_t m_bcid;
  uint32_t m_evnum;

};

}

#endif
