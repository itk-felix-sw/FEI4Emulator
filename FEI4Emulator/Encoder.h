#ifndef FEI4B_ENCODER_H
#define FEI4B_ENCODER_H

#include "FEI4Emulator/Command.h"
#include "FEI4Emulator/Trigger.h"
#include "FEI4Emulator/BCR.h"
#include "FEI4Emulator/ECR.h"
#include "FEI4Emulator/Cal.h"
#include "FEI4Emulator/RdReg.h"
#include "FEI4Emulator/WrReg.h"
#include "FEI4Emulator/WrFE.h"
#include "FEI4Emulator/Reset.h"
#include "FEI4Emulator/Pulse.h"
#include "FEI4Emulator/RunMode.h"
#include "FEI4Emulator/Unknown.h"

#include <cstdint>
#include <vector>

namespace fei4b{

/**
 * This class encodes FEI4B specific commands into a byte stream
 * (Encoder::Encode), and decodes a byte stream into FEI4B specific
 * commands (Encoder::Decode).
 *
 * The FEI4B commands are described by the class Command,
 * and can be added to the byte stream (Encoder::AddCommand) in the
 * same order they should appear in the byte stream.
 * The Commands added to the encoder will be owned by the encoder,
 * and should be deleted by the user. 
 * The byte stream contents (Encoder::GetBytes) and length (Encoder::GetLength)
 * can be accessed but not deleted.
 *
 * Similarly, a byte stream can be decoded by the Encoder::SetBytes.
 * The commands are available from Encoder::GetCommands.
 *
 * @verbatim
 
 Encoder enc;
 enc.AddCommand(new ECR());
 enc.AddCommand(new Pulse());
 enc.AddCommand(new Trigger());
 
 //Encode into bytes
 enc.Encode();
 uint8_t * bytes = enc.GetBytes();
 uin32_t length = enc.GetLength();

 //Decode into commands
 enc.Decode();
 enc.SetBytes(bytes, length);
 vector<Command*> cmds = enc.GetCommands();

   @endverbatim
 *
 * @brief FEI4B Command encoder/decoder
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class Encoder{
    
 public:

  /**
   * Initialize the RD53A data symbol dictionaries
   **/
  Encoder();
  
  /**
   * Delete the commands in memory
   **/
  ~Encoder();
  
  /**
   * Add bytes to the already existing byte array
   * @param bytes byte array
   * @param pos starting index of the byte array
   * @param len number of bytes to add
   **/
  void AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len);

  /**
   * Replace the bytes of the byte array.
   * @param bytes byte array
   * @param len number of bytes to add
   **/
  void SetBytes(uint8_t *bytes, uint32_t len);
  
  /**
   * Add a command to the end of the command list
   * @param cmd RD53A specific command
   **/
  void AddCommand(Command *cmd);
  
  /**
   * Clear the byte array
   **/
  void ClearBytes();
  
  /**
   * Clear the command list by deleting each
   * object in the list.
   **/
  void ClearCommands();
  
  /**
   * Clear the byte array and the command list
   **/
  void Clear();
  
  /**
   * Get a string representation of the bytes. 
   * @return a string in hexadecimal
   **/
  std::string GetByteString();

  /**
   * @return the byte array
   **/
  uint8_t * GetBytes();
  
  /**
   * @return the size of the byte array
   **/
  uint32_t GetLength();

  /**
   * Encode the commands into a byte array
   **/
  void Encode();

  /**
   * Decode the byte array into commands
   **/
  void Decode();
  
  /**
   * Get the list of commands
   * @return vector of RD53ACommand pointers
   **/
  std::vector<Command*> & GetCommands();
  
 private:
  
  std::vector<Command*> m_cmds;
  std::vector<uint8_t> m_bytes;
  uint32_t m_length;
  
  Trigger m_trg;
  BCR m_bcr;
  ECR m_ecr;
  Cal m_cal;
  RdReg m_rdreg;
  WrReg m_wrreg;
  WrFE m_wrfe;
  Reset m_reset;
  Pulse m_pulse;
  RunMode m_run;
  Unknown m_ukn;

};

}
#endif
