#ifndef FEI4B_DOUBLECOLUMN_H
#define FEI4B_DOUBLECOLUMN_H

#include "FEI4Emulator/Pixel.h"

#include <vector>

namespace fei4b{
/**
 * The FEI4B Pixel matrix is divided into 40 double columns.
 * The DoubleColumn represents one of these double columns,
 * that has 336 rows of Pixels and 2 columns, and can be used
 * to program the in Pixel registers that are not available otherwise.
 *
 * Each of the 13 bits of the in Pixel configuration is accessible one at a time
 * for the whole DoubleColumn through a 672 shift register.
 * The value of the shift register can be written with the WrFE Command,
 * where the first bit in the register corresponds to 1,335, and the first one
 * to 0,335.
 *
 * | Row/Col |   0 |   1 |
 * | ------- | --- | --- |
 * | 0       | 335 | 336 |
 * | ...     | ... | ... |
 * | 335     |   0 | 671 |
 *
 * The Pulse Command in conjunction with the Configuration::PixelLatch
 * global register can be used to write the value of the shift register
 * into in Pixel bit. The combination of registers Configuration::S0,
 * Configuration::S1, and Configuration::SR_Clock, allow to shift the
 * register to the next position so that the in Pixel bits get a different
 * value without sending another WrFE Command.
 *
 * This class contains a vector of pixels (DoubleColumn::m_pixels)
 * that hold the 13-bit configuration of the Pixel. Pixels are accessible
 * through DoubleColumn::GetPixel, and the 672-bit configuration for the selected
 * in Pixel bit is set with DoubleColumn::SetRegister.
 *
 * The double column values can be shifted to the right with DoubleColumn::Shift.
 *
 * @brief FEI4B DoubleColumn
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/
class DoubleColumn{

public:

  /**
   * Create a DoubleColumn with 672 Pixel objects
   **/
  DoubleColumn();

  /**
   * Delete the 672 Pixel objects
   **/
  ~DoubleColumn();

  /**
   * Set the internal 672-bit register
   * @param reg The 672 long bit value
   **/
  void SetRegister(std::vector<bool> reg);

  /**
   * Set the latch of each of the 672 Pixel objects from the internal register object
   * @param latch One of the 13 in Pixel configuration bits
   **/
  void SetLatches(uint32_t latch);

  /**
   * Set the latch of each of the 672 Pixel objects
   * @param latch One of the 13 in Pixel configuration bits
   * @param reg The 672 long bit value for each Pixel
   **/
  void SetRegister(uint32_t latch, std::vector<bool> reg);

  /**
   * Get the latch of each of the 672 Pixel objects
   * @param latch One of the 13 in Pixel configuration bits
   * @return The 672 long bit value for each Pixel
   **/
  std::vector<bool> GetRegister(uint32_t latch);

  /**
   * Get the Pixel at the given col (0,1) and row (0 to 335)
   * @param col The Pixel column in the double column (0 or 1)
   * @param row The Pixel row in the double column (0 to 335)
   **/
  Pixel * GetPixel(uint32_t col, uint32_t row);

  /**
   * Get the Pixel at the given pos in the double column (0 to 671)
   * @param pos The Pixel position in the double column (0 to 671)
   * @return The Pixel object
   **/
  Pixel * GetPixel(uint32_t pos);

  /**
   * Set the field of each of the 672 Pixel objects
   * @param index One of the Pixel configuration registers
   * @param values The 672 values for each Pixel
   **/
  void SetPixels(uint32_t index, std::vector<uint32_t> values);

  /**
   * Set the field of each of the 672 Pixel objects
   * @param name One name of the Pixel configuration registers
   * @param values The 672 values for each Pixel
   **/
  void SetPixels(std::string name, std::vector<uint32_t> values);

  /**
   * Shift the double column to the right.
   * The second value is the first one, the third the second ... and the first the last value.
   * @param latch One of the 13 Pixel configuration bits
   */
  void Shift(uint32_t latch);

private:

  std::vector<Pixel*> m_pixels;
  std::vector<bool> m_register;
};

}

#endif
