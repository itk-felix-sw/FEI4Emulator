#ifndef FEI4B_TOTSCAN_H
#define FEI4B_TOTSCAN_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * @brief FEI4B TotScan
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class TotScan: public Handler{

public:

  /**
   * Create the scan
   */
  TotScan();

  /**
   * Delete the scan
   */
  virtual ~TotScan();

  /**
   */
  virtual void Run();

private:


};

}

#endif
