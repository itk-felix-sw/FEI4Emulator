#ifndef FEI4B_THRESHOLDSCAN_H
#define FEI4B_THRESHOLDSCAN_H

#include "FEI4Emulator/TriggerHandler.h"

namespace fei4b{

/**
 *
 *
 * @brief FEI4B ThresholdScan
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class TriggerThresholdScan: public TriggerHandler{

public:

  /**
   * Create the scan
   */
  TriggerThresholdScan();

  /**
   * Delete the Handler
   */
  virtual ~TriggerThresholdScan();

  /**
   */
  virtual void Run();

private:


};

}

#endif
