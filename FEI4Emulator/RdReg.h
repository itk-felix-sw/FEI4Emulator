#ifndef FEI4B_RDREG_H
#define FEI4B_RDREG_H

#include "FEI4Emulator/Command.h"

namespace fei4b{

/**
 * This is a read register slow Command.
 * It is composed of a 5-bit identifier 0b10110,
 * that is NOT tolerant to 1-bit flips, followed by a
 * 4-bit field identifier 0b1000 common to all slow commands.
 * It is followed by a 4-bit field to identify the read register
 * 0b0001, and a 4-bit chip ID, and a 6-bit address field. 
 *
 * The bit content is the following:
 *
 * | RBit  |  0 |  4 |  5 |  8 |  9 | 12 | 13 | 16 | 17 | 20 |
 * | ---   | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Field |  1     ||  2     ||  3     ||  4     ||  5     ||
 * | Desc  |0b10110 || 0b0100 || 0b0001 || ChipID ||Address ||
 * | Size  |  5     ||  4     ||  4     ||  4     ||  6     ||
 *
 *
 * @brief FEI4B read register command
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class RdReg: public Command{
  
 public:

  /** 
   * Default constructor
   **/
  RdReg();

  /**
   * Create a read register Command with values
   * @param chipid Chip ID
   * @param address The address to read
   **/
  RdReg(uint32_t chipid, uint32_t address);

  /** 
   * Create a new object from another one
   * @param copy Pointer to copy 
   **/
  RdReg(RdReg *copy);
  
  /**
   * Destructor
   **/
  ~RdReg();

  /**
   * Create a new object from this one
   * @return copy Pointer to copy of this one 
   **/
  RdReg * Clone();

  /**
   * Return a human readable representation
   * @return The human readable representation
   **/
  std::string ToString();
  
  /**
   * Extract the contents from the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the new Position in the byte array
   **/
  Position UnPack(uint8_t * bytes, Position pos, uint32_t maxlen);
  
  /**
   * Place the contents in the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @return the new Position in the byte array
   **/
  Position Pack(uint8_t * bytes, Position pos);
  
  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the Chip ID
   * @param chipid The 4-bit Chip ID
   **/
  void SetChipID(uint32_t chipid);

  /**
   * Get the Chip ID
   * @return The 4-bit Chip ID
   **/
  uint32_t GetChipID();

  /**
   * Set the register address
   * @param address The 6-bit register address
   **/
  void SetAddress(uint32_t address);

  /**
   * Get the register address
   * @return The 6-bit register address
   **/
  uint32_t GetAddress();

 protected:

  uint32_t m_field1;
  uint32_t m_field2;
  uint32_t m_field3;
  uint32_t m_field4;
  uint32_t m_field5;

};

}
#endif 
