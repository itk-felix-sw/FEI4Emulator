#ifndef FEI4B_POSITION_H
#define FEI4B_POSITION_H

#include <cstdint>
#include <sstream>


namespace fei4b{

/**
 * Position is a class that keeps track of the current byte, and bit within the byte,
 * counting from the MSB to the LSB. Overflows of the bit count for values >7
 * are taken into account. It is also possible to return the position
 * as the total number of bits.
 *
 * @verbatim

   Position pos1; //initialize to (0,0)
   Position pos2(2,0); //initialize to (2,0)
   Position pos3(0,9); //initialize to (1,2)

   pos1++; //increment the position by one bit
   pos2+=pos3; //increment the position by 9 bits

   cout << pos1 << endl; //print "(0,1)"

   if(pos2<pos1){...} //Compare if (3,2) is smaller than (0,1)
   if(pos2==pos3){...} //Compare if (3,2) is equal to (1,2)

   @endverbatim
 *
 *
 * @brief This is a tool for positions
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class Position{
    
  public:

   /**
    * Default constructor. Initialize the position to (0,0)
    **/
	Position();

   /**
    * Create a new Position at given byte and bit.
    * @param byte The given byte.
    * @param bit The given bit.
    **/
    Position(uint32_t byte, uint32_t bit);

    /**
     * Default destructor
     **/
    ~Position();

    /**
     * Get the current byte
     * @return The current byte
     **/
    uint32_t GetByte();

    /**
     * Set the current byte
     * @param byte the new byte value
     **/
    void SetByte(uint32_t byte);

    /**
     * Get the position as the total number of bits.
     * @return The total bit count.
     **/
    uint32_t GetTotalBit();

    /**
     * Get the current bit.
     * @return The current bit.
     **/
    uint32_t GetBit();

    /**
     * Set the current bit.
     * @param bit The new bit value.
     **/
    void SetBit(uint32_t bit);

    /**
     * Fix the overflow in the bit number by adding the corresponding number of bytes.
     **/
    void Pack();

    /**
     * Allow Pos++;
     **/
    Position operator+(const Position &pos);

    /**
     * Allow Pos1+Pos2;
     **/
    Position operator+(uint32_t bits);

    /**
     * Allow Pos1+=Pos2;
     **/
    Position& operator+=(const Position &pos);

    /**
     * Allow Pos1+=2;
     **/
    Position& operator+=(uint32_t bits);
 
    /**
     * Allow Pos1==Pos2;
     **/
    bool operator==(Position& pos);

    /**
     * Allow Pos1!=Pos2;
     **/
    bool operator!=(Position& pos);

    /**
     * Allow Pos1<Pos2;
     **/
    bool operator<(Position& right);

    /**
     * Allow Pos1>Pos2;
     **/
    bool operator>(Position& right);


  private:

    uint32_t m_byte;
    uint32_t m_bit;

  };

  std::ostream& operator<<(std::ostream&, fei4b::Position& pos);
  
}


#endif
