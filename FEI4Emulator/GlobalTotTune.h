#ifndef FEI4B_GLOBALTOTTUNE_H
#define FEI4B_GLOBALTOTTUNE_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * GlobalTotTune tunes the global trimming of the pre-amplifier feedback (Configuration::PrmpVbpf)
 * in order to target a new mean TOT value for a given charge.
 * This is done by evaluating the mean TOT value of the chip at the target charge,
 * The initial value is 50 unless the retune flag has been set,
 * in which case it will be read from the configuration file.
 * The analysis provides the value for the next iteration.
 *
 *
 * The first part of the scan sets the trimming value, the different masks of the chip are looped over,
 * as well as the double columns, a charge is injected and triggers are sent to the chip.
 * Data is then read out of each chip.
 * The TOT distribution of the chip is accumulated.
 * In cases where the mean TOT is larger than the higher bound of the target TOT (target TOT + precision)
 * the trimming value is lowered and if the TOT/occupancy is lower than the lower bound of the target TOT
 * (target TOT - precision) the value of the trimming is raised.
 * The algorithm converges when the TOT is within the bounds of the target TOT.
 *
 *
 * @brief FEI4B GlobalTotTune
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class GlobalTotTune: public Handler{

public:

  /**
   * Create the scan
   */
  GlobalTotTune();

  /**
   * Delete the scan
   */
  virtual ~GlobalTotTune();

  /**
   * Configure the front-end threshold to the initial value or read from file.
   * Start a loop that will only finish if the new threshold is found,
   * followed by a mask loop, and a double column loop, in which the front-ends
   * are triggered and their hits collected.
   * The hits are analysed for the whole matrix, and a new threshold is proposed,
   * until the algorithm converges.
   *
   * The new trimming is proposed as bisection of high and low values of the trimming,
   * if the mean TOT is higher than the upper bound of the target TOT (target TOT + precision)
   * the trimming is reduced, and if the mean is lower than the lower bound of the target TOT
   * (target TOT - precision) the trimming is raised.
   */
  virtual void Run();

private:


};

}

#endif
