#ifndef FEI4B_RECORD_H
#define FEI4B_RECORD_H

#include <cstdint>
#include <string>

namespace fei4b{

/**
 * This class represents a generic FEI4B output Record which is the basic block of the FEI4B data.
 * The specific records extend this class are AddressRecord, ValueRecord, DataHeader, DataRecord, ServiceRecord.
 * Each Record is 24 bits long and with the exception of the DataRecord and the ServiceRecord, they
 * start with the pattern 0b11101 that is tolerant to 1-bit flips (0x1D, 0x1C, 0x1F, 0x19, 0x15, 0x0D),
 * followed by a 3-bit identifier code (0b001, 0b010, 0b100, 0b111).
 *
 * The order of the Records is always the following:
 *
 *  - Pixel data: DataHeader, ServiceRecord, (0 or many) DataRecord, ServiceRecord.
 *  - Register data: AddressRecord, ValueRecord.
 *
 * This class defines the methods to decode a byte array (Record::UnPack) and to encode it (Record::Pack).
 * Record::ToString returns a human readable string. 
 *
 * @brief Abstract FEI4B Record
 * @author Carlos.Solans@cern.ch
 * @date March 2020
 **/

  class Record{
    
  public:
    
    static const uint32_t UNKNOWN=0; 
    static const uint32_t DATA_HEADER=1;
    static const uint32_t DATA_RECORD=2;
    static const uint32_t ADDRESS_RECORD=3;
    static const uint32_t VALUE_RECORD=4;
    static const uint32_t SERVICE_RECORD=5;
    static const uint32_t EMPTY_RECORD=6;
    
    /**
     * Empty constructor
     **/
    Record();
    
    /**
     * Virtual destructor
     **/
    virtual ~Record();
    
    /**
     * Return a human readable representation of the Record
     * @return The human readable representation of the Record
     **/
    virtual std::string ToString()=0;
    
    /**
     * Extract the contents of this command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    virtual uint32_t UnPack(uint8_t * bytes, uint32_t maxlen)=0;
    
    /**
     * Set the contents of this Record to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    virtual uint32_t Pack(uint8_t * bytes)=0;
    
    /**
     * Get the type of the Record
     * @return the Record type
     **/
    virtual uint32_t GetType()=0;
    
  };
  
}

#endif 
