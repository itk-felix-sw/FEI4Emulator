#ifndef FEI4B_THRESHOLDSCAN_H
#define FEI4B_THRESHOLDSCAN_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * A ThresholdScan measures the threshold set for each pixel in the matrix.
 * This is done my injecting a gradually increasing charge into the pixels
 * following a pattern governed by masks and double column loops.
 * A charge below the threshold will not produce any hits, and a charge above threshold will always produce a hit.
 * The threshold is defined as the value at which the pixel fires 50% of the times.
 *
 * @brief FEI4B ThresholdScan
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class ThresholdScan: public Handler{

public:

  /**
   * Create the scan
   */
  ThresholdScan();

  /**
   * Delete the scan
   */
  virtual ~ThresholdScan();

  /**
   * Create histograms, disable all pixels per front-end.
   * Select an increasing charge to inject into the pixels by setting the (Configuration::Vcal) register.
   * Loop the matrix in groups of pixels and double columns.
   * Trigger few events and collect the hits per trigger and per FrontEnd.
   * Accumulate the number of hits per pixel as a function of Configuration::Vcal.
   * Fit a the error function to the distribution per pixel given by:
   *
   * @f[ y(x) = \frac{p_1}{2} \times \left(1+erf\left(\frac{x-p_2}{p_3}\right)\right) @f]
   *
   * Extract the threshold of the pixel as parameter 2, and the noise as parameter 3.
   * Make a distribution of thresholds and another one of noise values per front-end.
   */
  virtual void Run();

private:


};

}

#endif
