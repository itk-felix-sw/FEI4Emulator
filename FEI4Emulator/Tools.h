#ifndef FEI4B_TOOLS_H
#define FEI4B_TOOLS_H

#include <cstdint>

namespace fei4b{

/**
 * @brief FEI4B Tools
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class Tools{

public:

  Tools();
  ~Tools();

  /**
   * Convert a charge to vcal DAC units for the Configuration::PlsrDac Register.
   * @param charge The charge to convert (electrons).
   * @param use_smallcap If true, use the small capacitor.
   * @param use_largecap If true, use the large capacitor.
   * @param smallcap Optional value for the charge of the small capacitor (pC)
   * @param largecap Optional value for the charge of the large capacitor (pC)
   * @param vcalslope Optional value for the calibration constant from pC to electrons.
   * @return The Vcal value in DAC units
   */
  static uint32_t toVcal(double charge, bool use_smallcap=true, bool use_largecap=true,
                          float smallcap=1.9, float largecap=3.8, float vcalslope=1.5);


  /**
   * Convert a vcal DAC units for the Configuration::PlsrDac Register to charge (electrons).
   * @param vcal The Vcal value in DAC units
   * @param use_smallcap If true, use the small capacitor.
   * @param use_largecap If true, use the large capacitor.
   * @param smallcap Optional value for the charge of the small capacitor (pC)
   * @param largecap Optional value for the charge of the large capacitor (pC)
   * @param vcalslope Optional value for the calibration constant from pC to electrons.
   * @return The charge in electrons.
   */
  static double toCharge(double vcal, bool use_smallcap=true, bool use_largecap=true,
                          float smallcap=1.9, float largecap=3.8, float vcalslope=1.5);

};

}

#endif
