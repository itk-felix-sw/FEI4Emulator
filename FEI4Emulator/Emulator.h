#ifndef FEI4B_EMULATOR_H
#define FEI4B_EMULATOR_H

#include "FEI4Emulator/Decoder.h"
#include "FEI4Emulator/Encoder.h"
#include "FEI4Emulator/Configuration.h"
#include "FEI4Emulator/DoubleColumn.h"

#include <queue>
#include <vector>
#include <mutex>


namespace fei4b{

/**
 * This class emulates an FEI4B pixel detector that is identified by a chip ID
 * by making use of a Command Encoder, a Record Decoder, and a Field Configuration.
 *
 * A byte stream can be passed to the Emulator (Emulator::HandleCommand),
 * that will decode it into a list of commands, and added to a queue.
 * However, if the chip ID of the commands does not match the one for this emulator,
 * the command will be ignored. Messages with chip ID >7 will be always interpreted.
 *
 * Commands will be processed (Emulator::ProcessTrigger) from the queue starting
 * from the oldest one received.
 * If the emulator is configured in auto-trigger mode through a configuration command.
 * It will result in the generation of data events.
 *
 * @verbatim

   uint8_t * in_bytes =...
   uint32_t in_length =...

   Emulator emu(0);

   emu.HandleCommand(in_bytes,in_length);
   emu.ProcessTrigger();

   uint8_t * out_bytes = emu.GetBytes();
   uin32_t out_length = emu.GetLength();

   @endverbatim
 *
 * @brief FEI4B front-end emulator.
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

  class Emulator {
    
  public:
    
    /**
     * Create a new FEI4B Emulator
     * Allocates the Encoder and Decoder byte stream converters, and a Configuration object.
     * @param chipid The ID for this emulated front-end from 0 to 7.
     **/
    Emulator(uint32_t chipid=0);

    /**
     * Delete the Encoder, Decoder, and Configuration objects.
     */
    ~Emulator();
  
    /**
     * Enable the verbose mode
     * @param enable Enable verbose mode if true
     **/
    void SetVerbose(bool enable);

    /**
     * Get the ID for this emulated front-end.
     * @return The ID for this emulated front-end from 0 to 7.
     **/
    uint32_t GetChipID();

    /**
     * Set the ID for this emulated front-end from 0 to 7.
     * @return The ID for this emulated front-end from 0 to 7.
     **/
    void SetChipID(uint32_t chipid);

    /**
     * Set number of hits to be generated in auto-trigger mode.
     * @param nhits The number of hits.
     **/
    void SetNHits(uint32_t nhits);
    
    /**
     * Get number of hits to be generated in auto-trigger mode.
     * @return The number of hits.
     **/
    uint32_t GetNHits();

    /**
     * Get the output byte stream from the emulator.
     * @return Byte stream output from the emulator.
     **/
    uint8_t * GetBytes();

    /**
     * Get the length of the output byte stream from the emulator.
     * @return Length of the byte stream output from the emulator.
     **/
    uint32_t GetLength();

    /**
     * Process byte stream of commands in the emulator.
     * The command will be first decoded by the Encoder,
     * and then interpreted by the emulator.
     * Commands are then placed in a command queue.
     * If the chip ID of the commands does not match the one
     * for this emulator, the command will be ignored.
     * Messages with ChipID>7 will be always interpreted.
     * @param recv_data Byte stream of commands.
     * @param recv_size Size of the byte stream.
     **/
    void HandleCommand(uint8_t *recv_data, uint32_t recv_size);

    /**
     * Process commands pending queue starting from the oldest received.
     * If the emulator is configured in auto-trigger mode through a configuration command.
     * It will result in the generation of data events.
     **/
    void ProcessTrigger();
    
    
  private:
    
    bool m_verbose;  
    bool m_debug;
    uint32_t m_chipid;
    uint32_t m_mode;
    uint32_t m_l1id;
    uint32_t m_bcid;
    uint32_t m_cal_hi;
    uint32_t m_cal_lo;
    uint32_t m_nhits;
    
    float m_smallCap;
    float m_largeCap;
    float m_vcalOffset;
    float m_vcalSlope;

    /**
     *
     * Q[eV] = Charge [C] * Voltage [V] / ElectronCharge [eV]
     *       = (large_cap+small_cap)[fC] * (vcalSlope*vcal)[mV]/ (1.6E-19) [C/e]
     *       = 10 * (large_cap+small_cap) * (vcalSlope*vcal) / 1.6 [eV];
     **/
    double toCharge(double vcal, bool smallcap=true, bool largecap=true);


    /**
     *
     * Voltage [mV] = Q [eV] * ElectronCharge [eV] / Charge [C]
     *              = Q [eV] * (1.6E-19) [C/e] / (large_cap+small_cap)[fC]
     *              = charge * (1.6) / 1E4 * (large_cap+small_cap) [V]
     *              = charge * (1.6) / 10 * (large_cap+small_cap) [mV];
     *
     **/
    uint32_t toVcal(double charge, bool smallcap=true, bool largecap=true);


    std::mutex m_mutex;
    std::deque<uint32_t> m_addrs;
    std::deque<Command*> m_cmds;
    Decoder *m_decoder;
    Encoder *m_encoder;
    Configuration *m_config;
    std::vector<DoubleColumn*> m_dcs;
    std::vector<bool> m_shiftreg;
    
    bool IsMsgForMe(uint32_t chipid);

    /**
     *
     * Mode 1 (every 40)
     * Address 0: 1
     * Address 1: 2
     * ...
     * Address 39: 40
     *
     * Mode 1 (every 4)
     * Address 0: 1 5  9 13  17  21  25  29  33  37
     * Address 1: 2 6 10  14  18  22  26  30  34  38
     * Address 2: 3 7 11  15  19  23  27  31  35  39
     * Address 3: 4 8 12  16  20  24  28  32  36  40
     *
     * Mode 2 (every 8)
     * Address 0: 1  9  17  25  33
     * Address 0: 2 10  18  26  34
     * Address 0: 3 11  19  27  35
     * Address 0: 4 12  20  28  36
     * Address 0: 5 13  21  29  37
     * Address 0: 6 14  22  30  38
     * Address 0: 7 15  23  31  39
     * Address 0: 8 16  24  32  40
     *
     * Mode 3 (every one)
     * Address 0: 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
     *
     *
     */
    std::vector<uint32_t> GetActiveDoubleColumns(uint32_t mode=0xFF, uint32_t addr=0xFF);

};

}
#endif
