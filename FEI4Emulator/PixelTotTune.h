#ifndef FEI4B_PIXELTOTTUNE_H
#define FEI4B_PIXELTOTTUNE_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * PixelTotTune tunes the in-pixel trimming of the pre-amplifier feedback (Pixel::FDAC) to reduce the TOT tuning dispersion across the chip.
 * This is done by evaluating the mean TOT value per pixel at the target charge as a function of the 4-bit in-pixel trimming.
 * The initial value for every pixel is 8 unless the retune flag has been set,
 * in which case it will be read from the configuration file.
 * The analysis provides the value for the next iteration.
 *
 *
 * The first part of the scan sets the trimming value for each pixel, the different masks of the chip are looped over,
 * as well as the double columns, a charge is injected and triggers are sent to the chip.
 * Data is then read out of each chip.
 * The occupancy per pixel is accumulated, as well as the TOT per pixel, and the ratio TOT/occupancy is evaluated per pixel.
 * In cases where the TOT/occupancy of the pixel is larger than the higher bound of the target TOT (target TOT + precision)
 * the trimming value is lowered and if the TOT/occupancy of the pixel is lower than the lower bound of the target TOT
 * (target TOT - precision) the value of the trimming value is raised.
 * The algorithm converges when the TOT/occupancy of 98% of the unmasked pixels is within the target TOT bounds.
 *
 *
 *
 * @brief FEI4B PixelTotTune
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class PixelTotTune: public Handler{

public:

  /**
   * Create the scan
   */
  PixelTotTune();

  /**
   * Delete the scan
   */
  virtual ~PixelTotTune();

  /**
   * Configure the front-end in-pixel trimming of the preamplifier feedback (Pixel::FDAC) to the initial value or read from file.
   * Start a loop that will only finish when a new value is found for all pixels, that is followed by a mask loop,
   * a double column loop, and a trigger and read-out loop.
   * The hits are analysed for the whole matrix, and a new trimming value is proposed per pixel
   * defined as the bisection between the upper and lower previous values, until the algorithm converges.
   */
  virtual void Run();

private:


};

}

#endif
