#ifndef FEI4BDECODER_H
#define FEI4BDECODER_H

#include "FEI4Emulator/Record.h"
#include "FEI4Emulator/DataRecord.h"
#include "FEI4Emulator/DataHeader.h"
#include "FEI4Emulator/AddressRecord.h"
#include "FEI4Emulator/ValueRecord.h"
#include "FEI4Emulator/ServiceRecord.h"
#include "FEI4Emulator/EmptyRecord.h"

#include <cstdint>
#include <vector>

namespace fei4b{

/**
 * This class is designed to decode the FEI4B byte stream into specific Record objects:
 * DataHeader, DataRecord, AddressRecord, ValueRecord, and ServiceRecord.
 * It is also possible to encode a set of FEI4B records into a byte stream.
 *
 * The byte stream is accessible through Decoder::GetBytes.
 * Similarly, a byte stream can be decoded by the Decoder::SetBytes.
 * The Record objects are available from Decoder::GetRecords.
 *
 * @verbatim
 
   Decoder decoder;
   decoder.AddRecord(new DataHeader());
   decoder.AddRecord(new DataRecord());
   decoder.AddRecord(new DataRecord());
 
   //Encode into bytes
   decoder.Encode();
   uint8_t * bytes = decoder.GetBytes();
   uin32_t length = decoder.GetLength();

   //Decode into records
   decoder.SetBytes(bytes, length);
   decoder.Decode();
   vector<Record*> records = decoder.GetRecords();

   DataHeader* dh = dynamic_cast<DataHeader*>(records[0]);
   dh->GetL1ID();

   @endverbatim
 *
 * @brief FEI4B output data Record Decoder
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/


class Decoder{
    
 public:

  /**
   * Initialize the decoder
   **/
  Decoder();
  
  /**
   * Delete the frames in memory
   **/
  ~Decoder();
  
  /**
   * Add bytes to the already existing byte array
   * @param bytes byte array
   * @param pos starting index of the byte array
   * @param len number of bytes to add
   **/
  void AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len);

  /**
   * Replace the bytes of the byte array.
   * @param bytes byte array
   * @param len number of bytes to add
   **/
  void SetBytes(uint8_t *bytes, uint32_t len);
  
  /**
   * Add a record to the end of the frame list
   * @param record fei4b specific Record
   **/
  void AddRecord(Record *record);
    
  /**
   * Get the list of frames
   * @return vector of RD53AFrames pointers
   **/
  std::vector<Record*> & GetRecords();

  /**
   * Get a string representation of the bytes. 
   * @return a string in hexadecimal
   **/
  std::string GetByteString();

  /**
   * Get the byte array pointer. 
   * This pointer cannot be deleted by the user.
   * @return the byte array as a pointer
   **/
  uint8_t * GetBytes();
  
  /**
   * @return the size of the byte array
   **/
  uint32_t GetLength();

  /**
   * Encode the frames into a byte array
   **/
  void Encode();

  /**
   * Decode the byte array into frames
   **/
  void Decode();
  
  /**
   * Clear the byte array
   **/
  void ClearBytes();
  
  /**
   * Clear the frame list by deleting each
   * object in the list.
   **/
  void ClearRecords();
  
  /**
   * Clear the byte array and the command list
   **/
  void Clear();

 private:
  
  std::vector<Record*> m_records;
  std::vector<uint8_t> m_bytes;
  uint32_t m_length;
  
  DataHeader * m_DH;
  DataRecord * m_DR;
  AddressRecord * m_AR;
  ValueRecord * m_VR;
  ServiceRecord * m_SR;
  EmptyRecord * m_ER;
};

}
#endif
