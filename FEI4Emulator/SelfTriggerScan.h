#ifndef FEI4B_SELFTRIGGERSCAN_H
#define FEI4B_SELFTRIGGERSCAN_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * This is a self trigger scan. Description needs to be updated.
 *
 * @brief FEI4B SelfTriggerScan
 * @author Carlos.Solans@cern.ch
 * @date August 2020
 */

class SelfTriggerScan: public Handler{

public:

  /**
   * Create the scan
   */
  SelfTriggerScan();

  /**
   * Delete the Handler
   */
  virtual ~SelfTriggerScan();

  /**
   */
  virtual void Run();

private:

};

}

#endif
