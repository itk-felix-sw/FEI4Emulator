#ifndef FEI4B_REGISTERSCAN_H
#define FEI4B_REGISTERCAN_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * A RegisterScan is a simple read-back test for the global registers of the FEI4B.
 * 36 RdReg commands are sent to the FrontEnd to request the reading of each Register value.
 * The Data is automatically processed by the FrontEnd class, and displayed to the user.
 * The test is successful if the same value that is sent to the FrontEnd is read back.
 *
 * The RegisterScan does not produce any histogram in the output file.
 *
 * @brief FEI4B RegisterScan
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class RegisterScan: public Handler{

public:

  /**
   * Create the scan
   */
  RegisterScan();

  /**
   * Delete the scan
   */
  virtual ~RegisterScan();

  /**
   * Send 36 RdReg commands to each FrontEnd, wait 2 seconds, and display the updated Register values.
   */
  virtual void Run();

private:

};

}

#endif
