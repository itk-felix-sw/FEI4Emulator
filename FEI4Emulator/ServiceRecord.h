#ifndef FEI4B_SERVICERECORD_H
#define FEI4B_SERVICERECORD_H

#include "FEI4Emulator/Record.h"
#include <vector>

namespace fei4b{

/**
 * The ServiceRecord is a message containing an error or extra information.
 * It is composed of an identifier 0b11101 that is tolerant to 1-bit flips,
 * followed by a unique code 0b111, 
 * a 6-bit code (ServiceRecord::SetCode, ServiceRecord::GetCode), and a 
 * 10-bit counter (ServiceRecord::SetCounter, ServiceRecord::GetCounter).
 *
 * The bit content is the following:
 *
 * | Bit  | 23 | 19 | 18 | 16 | 15 | 10 |  9 |  8 |  7 |  0 |
 * | ---  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Rel  |  7 |  0 |  7 |  0 |  7 |  2 |  1 |  0 |  7 |  0 |
 * | Byte |  0             ||||  1             ||||  2     ||
 * | Desc |0b11101 || 0b111  || Code   || Counter        ||||
 * | Size |  5     || 3      || 6      || 10             ||||
 * 
 * Possible patterns for the header identifier are:
 * 
 * | ID (Bin) | ID (Hex) | Byte (Bin) | Byte (Hex) | 
 * | -------- | -------- | ---------- | ---------- |
 * | 0b11101  | 0x1D     | 0b11101111 | 0xEF       |
 * | 0b11100  | 0x1C     | 0b11100111 | 0xE7       |
 * | 0b11111  | 0x1F     | 0b11111111 | 0xFF       |
 * | 0b11001  | 0x19     | 0b11001111 | 0xCF       |
 * | 0b10101  | 0x15     | 0b10101111 | 0xAF       |
 * | 0b01101  | 0x0D     | 0b01101111 | 0x6F (not implemented) |
 *
 * The meaning of the codes is the following (ServiceRecord::m_code2meaning):
 *
 * | Code  | Meaning              
 * | ----  | -------
 * |    0  | BCID counter error
 * |    1  | Hamming code error in word 0
 * |    2  | Hamming code error in word 1
 * |    3  | Hamming code error in word 2
 * |    4  | L1_in counter error
 * |    5  | L1 request counter error
 * |    6  | L1 register error
 * |    7  | L1 Trigger ID error
 * |    8  | readout processor error
 * |    9  | Fifo_Full flag pulsed
 * |   10  | HitOr bus pulsed
 * | 11-13 | not used
 * |   14  | 3 MSBs of bunch counter and 7 MSBs of L1A counter
 * |   15  | Skipped trigger counter
 * |   16  | Truncated event flag and counter
 * | 17-20 | not used
 * |   21  | Reset bar RA2b pulsed
 * |   22  | PLL generated clock phase faster than reference
 * |   23  | Reference clock phase faster than PLL
 * |   24  | Triple redundant mismatch
 * |   25  | Write register data error
 * |   26  | Address error
 * |   27  | Other command decoder error
 * |   28  | Bit flip detected in command decoder input stream
 * |   29  | SEU upset detected in command decoder (triple redundant mismatch)
 * |   30  | Data bus address error
 * |   31  | Triple redundant mismatch
 *
 * In the case the code is 14 the bit content is the following:
 *
 * | Bit  | 23 | 19 | 18 | 16 | 15 | 10 |  9 |  8 |  7 |  3 |    2 |    0 |
 * | ---  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | ---- | ---- |
 * | Rel  |  7 |  0 |  7 |  0 |  7 |  2 |  1 |  0 |  7 |  3 |    2 |    0 |
 * | Byte |  0             ||||  1             ||||  2                 ||||
 * | Desc |0b11101 || 0b111  || 14     || L1ID(11:5)     |||| BCID(12:10)||
 * | Size |  5     || 3      || 6      || 7              ||||  3         ||
 *
 *
 * @brief FEI4B service record
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class ServiceRecord: public Record{
  
 public:

  /** 
   * Default constructor
   **/
  ServiceRecord();

  /** 
   * Create a new object from another one
   * @param copy Pointer to ServiceRecord to copy 
   **/
  ServiceRecord(ServiceRecord *copy);

  /**
   * Create a service record with values
   * @param code the code value
   * @param counter the counter value
   **/
  ServiceRecord(uint32_t code, uint32_t counter);

  /**
   * Create a service record of type 14 with l1id and bcid values.
   * @param l1id the new L1ID
   * @param bcid the new BCID
   * @param shifted Extract the bits from the l1id and bcid
   **/
  static ServiceRecord * DataCounter(uint32_t l1id, uint32_t bcid, bool shifted=0);

  /**
   * Destructor
   **/
  ~ServiceRecord();

  /**
   * Return a human readable representation of the register
   * @return The human readable representation of the register
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the code of the service record
   * @param code The code of the service record
   **/
  void SetCode(uint32_t code);

  /**
   * Get the code of the service record
   * @return The code of the service record
   **/
  uint32_t GetCode();

  /**
   * Set the counter
   * @param counter counter
   **/
  void SetCounter(uint32_t counter);

  /**
   * Get the counter
   * @return the counter
   **/
  uint32_t GetCounter();

  /**
   * Set the L1ID bits of the counter
   * @param l1id The L1ID bits of the counter
   * @param shifted Extract the bits from the bcid
   **/
  void SetL1IDbits(uint32_t l1id, bool shifted=0);

  /**
   * Get the L1ID bits of the counter
   * @return l1id The L1ID bits of the counter
   * @param shift Put the bits in their place
   **/
  uint32_t GetL1IDbits(bool shift=0);

  /**
   * Set the BCID bits of the counter
   * @param bcid The BCID bits of the counter
   * @param shifted Extract the bits from the bcid
   **/
  void SetBCIDbits(uint32_t bcid, bool shifted=0);

  /**
   * Get the BCID bits of the counter
   * @return bcid The BCID bits of the counter
   * @param shift Put the bits in their place
   **/
  uint32_t GetBCIDbits(bool shift=0);
    
 protected:

  uint32_t m_id;
  uint32_t m_code;
  uint32_t m_counter;

  /** Lookup table from error code to meaning **/
  static std::vector<std::string> m_code2meaning;


};

}
#endif 
