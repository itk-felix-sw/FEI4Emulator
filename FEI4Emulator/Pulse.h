#ifndef FEI4B_PULSE_H
#define FEI4B_PULSE_H

#include "FEI4Emulator/Command.h"

namespace fei4b{

/**
 * This is a pulse slow Command.
 * It is composed of a 5-bit identifier 0b10110,
 * that is NOT tolerant to 1-bit flips, followed by a
 * 4-bit field identifier 0b1000 common to all slow commands,
 * followed by a 4-bit field 0b1001 to identify the pulse,
 * a 4-bit chip ID, a 6-bit width field. 
 *
 * The bit content is the following:
 *
 * | RBit  |  0 |  4 |  5 |  8 |  9 | 12 | 13 | 16 | 17 | 20 |
 * | ---   | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Field |  1     ||  2     ||  3     ||  4     ||  5     ||
 * | Desc  |0b10110 || 0b0100 || 0b0001 || ChipID || Width  ||
 * | Size  |  5     ||  4     ||  4     ||  4     ||  6     ||
 *
 *
 * @brief FEI4B pulse command
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class Pulse: public Command{
  
 public:

  /** 
   * Default constructor
   **/
  Pulse();
  
  /**
   * Create pulse with values
   * @param chipid Chip ID
   * @param width Width of the pulse
   **/
  Pulse(uint32_t chipid, uint32_t width);

  /** 
   * Create a new object from another one
   * @param copy Pointer to copy 
   **/
  Pulse(Pulse *copy);
  
  /**
   * Destructor
   **/
  ~Pulse();

  /**
   * Create a new object from this one
   * @return copy Pointer to copy of this one 
   **/
  Pulse * Clone();

  /**
   * Return a human readable representation
   * @return The human readable representation
   **/
  std::string ToString();
  
  /**
   * Extract the contents from the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the new Position in the byte array
   **/
  Position UnPack(uint8_t * bytes, Position pos, uint32_t maxlen);
  
  /**
   * Place the contents in the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @return the new Position in the byte array
   **/
  Position Pack(uint8_t * bytes, Position pos);
  
  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the Chip ID
   * @param chipid The 4-bit Chip ID
   **/
  void SetChipID(uint32_t chipid);

  /**
   * Get the Chip ID
   * @return The 4-bit Chip ID
   **/
  uint32_t GetChipID();

  /**
   * Set the 6-bit pulse width
   * @param width The 6-bit pulse width
   **/
  void SetWidth(uint32_t width);

  /**
   * Get the 6-bit pulse width
   * @return The 6-bit pulse width
   **/
  uint32_t GetWidth();

 protected:

  uint32_t m_field1;
  uint32_t m_field2;
  uint32_t m_field3;
  uint32_t m_field4;
  uint32_t m_field5;

};

}
#endif 
