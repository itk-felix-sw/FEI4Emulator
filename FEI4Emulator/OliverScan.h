#ifndef FEI4B_OLIVERSCAN_H
#define FEI4B_OLIVERSCAN_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * A DigitalScan injects a signal in the digital circuit of the pixel, and checks for the corresponding hit in the data.
 * Because the output data bandwidth is limited, the injection is done in a few pixels at a time
 * and in groups of double column.
 * The analysis of the digital scan checks that every single pixel has the required number of hits.
 * If there is a discrepancy in the number of hits in a given set of pixels,
 * it is possible to generate a mask to ignore those pixels in subsequent scans.
 *
 * The digital injection pulse is ORed with the comparator output.
 * Therefore, the analog state must be below threshold, or the discriminator must be off (bias current set to zero)
 * for digital injection to work.
 * For example, setting a very low discriminator threshold with the comparator active will prevent digital injection from working,
 * because the discriminator will be always high. The width of the digital injection pulse will simulate the time over threshold response.
 * The digital injection pulse can be routed in parallel to any set of pixels by
 *  - Enabling digital injection by setting the DIGHITIN_SEL configuration bit
 *  - Selecting the column pairs for injection using the Colpr_Mode and Colpr_Addr registers
 *  - loading a 1 into the shift register for all desired pixels (zero for all not desired pixels) of the enabled columns
 *
 * @brief FEI4B DigitalScan
 * @author Carlos.Solans@cern.ch
 * @date June 2020
 */

class OliverScan: public Handler{

public:

  /**
   * Create the scan
   */
  OliverScan();

  /**
   * Delete the Handler
   */
  virtual ~OliverScan();

  /**
   * Create histograms, disable all pixels per front-end.
   * Loop the matrix in groups of pixels and double columns.
   * Trigger few events and collect the hits per trigger and per FrontEnd.
   * An occupancy histogram is filled per front-end, that is required
   * to have the same amount of entries or less than the number of triggers.
   * A pixel will be masked if it has more than the expected number of entries.
   */
  virtual void Run();

private:

};

}

#endif
