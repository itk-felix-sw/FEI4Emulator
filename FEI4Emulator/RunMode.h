#ifndef FEI4B_RUNMODE_H
#define FEI4B_RUNMODE_H

#include "FEI4Emulator/Command.h"

namespace fei4b{

/**
 * Slow command to change the mode of the chip between
 * config (RunMode::MODE_CONF), and run (RunMode::MODE_RUN).
 * It is composed of a 5-bit identifier 0b10110,
 * that is NOT tolerant to 1-bit flips, followed by a
 * 4-bit field identifier 0b1000 common to all slow commands,
 * followed by a 4-bit field 0b1010 to identify the command,
 * and a 4-bit chip ID, followed by the 6-bit mode.
 *
 * The bit content is the following:
 *
 * | RBit  |  0 |  4 |  5 |  8 |  9 | 12 | 13 | 16 | 17 | 20 |
 * | ---   | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Field |  1     ||  2     ||  3     ||  4     ||  5     ||
 * | Desc  |0b10110 || 0b0100 || 0b0001 || ChipID || Mode   ||
 * | Size  |  5     ||  4     ||  4     ||  4     ||  6     ||
 *
 *
 * @brief FEI4B run mode command
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class RunMode: public Command{
  
 public:
  
  /** 
   * Value to put the chip into run mode
   **/
  static const uint32_t RUNMODE  = 0x38;
  static const uint32_t RUN  = 0x38;
  
  /** 
   * Value to put the chip into config mode
   **/
  static const uint32_t CONFMODE = 0x07;
  static const uint32_t CONFIG = 0x07;
  
  /** 
   * Default constructor
   **/
  RunMode();

  /**
   * Create run mode with values
   * @param chipid Chip ID
   * @param mode Chip mode
   **/
  RunMode(uint32_t chipid, uint32_t mode);

  /** 
   * Create a new object from another one
   * @param copy Pointer to copy 
   **/
  RunMode(RunMode *copy);
  
  /**
   * Destructor
   **/
  ~RunMode();

  /**
   * Create a new object from this one
   * @return copy Pointer to copy of this one 
   **/
  RunMode * Clone();

  /**
   * Return a human readable representation
   * @return The human readable representation
   **/
  std::string ToString();
  
  /**
   * Extract the contents from the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the new Position in the byte array
   **/
  Position UnPack(uint8_t * bytes, Position pos, uint32_t maxlen);
  
  /**
   * Place the contents in the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @return the new Position in the byte array
   **/
  Position Pack(uint8_t * bytes, Position pos);
  
  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the Chip ID
   * @param chipid The 4-bit Chip ID
   **/
  void SetChipID(uint32_t chipid);

  /**
   * Get the Chip ID
   * @return The 4-bit Chip ID
   **/
  uint32_t GetChipID();

  /**
   * Set the register address
   * @param address The 6-bit register address
   **/
  void SetAddress(uint32_t address);

  /**
   * Get the register address
   * @return The 6-bit register address
   **/
  uint32_t GetAddress();

  /**
   * Set the run mode
   * @param value The run mode (RunMode::RUNMODE, RunMode::CONFMODE)
   **/
  void SetMode(uint32_t value);

  /**
   * Get the run mode
   * @return The run mode (RunMode::RUNMODE, RunMode::CONFMODE)
   **/
  uint32_t GetMode();

 protected:

  uint32_t m_field1;
  uint32_t m_field2;
  uint32_t m_field3;
  uint32_t m_field4;
  uint32_t m_field5;

};

}
#endif 
