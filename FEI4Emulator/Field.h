#ifndef FEI4B_FIELD_H
#define FEI4B_FIELD_H

#include "FEI4Emulator/Register.h"

#include <cstdint>
#include <vector>

namespace fei4b{

/**
 * The global Configuration of the FEI4B is accessible
 * as 16-bit registers through the WrReg and RdReg Command.
 * Each of the 16-bit registers can contain one or more Field objects.
 * That represent a particular setting of the FEI4B.
 *
 * @brief FEI4B Configuration Field
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class Field{
	
public:
    
  /**
   * Construct a Field given a pointer to the Configuration register.
   * @param data pointer to the Configuration register
   * @param start First bit of the data
   * @param len Length of the Field in bits
   * @param defval Default value for the Field
   * @param reversed indicate that the values are stored in big endian mode, MSB in the LSB position
   **/
  Field(Register * data, uint32_t start, uint32_t len, uint32_t defval=0, bool reversed=false);

  /**
   * Empty Destructor
   **/
  ~Field();

  /**
   * Set the value of the Field. Extra bits will be truncated.
   * @param value New value of the Field
   **/
  void SetValue(uint32_t value);

  /**
   * Get the value of the Field
   * @return The value of the Field
   **/
  uint32_t GetValue();

private:
  
  Register * m_data;
  uint32_t m_start;
  uint32_t m_len;
  uint32_t m_defval;
  uint32_t m_mask;
  uint32_t m_smask;
  bool m_reversed;

  static std::vector<uint32_t> m_masks;
    
  uint32_t Reverse(uint32_t value, uint32_t sz);

};

}

#endif
