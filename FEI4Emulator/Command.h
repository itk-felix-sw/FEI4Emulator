#ifndef FEI4B_COMMAND_H
#define FEI4B_COMMAND_H

#include <cstdint>
#include <string>
#include <vector>
#include "FEI4Emulator/Position.h"

namespace fei4b{

/**
 * This class represents a generic FEI4B input Command.
 * The specific commands extend this class. 
 *
 * This class defines the methods to decode a byte array
 * (Command::Pack) and to encode it (Command::Unpack).
 * Command::ToString returns a human readable string. 
 *
 * @brief Abstract FEI4B Command
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/
  
  class Command{
    
  public:
    
    static const uint32_t UNKNOWN=0; 
    static const uint32_t LVL1=1;
    static const uint32_t BCR=2;
    static const uint32_t ECR=3;
    static const uint32_t CAL=4;
    static const uint32_t RDREG=5;
    static const uint32_t WRREG=6;
    static const uint32_t WRFE=7;
    static const uint32_t RESET=8;
    static const uint32_t PULSE=9;
    static const uint32_t RUNMODE=10;
    
    /**
     * Empty constructor
     **/
    Command();
    
    /**
     * Virtual destructor
     **/
    virtual ~Command();
    
    /**
     * Return a clone of the current Command
     * @return A clone of the current Command
     **/
    virtual Command * Clone()=0;

    /**
     * Return a human readable representation
     * @return The human readable representation
     **/
    virtual std::string ToString()=0;
    
    /**
     * Extract the contents from the byte array
     * @param bytes the byte array
     * @param pos the Position in the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the new Position in the byte array
     **/
    virtual Position UnPack(uint8_t * bytes, Position pos, uint32_t maxlen)=0;
    
    /**
     * Place the contents in the byte array
     * @param bytes the byte array
     * @param pos the Position in the byte array
     * @return the new Position in the byte array
     **/
    virtual Position Pack(uint8_t * bytes, Position pos)=0;
    
    /**
     * Get the type of the Record
     * @return the Record type
     **/
    virtual uint32_t GetType()=0;
    
  protected:

    bool m_debug;
    
    /**
     * Write a predefined number of bits on a byte array
     * The data to write is padded to the right.
     * And has to be written aligned to the left in the byte.
     **/ 
    Position ToByteArray(uint8_t * bytes, uint32_t data, uint32_t size, Position startpos);

    /**
     * Write a predefined number of bits on a byte array
     * The data to write is padded to the right.
     * And has to be written aligned to the left in the byte.
     **/ 
    Position FromByteArray(uint8_t * bytes, uint32_t *data, uint32_t size, Position startpos);

    /**
     * Masks used for byte writing
     **/
    static std::vector<std::vector<uint32_t> > m_masks;

    /**
     * actual size of the word being written
     **/
    static std::vector<std::vector<uint32_t> > m_sizes;

    /**
     * Leftover bits from curren writing
     **/
    static std::vector<std::vector<uint32_t> > m_rests;

    /**
     * Leftover bits from curren writing
     **/
    static std::vector<std::vector<uint32_t> > m_shifts;

  };
  
}

#endif 
