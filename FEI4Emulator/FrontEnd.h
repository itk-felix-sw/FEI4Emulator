#ifndef FEI4B_FRONTEND_H
#define FEI4B_FRONTEND_H

#include "FEI4Emulator/Decoder.h"
#include "FEI4Emulator/Encoder.h"
#include "FEI4Emulator/Configuration.h"
#include "FEI4Emulator/DoubleColumn.h"
#include "FEI4Emulator/Hit.h"

#include <queue>
#include <vector>
#include <mutex>


namespace fei4b{

/**
 * This class controls an FEI4B pixel detector that is identified by
 * a chip ID (FrontEnd::GetChipID, FrontEnd::SetChipID),
 * and has a name (FrontEnd::GetName, FrontEnd::SetName),
 * by making use of a Command Encoder, a Record Decoder, a Field Configuration, and a DoubleColumn array.
 *
 * The Encoder encodes the operations on the FrontEnd like
 * loading the global configuration (FrontEnd::LoadGlobalConfig),
 * loading the per pixel configuration (FrontEnd::LoadPixelConfig),
 * setting different mask stages for calibration (FrontEnd::SetMask),
 * and triggering the detector (FrontEnd::Trigger),
 * into the corresponding Command objects.
 * These are converted into a byte stream on request (FrontEnd::ProcessCommands),
 * and available for the communication layer (FrontEnd::GetBytes, FrontEnd::GetLength).
 *
 * The Decoder decodes the data from the communication layer (FrontEnd::HandleData),
 * and assigns the read-back registers (ValueRecord, AddressRecord) to the
 * global Configuration, and converts the rest of the data (DataHeader, ServiceHeader, DataRecord)
 * into Hit objects that are stored in a FIFO.
 * The status of the FIFO can be checked (FrontEnd::HasHits), and polled (FrontEnd::GetHit, FrontEnd::NextHit).
 *
 * The pixel matrix is represented by a DoubleColumn array. Each DoubleColumn contains a shift register
 * that can be accessed through WrFE commands, and 672 Pixel objects that contain the in-pixel configuration.
 *
 * The global Configuration can be accessed (FrontEnd::GetConfig) to address the global Register objects,
 * or the Field objects in which the Register objects are divided.
 * It is required to add the commands to the encoder (FrontEnd::ConfigGlobal) after altering the Configuration.
 *
 * Similarly, the pixel configuration can be accessed to change the pixel threshold (FrontEnd::SetPixelThreshold),
 * the pre-amplifier feedback tuning (FrontEnd::SetPixelPreampFeedback), and enable the large and small capacitors
 * (FrontEnd::EnableLCap, FrontEnd::EnableSCap).
 * It is also required to add the commands to the encoder after altering the pixel configuration,
 * which can be done for the whole Pixel latches at a time (FrontEnd::ConfigPixels),
 * by double column (FrontEnd::ConfigDoubleColumn),
 * only for the pixel threshold (FrontEnd::ConfigPixelThresholds),
 * or only for the pixel pre-amplifier feedback tuning value (FrontEnd::ConfigPixelPreampFeedback).
 *
 * | Colpr_Addr | Digital [Col] | Analog [Col] |
 * | ---------- | ------------- | ------------ |
 * | 0          | 1 and 2       | 1            |
 * | 1          | 3 and 4       | 2 and 3      |
 * | 2          | 5 and 6       | 4 and 5      |
 * | ...        | ...           | ...          |
 * | 38         | 77 and 78     | 76 and 77    |
 * | 39         | 79 and 80     | 78, 79, 80   |
 * | n          | 2n+1 and 2n+2 | 2n and 2n+1  |
 *
 *
 * @verbatim

   FrontEnd fe;

   fe.LoadGlobalConfig(...);
   fe.ProcessCommands();
   fe.Trigger();

   uint8_t * out_bytes = fe.GetBytes();
   uin32_t out_length = fe.GetLength();

   fe.HandleData(in_bytes, in_length);

   while(fe.HasHits()){
     Hit * hit = fe.GetHit();
     hit.NextHit();
   }

   @endverbatim
 *
 * @brief FEI4B FrontEnd controller.
 * @author Carlos.Solans@cern.ch
 * @date June 2020
 **/

  class FrontEnd {

  public:

    /**
     * Create an Encoder, a Decoder, a Configuration, and 40 DoubleColumn objects.
     * Set the FEI4B name to FEI4B, Chip Id to 0, and active flag to true.
     **/
    FrontEnd();

    /**
     * Delete the Encoder, Decoder, Configuration, and the 40 DoubleColumn objects.
     */
    ~FrontEnd();

    /**
     * Enable the verbose mode if enable is true
     * @param enable Enable verbose mode if true
     **/
    void SetVerbose(bool enable);

    /**
     * Get the ID for this front-end.
     * @return The ID for this front-end from 0 to 7.
     **/
    uint32_t GetChipID();

    /**
     * Set the ID for this front-end from 0 to 7.
     * @return The ID for this front-end from 0 to 7.
     **/
    void SetChipID(uint32_t chipid);

    /**
     * Set the common name of this FrontEnd.
     * @param name The common name for this FrontEnd.
     **/
    void SetName(std::string name);

    /**
     * Get the common name of this FrontEnd.
     * @return The common name for this FrontEnd.
     **/
    std::string GetName();

    /**
     * Clear the Command Encoder and the Record Decoder
     */
    void Clear();

    /**
     * Check if the FrontEnd is active
     * An inactive FrontEnd should not be used further during a scan.
     * @return True if it is active
     */
    bool IsActive();

    /**
     * Force the FrontEnd to be active or inactive.
     * An inactive FrontEnd should not be used further during a scan.
     * @param active Activate if True, else deactivate the FrontEnd
     */
    void SetActive(bool active);

    /**
     * Change the operation mode of the FrontEnd between running mode (RunMode::RUN)
     * and configuration mode (RunMode::CONFIG).
     * A FrontEnd in running mode cannot accept read register, write register or write front-end commands.
     * @param enable Set to run mode if True, else set to config mode.
     */
    void SetRunMode(bool enable);

    /**
     * Get the global threshold by merging the Configuration::Vthin_Coarse
     * and Configuration::Vthin_Fine register values
     * @return The global threshold
     */
    uint32_t GetGlobalThreshold();

    /**
     * Set the global threshold by merging the Configuration::Vthin_Coarse and Vthin_Fine registers
     * @param threshold The global threshold
     */
    void SetGlobalThreshold(uint32_t threshold);

    /**
     * Set the pixel enable (Pixel::Enable) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * @param enable Enable the Pixel if true
     */
    void SetPixelEnable(uint32_t col, uint32_t row, bool enable);

    /**
     * Get the pixel enable (Pixel::Enable) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * return true if the pixel is enabled
     */
    bool GetPixelEnable(uint32_t col, uint32_t row);


    bool GetPixelMask(uint32_t col, uint32_t row);

    void SetPixelMask(uint32_t col, uint32_t row, bool enable);

    /**
     * Set the pixel hitbus (Pixel::HitBus) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * @param enable Enable the Pixel hitbus if true
     */
    void SetPixelHitBus(uint32_t col, uint32_t row, bool enable);

    /**
     * Get the pixel hitbus (Pixel::HitBus) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * return true if the pixel hitbus is enabled
     */
    bool GetPixelHitBus(uint32_t col, uint32_t row);

    /**
     * Get the pixel threshold (Pixel::TDAC) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * @return The pixel threshold
     */
    uint32_t GetPixelThreshold(uint32_t col, uint32_t row);

    /**
     * Get the pixel threshold (Pixel::TDAC) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * @param threshold The pixel threshold
     */
    void SetPixelThreshold(uint32_t col, uint32_t row, uint32_t threshold);

    /**
     * Get the global pre-amplifier feedback (Configuration::PrmpVbpf)
     * @return The global feedback
     */
    uint32_t GetGlobalPreampFeedback();

    /**
     * Get the global pre-amplifier feedback (Configuration::PrmpVbpf)
     * @param feedback The global pre-amplifier feedback
     */
    void SetGlobalPreampFeedback(uint32_t feedback);

    /**
     * Get the pixel pre-amplifier feedback (Pixel::FDAC) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * @return The pixel pre-amplifier feedback
     */
    uint32_t GetPixelPreampFeedback(uint32_t col, uint32_t row);

    /**
     * Set the pixel pre-amplifier feedback (Pixel::FDAC) from the corresponding Pixel in the DoubleColumn
     * @param col The Pixel column
     * @param row The Pixel row
     * @param feedback The pixel pre-amplifier feedback
     */
    void SetPixelPreampFeedback(uint32_t col, uint32_t row, uint32_t feedback);

    /**
     * Get the global Configuration pointer for this FrontEnd.
     * It allows direct access to all the global 16-bit Register objects,
     * and the virtual Field objects associated like the HitOr.
     * Any modification of the Configuration pointer has to be followed
     * by FrontEnd::ConfigGlobal.
     *
     *     value=GetConfig()->GetField(Configuration::HitOr)->GetValue()
     *     GetConfig()->GetField(Configuration::HitOr)->SetValue(value)
     *
     * @return The global Configuration pointer
     */
    Configuration * GetConfig();

    /**
     * Set the global registers of this FrontEnd by passing a map of configuration values.
     * This is the default way to load the global configuration into memory.
     * In order to convert the configuration values into Commands call FrontEnd::ConfigGlobal
     * after calling this method.
     * The strings used for this map can be found in Configuration::m_name2index.
     * @param config A map of strings to unsigned 32 bit values
     */
    void LoadGlobalConfig(std::map<std::string,uint32_t> config);

    /**
     * Set a pixel register of this FrontEnd for the given column, row, and register name.
     * The names of the registers are Enable, HitBus, LCap, SCap, TDAC, FDAC.
     * This is the default way to load the pixel configuration into memory.
     * In order to convert the configuration values into Commands call FrontEnd::ConfigPixels
     * after calling this method.
     * @param column The column of the pixel
     * @param row The row the pixel
     * @param name The name of the register
     * @param value The value for the pixel register
     */
    void LoadPixelConfig(uint32_t column, uint32_t row, std::string name, uint32_t value);

    /**
     * Get the value of a Pixel register given column, row, and register name.
     * The names of the registers are Enable, HitBus, LCap, SCap, TDAC, FDAC.
     * This is the default way to read the pixel configuration from memory.
     * @param column The column of the pixel
     * @param row The row the pixel
     * @param name The name of the register
     * @return The value for the pixel register
     */
    uint32_t GetPixelConfig(uint32_t column, uint32_t row, std::string name);

    /**
     * Convert the updated global registers into a WrReg Command array, and add them to the Encoder.
     */
    void ConfigGlobal();

    /**
     * Request the reading of the global registers by adding 36 WrReg commands to the Encoder.
     */
    void ReadGlobal();

    /**
     * Write to the FrontEnd the value of all the latches of all pixels at once.
     * Generates a very long command that should be transmitted through the communication layer.
     *
     * A transparent latch is one where the inputs are passed straight through to the outputs when the "select" signal is active.
     * When the select signal goes inactive, the final input state is latched on the outputs.
     */
    void ConfigPixels();

    /**
     * Send a BCR and and ECR to the FEI4B.
     */
    void Reset();

    /**
     * Enable the large capacitor the pixels (Pixel::LCap) of the double columns indicated by mode and dc.
     * Default is to enable all the capacitors in the whole matrix (mode=3, dc=0).
     * @param mode The Configuration::Colpr_mode, values are 0=single DC, 1=every 4th DC, 2=every 8th DC, 3=all DCs.
     * @param dc The Configuration::Colpr_addr, max value depends on mode: 0: 39, 1: 10, 2: 5, 3: 1
     */
    void EnableLCap(uint32_t mode=3, uint32_t dc=0);

    /**
     * Enable the small capacitor the pixels (Pixel::SCap) of the double columns indicated by mode and dc.
     * Default is to enable all the capacitors in the whole matrix (mode=3, dc=0).
     * @param mode The Configuration::Colpr_mode, values are 0=single DC, 1=every 4th DC, 2=every 8th DC, 3=all DCs.
     * @param dc The Configuration::Colpr_addr, max value depends on mode: 0: 39, 1: 10, 2: 5, 3: 1
     */
    void EnableSCap(uint32_t mode=3, uint32_t dc=0);

    /**
     * Configure the Pixel thresholds (Pixel::TDAC) of all the pixel matrix.
     * Generates a very long command that should be transmitted through the communication layer.
     *
     * This method loops over all double columns, and builds a WeFE Command for every bit of the Pixel::TDAC
     * that is written into the shift register, and loaded to the corresponding Pixel latch by the ConfigPixel,
     * that is handled internally.
     *
     * Note the bits are written from Pixel::TDAC_0 to Pixel::TDAC_4 because of the order of the bits.
     *
     * This method is intended to be used once every time the whole Pixel::Thresholds have been updated with SetPixelThreshold.
     */
    void ConfigPixelThresholds();

    /**
     * Configure the Pixel thresholds (Pixel::HitBus) of all the pixel matrix.
     * Generates a very long command that should be transmitted through the communication layer.
     *
     * This method loops over all double columns, and builds a WeFE Command for the Pixel::HitBus
     * that is written into the shift register, and loaded to the corresponding Pixel latch by the ConfigPixel,
     * that is handled internally.
     *
     * This method is intended to be used once every time the whole Pixel::HitBus have been updated with SetPixelHitBus.
     */
    void ConfigPixelHitBus();

    /**
     * Configure the Pixel pre-amplifier feedback (Pixel::FDAC) of all the pixel matrix.
     * Generates a very long command that should be transmitted through the communication layer.
     *
     * This method loops over all double columns, and builds a WeFE Command for every bit of the Pixel::FDAC
     * that is written into the shift register, and loaded to the corresponding Pixel latch by the ConfigPixel,
     * that is handled internally.
     *
     * Note the bits are written from Pixel::FDAC_3 to Pixel::FDAC_0 because of the order of the bits.
     * This is done on purpose.
     *
     * This method is intended to be used once every time the whole Pixel pre-amplifier feedback values have
     * been updated with SetPixelPreampFeedback.
     */
    void ConfigPixelPreampFeedback();

    /**
     * Write to the FrontEnd the value for the selected latch of all pixels at once.
     *
     * A transparent latch is one where the inputs are passed straight through to the outputs when the "select" signal is active.
     * When the select signal goes inactive, the final input state is latched on the outputs.
     *
     * @param pos The pixel latch to configure
     */
    void ConfigPixel(uint32_t pos);

    /**
     * Write to the FrontEnd the value for the selected latch of the selected double column by mode and dc.
     * The dc_mode changes the selection mode for the double columns.
     *  - mode 0: address one single double column, max 40.
     *  - mode 1: address one every four double columns, max 4.
     *  - mode 2: address one every eight double columns, max 8.
     *  - mode 3: address every double column at once, max 1.
     *
     * A transparent latch is one where the inputs are passed straight through to the outputs when the "select" signal is active.
     * When the select signal goes inactive, the final input state is latched on the outputs.
     *
     * @param dc_mode The double column addressing mode (single, every 4, every 8, all of them)
     * @param dc The actual double column to address (maximum value is 40, 4, 8, or 1)
     * @param pos The pixel latch to configure
     */
    void ConfigDoubleColumn(uint32_t dc_mode, uint32_t dc, uint32_t pos=13);

    /**
     * Change the enable mask of the pixel matrix.
     * This is done to change the sequence of pixel that will have their output enable during a scan.
     * It avoids cross-talk and saturation of the output data bandwidth.
     * The mask_mode selects how many pixels to skip in the masking
     *  - mode 0: all pixels masked
     *  - mode 1: mask every pixel
     *  - mode 2: mask every pixel out of 2
     *  - mode 3: mask every pixel out of 3
     *  - mode 8: mask every pixel out of 8
     * The mask_iter changes the sequence of the mask from 0 to mask_mode.
     * @param mask_mode How many pixels to skip in the masking
     * @param mask_iter Iteration in the pixel mask (up to mask_mode)
     */
    void SetMask(uint32_t mask_mode, uint32_t mask_iter);

    /**
     * Change the double column that is selected for an injection.
     * The double columns that will be targeted by the digital or analog injection
     * depend on the value of the Configuration::Colpr_Mode and Configuration::Colpr_Addr.
     *
     * | Colpr_Addr | Digital [Col] | Analog [Col] |
     * | ---------- | ------------- | ------------ |
     * | 0          | 1 and 2       | 0            |
     * | 39         | 79 and 80     | 78, 79, 80   |
     * | n          | 2n+1 and 2n+2 | 2n and 2n+1  |
     *
     * @param dc_mode The double column addressing mode (single, every 4, every 8, all of them)
     * @param dc The actual double column to address (maximum value is 40, 4, 8, or 1)
     */
    void SelectDoubleColumn(uint32_t dc_mode, uint32_t dc);

    /**
     * Shift the shift register of the selected double column
     */
    void ShiftMask();

    /**
     * Prepare the trigger sequence for the scan.
     * It will
     */
    void Trigger(uint32_t delay=0);

    /**
     * Get the next Hit from the FIFO of hits.
     * @return The next available Hit
     */
    Hit * GetHit();

    /**
     * Pop the next Hit from the FIFO and delete it.
     * @return True if there FIFO is still not empty.
     */
    bool NextHit();

    /**
     * Check if the FIFO of Hits has any hits.
     * @return True if there FIFO has hits.
     */
    bool HasHits();

    /**
     * Handle the data from the communication layer given by a byte array (recv_data) and its size (recv_size).
     * The byte array will be decoded by the Decoder into a Record array,
     * and interpreted accordingly.
     * The AddressRecord, and ValueRecord records are used to update the values of the Configuration object.
     * The DataHeader, DataRecord, and ServiceRecord records, are decoded into Hit objects and stored in an internal FIFO.
     * The way to read the FIFO is through FrontEnd::GetHit,
     * and FrontEnd::HasHits to check if there are any hits in the FIFO.
     *
     * @param recv_data Array of bytes from the communication layer.
     * @param recv_size Number of bytes in the byte array
     */
    void HandleData(uint8_t *recv_data, uint32_t recv_size);

    /**
     * Encode the commands stored in the Encoder in order to obtain the byte array
     * that has to be sent to the communication layer.
     * Calling this method is mandatory after performing any configuration, in order to populate the byte array.
     * The communication with the actual front-end is handled externally to this class.
     */
    void ProcessCommands();

    /**
     * Get the output byte stream from the emulator.
     * @return Byte stream output from the emulator.
     **/
    uint8_t * GetBytes();

    /**
     * Get the length of the output byte stream from the emulator.
     * @return Length of the byte stream output from the emulator.
     **/
    uint32_t GetLength();

    /**
     * This map is a look-up-table with the maximum number of iterations
     * per DoubleColumn address mode.
     * @brief Maximum number of iterations per DoubleColumn address mode.
     */
    static std::map<uint32_t,uint32_t> mode_dc_max;

    /**
     * This map is a look-up-table with the double columns addressed per
     * iteration for each DoubleColumn address mode.
     * @brief Iteration order per double column per address mode.
     */
    static std::map<uint32_t,std::map<uint32_t,uint32_t> >mode_dc_it;

  private:

    std::vector<uint32_t> GetActiveDoubleColumns(uint32_t mode=0xFF, uint32_t addr=0xFF);

    bool m_verbose;
    bool m_active;
    uint32_t m_chipid;
    std::string m_name;

    float m_smallCap;
    float m_largeCap;
    float m_vcalOffset;
    float m_vcalSlope;

    Decoder *m_decoder;
    Encoder *m_encoder;
    Configuration *m_config;
    std::vector<DoubleColumn*> m_dcs;
    std::deque<Hit*> m_hits;
    std::mutex m_mutex;
};

}
#endif
