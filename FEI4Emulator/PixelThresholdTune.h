#ifndef FEI4B_PIXELTHRESHOLDTUNE_H
#define FEI4B_PIXELTHRESHOLDTUNE_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * PixelThresholdTune tunes the in-pixel threshold register (Pixel::TDAC) to reduce the threshold dispersion across the chip.
 * This is done by evaluating the occupancy of the pixel at the target charge as a function of the 5-bit in-pixel threshold.
 * The initial value for every pixel is 16 unless the retune flag has been set,
 * in which case it will be read from the configuration file.
 * The analysis provides the next value for the next iteration.
 *
 * The first part of the scan sets the threshold value for each pixel, the different masks of the chip are looped over,
 * as well as the double columns, a charge is injected and triggers are sent to the chip.
 * Data is then read out for each chip.
 * The occupancy per pixel is accumulated at each step of the threshold loop and evaluated by the analysis.
 * In cases where the occupancy of the pixel is larger than 50% the value of the threshold is lowered and
 * if the occupancy of the pixel is lower than 50% the value of the threshold is raised.
 * The algorithm converges when the occupancy of the pixel is between 49% and 51% of the 98% of the unmasked pixels.
 * The new value of the pixel threshold register is passed onto the next iteration of the loop.
 * This scan also does not mask pixels at the end and requires HV to be applied across the front-end.
 *
 *
 * @brief FEI4B PixelThresholdTune
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class PixelThresholdTune: public Handler{

public:

  /**
   * Create the scan
   */
  PixelThresholdTune();

  /**
   * Delete the scan
   */
  virtual ~PixelThresholdTune();

  /**
   * Configure the front-end in-pixel threshold to the initial value or read from file.
   * Start a loop that will only finish when a new threshold is found for all pixels,
   * that is followed by a mask loop, a double column loop, and a trigger and read-out loop.
   * The hits are analysed for the whole matrix, and a new threshold is proposed,
   * until the algorithm converges.
   *
   * The new threshold is proposed as bisection of high and low values of the threshold,
   * if the mean occupancy is higher than 0.51 % the upper threshold is reduced,
   * and if the mean is lower than 0.49 % the lower threshold is raised.
   * The new threshold is then proposed after the range is defined.
   */
  virtual void Run();

private:


};

}

#endif
