#ifndef FEI4B_CONFIGURATION_H
#define FEI4B_CONFIGURATION_H

#include "FEI4Emulator/Field.h"
#include "FEI4Emulator/Register.h"
#include <map>
#include <vector>
#include <string>

namespace fei4b{

/**
 * FEI4B Configuration map
 *
 * @brief FEI4B global Configuration
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class Configuration{
	
  public:

	enum FieldType{
    Reg1Spare,       /**< Register 1, bits 0x9 to 0xF **/
    SmallHitErase,   /**< Register 1, bit 0x8 **/
    EventLimit,      /**< Register 1, bits 0x0 to 0x7, big endian **/
    Trig_Count,      /**< Register 2, bits 0xC to 0xF, little endian **/
	  Conf_AddrEnable, /**< Register 2, bit 0xB **/
	  Reg2Spare,       /**< Register 2, bits 0x0 to 0xA **/
	  ErrorMask_0,     /**< Register 3, bits 0x0 to 0xF **/
	  ErrorMask_1,     /**< Register 4, bits 0x0 to 0xF **/
	  PrmpVbp_R,       /**< Register 5, bits 0x8 to 0xF, big endian **/
	  BufVgOpAmp,      /**< Register 5, bits 0x0 to 0x7, big endian **/
	  Reg6Spare,       /**< Register 6, bits 0x8 to 0xF **/
    PrmpVbp,         /**< Register 6, bits 0x0 to 0x7, big endian **/
    TdacVbn,         /**< Register 7, bits 0x8 to 0xF, big endian **/
    DisVbn,          /**< Register 7, bits 0x0 to 0x7, big endian **/
    Amp2Vbn,         /**< Register 8, bits 0x8 to 0xF, big endian **/
    Amp2VbpFol,      /**< Register 8, bits 0x0 to 0x7, big endian **/
    Reg9Spare,       /**< Register 9, bits 0x8 to 0xF, big endian **/
    Amp2Vbp,         /**< Register 9, bits 0x0 to 0x7, big endian **/
    FdacVbn,         /**< Register 10, bits 0x8 to 0xF, big endian **/
    Amp2Vbpff,       /**< Register 10, bits 0x0 to 0x7, big endian **/
    PrmpVbnFol,      /**< Register 11, bits 0x8 to 0xF, big endian **/
    PrmpVbp_L,       /**< Register 11, bits 0x0 to 0x7, big endian **/
    PrmpVbpf,        /**< Register 12, bits 0x8 to 0xF, big endian **/
    PrmpVbnLcc,      /**< Register 12, bits 0x0 to 0x7, big endian **/
    S1,              /**< Register 13, bit 0xF **/
    S0,              /**< Register 13, bit 0xE **/
    PixelLatch,      /**< Register 13, bits 0x1 to 0xD, big endian **/
    Reg13Spare,      /**< Register 13, bit 0x0 **/
    LVDSDrvIref,     /**< Register 14, bits 0x8 to 0xF, big endian **/
    GADCCompBias,    /**< Register 14, bits 0x0 to 0x7, big endian **/
    PllIbias,        /**< Register 15, bits 0x8 to 0xF, big endian **/
    LVDSDrvVos,      /**< Register 15, bits 0x0 to 0x7, big endian **/
    TempSensIbias,   /**< Register 16, bits 0x8 to 0xF, big endian **/
    PllIcp,          /**< Register 16, bits 0x0 to 0x7, big endian **/
    Reg17Spare,      /**< Register 17, bits 0x8 to 0xF **/
    PlsrIDacRamp,    /**< Register 17, bits 0x0 to 0x7, big endian **/
    VrefDigTune,     /**< Register 18, bits 0x8 to 0xF, big endian **/
    PlsrVgOPamp,     /**< Register 18, bits 0x0 to 0x7, big endian **/
    PlsrDacBias,     /**< Register 19, bits 0x8 to 0xF, big endian **/
    VrefAnTune,      /**< Register 19, bits 0x0 to 0x7, big endian **/
    Vthin_Coarse,    /**< Register 20, bits 0x8 to 0xF, big endian **/
    Vthin_Fine,      /**< Register 20, bits 0x0 to 0x7, big endian **/
    Reg21Spare,      /**< Register 21, bits 0xD to 0xF **/
    HITLD_IN,        /**< Register 21, bit 0xC **/
    DINJ_Override,   /**< Register 21, bit 0xB **/
    DigHitIn_Sel,    /**< Register 21, bit 0xA **/
    PlsrDac,         /**< Register 21, bits 0x0 to 0x9, big endian **/
    Reg22Spare2,     /**< Register 22, bits 0xA to 0xF **/
    Colpr_Mode,      /**< Register 22, bits 0x8 to 0x9, big endian **/
    Colpr_Addr,      /**< Register 22, bits 0x2 to 0x7, big endian **/
    Reg22Spare1,     /**< Register 22, bits 0x0 to 0x1 **/
    DisableColCfg_0, /**< Register 23, bits 0x0 to 0xF, little endian **/
    DisableColCfg_1, /**< Register 24, bits 0x0 to 0xF, little endian **/
    Trig_Latency,    /**< Register 25, bits 0x8 to 0xF, little endian **/
    DisableColCfg_2, /**< Register 25, bits 0x0 to 0x7, little endian **/
    CalPulseWidth,   /**< Register 26, bits 0xB to 0xF, little endian **/
    CalPulseDelay,   /**< Register 26, bits 0x3 to 0xA, little endian **/
    CMDcnt,          /**< Register 26, bits 0x3 to 0xF, little endian **/
    StopModeCnfg,    /**< Register 26, bit 0x2 **/
    HitDiscCfg,      /**< Register 26, bits 0x0 to 0x1, little endian **/
    PLLEn,           /**< Register 27, bit 0xF **/
    EFUSE_Sense,     /**< Register 27, bit 0xE **/
    StopClkPulse,    /**< Register 27, bit 0xD **/
    ReadErrorReq,    /**< Register 27, bit 0xC **/
	  Reg27Spare1,     /**< Register 27, bit 0xB **/
	  GADCStart,       /**< Register 27, bit 0xA **/
	  SRRead,          /**< Register 27, bit 0x9 **/
	  Reg27Spare2,     /**< Register 27, bits 0x6 to 0x8 **/
	  HitOr,           /**< Register 27, bit 0x5 **/
	  CalEn,           /**< Register 27, bit 0x4 **/
	  SRClr,           /**< Register 27, bit 0x3 **/
	  LatchEn,         /**< Register 27, bit 0x2 **/
	  SR_Clock,        /**< Register 27, bit 0x1 **/
    M13,             /**< Register 27, bit 0x0 **/
	  LVDSDrvSet06,    /**< Register 28, bit 0xF **/
	  Reg28Spare,      /**< Register 28, bits 0xA to 0xE **/
    EN_40M,          /**< Register 28, bit 0x9 **/
    EN_80M,          /**< Register 28, bit 0x8 **/
    CLK0_S2,         /**< Register 28, bit 0x7 **/
    CLK0_S1,         /**< Register 28, bit 0x6 **/
    CLK0_S0,         /**< Register 28, bit 0x5 **/
    CLK1_S2,         /**< Register 28, bit 0x4 **/
    CLK1_S1,         /**< Register 28, bit 0x3 **/
    CLK1_S0,         /**< Register 28, bit 0x2 **/
    EN_160M,         /**< Register 28, bit 0x1 **/
    EN_320M,         /**< Register 28, bit 0x0 **/
	  Reg29Spare1,     /**< Register 29, bits 0xE to 0xF **/
	  No8b10b,         /**< Register 29, bit 0xD **/
	  Clk2Out,         /**< Register 29, bit 0xC **/
	  EmptyRecord,     /**< Register 29, bits 0x4 to 0xB, big endian **/
	  Reg29Spare2,     /**< Register 29, bit 0x3 **/
    LVDSDrvEn,       /**< Register 29, bit 0x2 **/
    LVDSDrvSet30,    /**< Register 29, bit 0x1 **/
    LVDSDrvSet12,    /**< Register 29, bit 0x0 **/
    TempSensDisable, /**< Register 30, bits 0xE to 0xF, big endian **/
    TempSensDiodeSel,/**< Register 30, bit 0xD **/
    MonleakRange,    /**< Register 30, bit 0xC **/
	  Reg30Spare,      /**< Register 30, bits 0x0 to 0xB **/
	  PlsrRiseUpTau,   /**< Register 31, bits 0xD to 0xE, little endian **/
	  PlsrPwr,         /**< Register 31, bit 0xC **/
	  PlsrDelay,       /**< Register 31, bits 0x6 to 0xB, big endian **/
	  ExtDigCalSW,     /**< Register 31, bit 0x5 **/
	  ExtAnaCalSW,     /**< Register 31, bit 0x4 **/
	  Reg31Spare,      /**< Register 31, bit 0x3 **/
	  GADCSel,         /**< Register 31, bits 0x0 to 0x2, little endian **/
    SELB_0,          /**< Register 32, bits 0x0 to 0xF, little endian **/
    SELB_1,          /**< Register 33, bits 0x0 to 0xF, little endian **/
    SELB_2,          /**< Register 34, bits 0x8 to 0xF, little endian **/
	  Reg34Spare1,     /**< Register 34, bits 0x5 to 0x7 **/
	  PrmpVbpEn,       /**< Register 34, bit 0x4 **/
	  Reg34Spare2,     /**< Register 34, bits 0x0 to 0x3 **/
	  Chip_SN          /**< Register 35, bits 0x0 to 0xF, little endian **/
	};

	/**
	 * Create a new configuration object. Initialize the fields to their default values.
	 **/
	Configuration();

	/**
	 * Clear internal arrays
	 **/
	~Configuration();

	/**
   * Get the number of registers in the configuration
   * @return The configuration size
   **/
	uint32_t Size();

	/**
	 * Get the whole 16-bit value of a register
	 * @param index Register index
	 * @return The 16-bit register value
	 **/
	uint16_t GetRegister(uint32_t index);

	/**
	 * Set the whole 16-bit value of a register
	 * @param index Register index
	 * @param value The 16-bit register value
	 **/
	void SetRegister(uint32_t index, uint16_t value);

  /**
   * Set the Field value from a string. Note it is not necessarily the whole register
   * @param name Field name
   * @param value The new value
   **/
  void SetField(std::string name, uint16_t value);

  /**
   * Set the Field value from a string. Note it is not necessarily the whole register
   * @param index Field index
   * @param value The new value
   **/
  void SetField(uint32_t index, uint16_t value);

  /**
	 * Get a Field inside the Configuration given
	 * its Field index (ie: Configuration::GateHitOr).
	 * @param index The Field index (Configuration::FieldType)
	 * @return pointer to the field
	 **/
	Field * GetField(uint32_t index);

  /**
   * Get a Field inside the Configuration by name
   * @param name The Field name (Configuration::m_name2index)
   * @return pointer to the field
   **/
  Field * GetField(std::string name);

  /**
   * Get list of addresses that have been updated
   * @return vector of addresses that have been updated
   **/
  std::vector<uint32_t> GetUpdatedRegisters();

  /**
   * Get map of addresses
   * @return map of strings to addresses
   **/
  std::map<std::string,uint32_t> GetRegisters();

 private:

	std::vector<Register> m_registers;
	std::map<uint32_t, Field*> m_fields;
  static std::map<std::string, uint32_t> m_name2index;
  std::vector<std::string> m_names_yarr;
  std::vector<std::string> m_names_rce;
};

}

#endif
