#ifndef FEI4B_PIXEL_H
#define FEI4B_PIXEL_H

#include <stdint.h>
#include <map>
#include <string>

namespace fei4b{

/**
 * A Pixel is a representation of the in Pixel configuration of the FEI4B.
 * It contains 13 SEU tolerant bits (latches) that can be written one at a time
 * with vector of 672 bits to a DoubleColumn (DoubleColumn::SetRegister).
 * The bits are grouped by functionality: Output, TDAC, LargeCap, SmallCap, HitBus, FDAC.
 *
 * | Bit | Name     | Definition                            |
 * | --- | ----     | ----------                            |
 * |  0  | Output   | Enable the output of the Pixel (mask) |
 * | 1:5 | TDAC     | Pixel threshold setting (LSB first)   |
 * |  6  | SmallCap | Use the small capacitor for injection |
 * |  7  | LargeCap | Use the large capacitor for injection |
 * |  8  | HitBus   | Connect the Pixel to the hit bus OR   |
 * | 9:12| FDAC     | Pixel TOT setting (MSB first)         |
 *
 * @brief FEI4B Pixel emulator
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/
class Pixel{

public:

  static const uint32_t Output = 0;
  static const uint32_t Enable = 0;
  static const uint32_t TDAC   = 1;
  static const uint32_t TDAC_0 = 1;
  static const uint32_t TDAC_1 = 2;
  static const uint32_t TDAC_2 = 3;
  static const uint32_t TDAC_3 = 4;
  static const uint32_t TDAC_4 = 5;
  static const uint32_t SmallCap = 6;
  static const uint32_t SCap   = 6;
  static const uint32_t LargeCap = 7;
  static const uint32_t LCap   = 7;
  static const uint32_t HitBus = 8;
  static const uint32_t FDAC   = 9;
  static const uint32_t FDAC_3 = 9;
  static const uint32_t FDAC_2 = 10;
  static const uint32_t FDAC_1 = 11;
  static const uint32_t FDAC_0 = 12;

  /**
   * Create a new Pixel with default empty configuration
   **/
  Pixel();

  /**
   * Empty destructor
   **/
  ~Pixel();

  /**
   * Set a Pixel register (group of bits) given an index.
   * Any index between 1 and 5 will result in TDAC, any index between 9 and 12 will result in FDAC.
   * @param index The index of the register (0 to 12)
   * @param value The value of the register
   **/
  void SetRegister(uint32_t index, uint32_t value);

  /**
   * Set a Pixel register (group of bits) given a name.
   * @param name The name of the register (Output, LargeCap)
   * @param value The value of the register
   **/
  void SetRegister(std::string name, uint32_t value);

  /**
   * Get a Pixel register (group of bits) given an index.
   * Any index between 1 and 5 will result in TDAC, any index between 9 and 12 will result in FDAC.
   * @param index The index of the register (0 to 12)
   * @return The value of the register
   **/
  uint32_t GetRegister(uint32_t index);

  /**
   * Get a Pixel register (group of bits) given a name.
   * @param name The name of the register (Output, LargeCap)
   * @return The value of the register
   **/
  uint32_t GetRegister(std::string name);

  /**
   * Set the corresponding latch (bit) of the Pixel
   * @param pos The position of the latch (0 to 12)
   * @param value The value of the latch
   **/
  void SetLatch(uint32_t pos, bool value);

  /**
   * Get the corresponding latch (bit) of the Pixel
   * @param pos The position of the latch (0 to 12)
   * @return The value of the latch
   **/
  bool GetLatch(uint32_t pos);

  /**
   * Get if the output of the Pixel is enabled
   * @return The output status of the Pixel
   **/
  bool GetOutput();

  /**
   * Get the threshold setting of the Pixel
   * @return The threshold of the Pixel
   **/
  uint32_t GetTDAC();

  /**
   * Get if the large capacitor of the Pixel is enabled
   * @return The status of the large capacitor of the Pixel
   **/
  bool GetLargeCap();

  /**
   * Get if the small capacitor of the Pixel is enabled
   * @return The status of the small capacitor of the Pixel
   **/
  bool GetSmallCap();

  /**
   * Get if the Pixel is connected to the HitOR
   * @return The status of the connection to the HitOR of the Pixel
   **/
  bool GetHitBus();

  /**
   * Get the TOT setting of the Pixel
   * @return The TOT of the Pixel
   **/
  uint32_t GetFDAC();
  
  bool GetMasked();

  /**
   * Set if the output of the Pixel is enabled
   * @param output The output status of the Pixel
   **/
  void SetOutput(bool output);

  /**
   * Set the threshold setting of the Pixel
   * @param tdac The threshold of the Pixel
   **/
  void SetTDAC(uint32_t tdac);

  /**
   * Set if the large capacitor of the Pixel is enabled
   * @param largecap The status of the large capacitor of the Pixel
   **/
  void SetLargeCap(bool largecap);

  /**
   * Set if the small capacitor of the Pixel is enabled
   * @param smallcap The status of the small capacitor of the Pixel
   **/
  void SetSmallCap(bool smallcap);

  /**
   * Get if the Pixel is connected to the HitOR
   * @return The status of the connection to the HitOR of the Pixel
   **/
  void SetHitBus(bool hitbus);

  /**
   * Set the TOT setting of the Pixel
   * @param fdac The TOT of the Pixel
   **/
  void SetFDAC(uint32_t fdac);
  
  void SetMasked(bool masked);

private:

  bool m_masked;
  bool m_output;
  bool m_largecap;
  bool m_smallcap;
  bool m_hitbus;
  uint32_t m_tdac;
  uint32_t m_fdac;

  static std::map<std::string, uint32_t> m_name2index;
};

}

#endif
