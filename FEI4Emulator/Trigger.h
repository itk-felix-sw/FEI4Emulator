#ifndef FEI4B_TRIGGER_H
#define FEI4B_TRIGGER_H

#include "FEI4Emulator/Command.h"

namespace fei4b{

/**
 * A Trigger Command initiates the acquisition of a new event.
 * The minimum distance between 2 Trigger Commands is 5 clock cycles.
 * It is composed of an identifier 0b11101 that is tolerant to 1-bit flips.
 *
 * The bit content is the following:
 *
 * | Bit   |  5 |  0 |
 * | ---   | -- | -- |
 * | Rel   |  5 |  0 |
 * | Field |  1     ||
 * | Desc  |0b11101 ||
 * | Size  |  5     ||
 * 
 * Possible patterns for the header identifier are:
 * 
 * | ID (Bin) | ID (Hex) |
 * | -------- | -------- |
 * | 0b11101  | 0x1D     |
 * | 0b11100  | 0x1C     |
 * | 0b11111  | 0x1F     | 
 * | 0b11001  | 0x19     | 
 * | 0b10101  | 0x15     |
 * | 0b01101  | 0x0D     |
 *
 *
 * @brief FEI4B trigger command
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class Trigger: public Command{
  
 public:

  /** 
   * Default constructor
   **/
  Trigger();

  /** 
   * Create a new object from another one
   * @param copy Pointer to copy 
   **/
  Trigger(Trigger *copy);
    
  /**
   * Destructor
   **/
  ~Trigger();

  /** 
   * Create a new object from this one
   * @return copy Pointer to copy of this one 
   **/
  Trigger * Clone();
  
  /**
   * Return a human readable representation
   * @return The human readable representation
   **/
  std::string ToString();
  
  /**
   * Extract the contents from the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the new Position in the byte array
   **/
  Position UnPack(uint8_t * bytes, Position pos, uint32_t maxlen);
  
  /**
   * Place the contents in the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @return the new Position in the byte array
   **/
  Position Pack(uint8_t * bytes, Position pos);
  
  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();
    
 protected:

  uint32_t m_field1;

};

}
#endif 
