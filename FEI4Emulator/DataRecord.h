#ifndef FEI4B_DATARECORD_H
#define FEI4B_DATARECORD_H

#include "FEI4Emulator/Record.h"
#include <vector>

namespace fei4b{

/**
 * The DataRecord Record is the pixel data formatted using dynamic pairing, 
 * where a single DataRecord is used to send information from two neighbor 
 * pixels adjacent inside the same column. 
 * It contains 7-bit column (DataRecord::SetColumn, DataRecord::GetColumn),
 * followed by an 9-bit row (DataRecord::SetRow, DataRecord::GetRow),
 * a 4-bit TOT for the given pixel (DataRecord::SetTOT1, DataRecord::SetTOT1), and
 * a 4-bit for the pixel in the next row (row+1) (DataRecord::SetTOT2, DataRecord::SetTOT2).
 * Whenever pixel 336 is hit but 335 is not, the pairing will assign row 
 * number 336, and the ToT(2) value will contain no hit.
 * 
 * Encoding (DataRecord::Pack) and decoding (DataRecord::UnPack)
 * require that the HitDiscCfg it defined externally 
 * (DataRecord::SetHDC, DataRecord::GetHDC).
 *
 * The bit content is the following:
 *
 * | Bit  | 23 | 17 | 16 | 15 |  8 |  7 |  4 |  3 |  0 |
 * | ---  | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Rel  |  7 |  1 |  0 |  7 |  0 |  7 |  4 |  3 |  0 |
 * | Byte |  0         |||    1   ||  2             ||||
 * | Desc | Column || Row        ||| TOT1   ||  TOT2  ||
 * | Size |  7     || 9          ||| 4      || 4      ||
 * 
 * The TOT code is encoded for different HitDiscCfg settings (0,1,2):
 * 
 * | TOT |  0 |  1 |  2 |
 * | --- | -- | -- | -- |
 * |   0 |  F |  F |  F |
 * |   1 |  0 |  E |  E |
 * |   2 |  1 |  0 |  D |
 * |   3 |  2 |  1 |  0 |
 * |   4 |  3 |  2 |  1 |
 * |   5 |  4 |  3 |  2 |
 * |   6 |  5 |  4 |  3 |
 * |   7 |  6 |  5 |  4 |
 * |   8 |  7 |  6 |  5 |
 * |   9 |  8 |  7 |  6 |
 * |   A |  9 |  8 |  7 |
 * |   B |  A |  9 |  8 |
 * |   C |  B |  A |  9 |
 * |   D |  C |  B |  A |
 * |   E |  D |  C |  B |
 * |   F |  E |  D |  C |
 *
 *
 * @brief FEI4B data header record
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class DataRecord: public Record{
  
 public:

  /** 
   * Default constructor
   **/
  DataRecord();

  /** 
   * Create a new object from another one
   * @param copy Pointer to DataRecord to copy 
   **/
  DataRecord(DataRecord *copy);

  /** 
   * Create a new object from another one
   * @param col the column number
   * @param row the row number
   * @param tot1 the TOT1
   * @param tot2 the TOT2
   * @param hdc the HDC encoding
   **/
  DataRecord(uint32_t col,uint32_t row,uint32_t tot1,uint32_t tot2,uint32_t hdc=0);

  /**
   * Destructor
   **/
  ~DataRecord();

  /**
   * Return a human readable representation of the register
   * @return The human readable representation of the register
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the column number
   * @param column the column number
   **/
  void SetColumn(uint32_t column);

  /**
   * Get the column number
   * @return the column number
   **/
  uint32_t GetColumn();

  /**
   * Set the row number 
   * @param row the row number
   **/
  void SetRow(uint32_t row);

  /**
   * Get the row number
   * @return the row number
   **/
  uint32_t GetRow();
  
  /**
   * Set the TOT1
   * @param tot the TOT1
   **/
  void SetTOT1(uint32_t tot);

  /**
   * Get the TOT1
   * @return the TOT1 
   **/
  uint32_t GetTOT1();

  /**
   * Set the TOT2
   * @param tot the TOT2
   **/
  void SetTOT2(uint32_t tot);

  /**
   * Get the TOT2
   * @return the TOT2 
   **/
  uint32_t GetTOT2();

  /**
   * Set the HitDicCfg
   * @param hdc The HitDiscCfg
   **/
  void SetHDC(uint32_t hdc);

  /**
   * Get the HitDiscCfg
   * @return the HitDiscCfg 
   **/
  uint32_t GetHDC();
  
 protected:

  uint32_t m_col;
  uint32_t m_row;
  uint32_t m_tot1;
  uint32_t m_tot2;
  uint32_t m_hdc;
  
  /** Lookup table from HitDiscCfg to TOT **/
  static std::vector<std::vector<uint32_t> > m_hdc2tot;

  /** Lookup table from TOT to HitDiscCfg **/
  static std::vector<std::vector<uint32_t> > m_tot2hdc;

};

}
#endif 
