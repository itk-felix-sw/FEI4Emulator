#ifndef FEI4B_UNKNOWN_H
#define FEI4B_UNKNOWN_H

#include "FEI4Emulator/Command.h"

namespace fei4b{

/**
 * Gap filler with the size of a single bit
 * that corresponds to one clock cycle.
 *
 * @brief FEI4B unknown command
 * @author Carlos.Solans@cern.ch
 * @date April 2020
 **/

class Unknown: public Command{
  
 public:

  /** 
   * Default constructor
   **/
  Unknown();

  /** 
   * Create a new object from another one
   * @param copy Pointer to copy 
   **/
  Unknown(Unknown *copy);
  
  /**
   * Destructor
   **/
  ~Unknown();

  /**
   * Create a new object from this one
   * @return copy Pointer to copy of this one 
   **/
  Unknown * Clone();

  /**
   * Return a human readable representation
   * @return The human readable representation
   **/
  std::string ToString();
  
  /**
   * Extract the contents from the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the new Position in the byte array
   **/
  Position UnPack(uint8_t * bytes, Position pos, uint32_t maxlen);
  
  /**
   * Place the contents in the byte array
   * @param bytes the byte array
   * @param pos the Position in the byte array
   * @return the new Position in the byte array
   **/
  Position Pack(uint8_t * bytes, Position pos);
  
  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();
    
 protected:

  uint32_t m_field1;

};

}
#endif 
