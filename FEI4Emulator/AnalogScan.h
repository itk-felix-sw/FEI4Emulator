#ifndef FEI4B_ANALOGSCAN_H
#define FEI4B_ANALOGSCAN_H

#include "FEI4Emulator/Handler.h"

namespace fei4b{

/**
 * The AnalogScan injects a very large charge 50000 electrons (Configuration::PlsrDac=500) to the pixel
 * with a high threshold (Configuration::Vthin_Fine=100), and checks for the corresponding hit in the data.
 * Hot pixels will fire more than the number of trigger, and will be masked by setting Pixel::Enable=0.
 *
 * Because the output data bandwidth is limited, the injection is done in a few pixels at a time and in groups of double column.
 * The analysis of the digital scan checks that every single pixel has the required number of hits.
 * If there is a discrepancy larger than 50% in the number of hits, the Pixel will be masked by forcing Pixel::Enable=0.
 *
 * The analog injection requires a charge (Configuration::PlsrDac) higher than the threshold
 * (Configuration::Vthin_Fine), and the selection of the double column for the injection
 * (Configuration::Colpr_Addr, Configuration::Colpr_Mode),
 * and writing a 1 to the shift register of the desired pixel in the addressed double column.
 *
 * The addressing of the analog pulse is given by (2n and 2n+1) that is accomplished with
 * FrontEnd::SelectDoubleColumn.
 *
 * | Colpr_Addr | Analog [Col] |
 * | ---------- | ------------ |
 * | 0          | 0            |
 * | 1          | 1, 2         |
 * | 2          | 3, 4         |
 * | ...        | ...          |
 * | 28         | 76, 77       |
 * | 39         | 78, 79, 80   |
 * | n          | 2n and 2n+1  |
 *
 *
 * @brief FEI4B AnalogScan
 * @author Carlos.Solans@cern.ch
 * @date June 2020
 */

class AnalogScan: public Handler{

public:

  /**
   * Create the scan
   */
  AnalogScan();

  /**
   * Delete the Handler
   */
  virtual ~AnalogScan();

  /**
   * Create histograms, disable all pixels per front-end.
   * Loop the matrix in groups of pixels and double columns.
   * Trigger few events and collect the hits per trigger and per FrontEnd.
   * An occupancy histogram is filled per front-end, that is required
   * to have the same amount of entries or less than the number of triggers.
   * A pixel will be masked if it has more than the expected number of entries.
   */
  virtual void Run();

private:

};

}

#endif
