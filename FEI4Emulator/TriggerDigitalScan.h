#ifndef FEI4B_DIGITALSCAN_H
#define FEI4B_DIGITALSCAN_H

#include "FEI4Emulator/TriggerHandler.h"

namespace fei4b{

/**
 *
 *
 * @brief FEI4B ThresholdScan
 * @author Carlos.Solans@cern.ch
 * @date July 2020
 */

class TriggerDigitalScan: public TriggerHandler{

public:

  /**
   * Create the scan
   */
  TriggerDigitalScan();

  /**
   * Delete the Handler
   */
  virtual ~TriggerDigitalScan();

  /**
   */
  virtual void Run();

private:


};

}

#endif
