# FEI4B software for ITK-FELIX {#PackageFEI4Emulator}

This package contains all the software necesary to control, read-out and emulate the FEI4B pixel detector.

## The FEI4B pixel detector

An FEI4B detector (fei4b::FrontEnd, fei4b::Emulator) has a pixel matrix with 336 rows, and 80 columns. The configuration is stored in 36 global 16-bit registers, and 13 radiation hard latches per pixel. The global registers are accessed one at a time, where the 16-bit value has to be written to the front-end as a whole through a command, and read-back from the front-end in the same way. 
The bits of each register are grouped into fields, that control a speficic parameter of the FEI4B. 
Every two adjacent pixel columns starting with the first column defines a double column, which totals 40 double columns in the FEI4B, each one contains a 1-bit wide and 672-bit long shift register that can be written through a command.
The shift register can access the pixel latches one at a time through a process known as latching.
The latches in each pixel can to be set to transparent in order to accept the value from the shift register, or into latched mode, in which case they will ignore the value from the shift register.

The command path of the FEI4B can handle up to 80 Mbps of fei4b::Command commands. The fei4b::WrReg allows to write a global fei4b::Register, and the fei4b::RdReg is meant to read-back a global fei4b::Register, that will appear in the data path in the form of fei4b::AddressRecord and fei4b::ValueRecord. The writing of the shift register is performed through the fei4b::WrFE fei4b::Command into a subset of the double columns selected via the global fei4b::Register fei4b::Field values (fei4b::Configuration::Colpr_Mode, and fei4b::Configuration::Colpr_Addr).
After writing the shift register, the value of each bit has to be written into the corresponding fei4b::Pixel latch. This is controlled by a combination of global fei4b::Register fei4b::Field values (fei4b::Configuration::LatchEn, and fei4b::Configuration::PixelLatch), and the fei4b::Pulse fei4b::Command.

The threshold of the pixels is a combination of the global threshold for the hole matrix (fei4b::Configuration::Vthin_Coarse, fei4b::Configuration::Vthin_Fine), and the 5-bit in-pixel threshold (fei4b::Pixel::TDAC). The TOT of the pixels is set by a combination of the global feedback current of the preamplifier (fei4b::Configuration::PrmpVbpf), and the 4-bit in-pixel trimming value (fei4b::Pixel::FDAC).

## Calibration procedure

The calibration procedure of the FEI4B is handled by the classes extended from the fei4b::Handler class (see available calibration scans below).
The fei4b::Handler creates multiple fei4b::FrontEnd objects and multiple NETIO low latency send (TX) or subscribe (RX) sockets grouped by front-end module.
The fei4b::FrontEnd objects with the same TX elink will be grouped into the same TX. Every fei4b::FrontEnd will be allocated their own RX.

The Handler configuration is stored as JSON files. There are two types of files, a mapping file that contains the path to the configuration file of each fei4b::FrontEnd, its TX and RX elinks, and FELIX host details (see fei4b::Handler::SetMapping), and the actual configuration file per front-end that is based on previous configuration file formats (see fei4b::Handler::AddFE, fei4b::Handler::SaveFE for details).

The steps of a scan are fei4b::Handler::Connect, fei4b::Handler::Config, fei4b::Handler::Run, fei4b::Handler::Disconnect, and fei4b::Handler::Save. The actual calibration procedure takes place inside the fei4b::Handler::Run method.

### Available calibration scans

- fei4b::RegisterScan
- fei4b::DigitalScan
- fei4b::AnalogScan
- fei4b::GlobalThresholdTune
- fei4b::PixelThresholdTune
- fei4b::ThresholdScan
- fei4b::GlobalTotTune
- fei4b::PixelTotTune
- fei4b::TotScan
- fei4b::NoiseScan
- fei4b::SelfTriggerScan

## Implementation paradigm

This software is implemented following the idea of separating the communication, the payload, and the algorithm layers. Much like the view controller model that separates the data from how it is represented on a web-page, this software differenciates the payload (data and commands) from the actual communication layer (writing and reading from the front-end), and the algorithm (what do we want to do with the payload). The communication layer does not care what are the messages being transmitted as long as they can be encapsulated in the communication protocol. The payload layer does not care how the messages are handled, or what are they being used for. And the algorithm layer does not need to know how the messages are encoded, or transmitted as long as the communication is possible.

The fei4b::Handler implements the communication layer with NETIO, the classes extended from the fei4b::Record and fei4b::Command classes represent the payload layer specific for the FEI4B, and the classes extended from the fei4b::Handler implement the algorithm layers. Additionally the fei4b::FrontEnd and fei4b::Emulator are available helper classes that encapsulate the typical actions of the FEI4B, and have a meaning in the algorithm layer.

The FEI4B fei4b::FrontEnd is implemented with a set of classes (fei4b::Encoder, fei4b::Decoder, fei4b::Configuration, fei4b::DoubleColumn) that represent the functionalities of the FEI4B. In particular each class can perform their assigned operation and reverse. For example, the Command Encoder can encode a byte array from a Command array, and it can also decode a byte array into a Command array. This allows closure tests, and the possibility of emulating the front-end behaviour in the fei4b::Emulator class.

Other classes like fei4b::Configuration are designed to be modified from the operator perspective, by changing the virtual Fields of the FrontEnd (fei4b::Configuration::SetField), and from the front-end perspective by accessing the whole register at a time (fei4b::Configuration::SetRegister). This allows the usage of the class in the control class (fei4b::FrontEnd) and in emulation (fei4b::Emulator).

The fei4b::Handler implements the communication layer with FELIX through NETIO for any number of fei4b::FrontEnd objects. The processes inside the fei4b::Handler (fei4b::Handler::Connect, fei4b::Handler::Config, fei4b::Handler::Run) are single threaded, thus smart grouping of fei4b::FrontEnd objects into multiple fei4b::Handler instances, will allow concurrent processing. Needless to say, the tuning procedure of a pixel detector module is a sequence of tuning steps, but the tuning of many modules is a completely independent process, that can be highly parallelized. The available calibration scans and tuning procedures are those that extend from the fei4b::Handler.




## How to run a scan

1. Prepare the FEI4 configuration file
2. Prepare the mapping file
3. Launch the scan_manager_fei4b

